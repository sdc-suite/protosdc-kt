plugins {
    id("org.somda.protosdc.sdc.shared")
}

dependencies {
    implementation(projects.protosdcKt)

    implementation(libs.clikt)
}
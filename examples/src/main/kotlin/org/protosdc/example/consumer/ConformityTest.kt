package org.protosdc.example.consumer

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

enum class TestId {
    // DEV-23, DEV-24:
    // Discovery of a provider with a specific endpoint reference address
    //
    // See that Probe is answered
    // See that Resolve is answered
    //
    TEST_1,
    // DEV-25:
    // Connect to the provider with specific endpoint, i.e. establish TCP connection(s) and retrieve endpoint metadata
    //
    TEST_2,
    // DEV-26:
    // Read MDIB of the provider
    //
    TEST_3,
    // DEV-27:
    // Subscribe at least metrics, alerts, waveforms and operation invoked reports of the provider
    //
    TEST_4,
    // DEV-26, DEV-27:
    // Check that least one patient context exists
    //
    TEST_5,
    // DEV-26, DEV-27:
    // Check that at least one location context exists
    //
    TEST_6,
    // DEV-35, DEV-36, DEV-29:
    // Check that metric updates for one metric arrive at least 5 times in 30 seconds
    //
    TEST_7,
    // DEV-38, DEV-39:
    // Check that alert updates of one alert condition arrive at least 5 times in 30 seconds
    //
    TEST_8,
    // DEV-31, (DEV-44, DEV-45):
    // Execute external control operations (any operation that exists in the containment tree,
    // if none exist: skip test) by checking the ultimate transaction result is "finished"
    //
    // a. Any Activate
    // b. Any SetString
    // c. Any SetValue
    //
    TEST_9,
    // DEV-34:
    // Shutdown connection (cancel subscription, close connection)
    //
    TEST_10
}

enum class TestStatus {
    INITIAL,
    PASSED,
    FAILED,
    SKIPPED
}

data class TestResult(val testStatus: TestStatus = TestStatus.INITIAL, val cause: Throwable? = null)

class ConformityTestMap {
    private val mutex = Mutex()

    private val testMap = mutableMapOf(
        TestId.TEST_1 to TestResult(),
        TestId.TEST_2 to TestResult(),
        TestId.TEST_3 to TestResult(),
        TestId.TEST_4 to TestResult(),
        TestId.TEST_5 to TestResult(),
        TestId.TEST_6 to TestResult(),
        TestId.TEST_7 to TestResult(),
        TestId.TEST_8 to TestResult(),
        TestId.TEST_9 to TestResult(),
        TestId.TEST_10 to TestResult()
    )

    suspend fun getMap() = mutex.withLock { testMap.toMap() }

    suspend fun setResultIfInitial(id: TestId, status: TestStatus, cause: Throwable? = null) {
        mutex.withLock {
            if (testMap[id]?.testStatus == TestStatus.INITIAL) {
                testMap[id] = TestResult(status, cause)
            }
        }
    }

    suspend fun overrideResult(id: TestId, status: TestStatus, cause: Throwable? = null) {
        mutex.withLock {
            testMap[id] = TestResult(status, cause)
        }
    }
}
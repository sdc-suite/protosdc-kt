package org.protosdc.example.consumer

import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import org.apache.logging.log4j.kotlin.Logging
import org.protosdc.example.common.ProtoSdcApp
import org.somda.protosdc.biceps.common.MdibEntity
import org.somda.protosdc.biceps.common.access.MdibAccessEvent
import org.somda.protosdc.biceps.common.association
import org.somda.protosdc.biceps.common.descriptorHandleString
import org.somda.protosdc.device.consumer.SdcDeviceProxy
import org.somda.protosdc.device.consumer.SetServiceFactory
import org.somda.protosdc.discovery.consumer.DiscoveryEvent
import org.somda.protosdc.model.biceps.AbstractSetOneOf
import org.somda.protosdc.model.biceps.ContextAssociation
import org.somda.protosdc.model.biceps.HandleRef
import java.math.BigDecimal
import java.util.*
import kotlin.concurrent.thread

fun main(args: Array<String>) = ExampleConsumer().main(args)


class ExampleConsumer : ProtoSdcApp(name = "example-provider") {
    companion object : Logging {
        private const val MAX_WAIT_TIME_FOR_EXPLICIT_SEARCH = 5_000L
        private const val MAX_WAIT_TIME_FOR_IMPLICIT_SEARCH = 15_000L
    }

    private val endpointIdentifier by option("--endpointid", help = "Endpoint identifier")
        .default(UUID.randomUUID().toString())

    private val runConformityTest by option(
        "--conformitytest",
        help = "Runs conformity tests and closes application afterwards"
    ).flag(default = false)

    private val testMap = ConformityTestMap()

    override fun runApp() = runBlocking {
        val consumer = protoComponent.createSdcConsumer()
        val discovery = consumer.discovery.apply { start() }

        protoFramework.start()
        protoComponent.start()

        val discoverySubscription = discovery.subscribe()
        val job = launch(Dispatchers.Default) {
            val sdcDeviceProxy = async {
                val implicitSearch = async innerAsync@{
                    logger.info { "Start implicit search" }
                    while (coroutineContext.isActive) {
                        discoverySubscription.receive().let {
                            when (it) {
                                is DiscoveryEvent.EndpointEntered -> {
                                    testMap.setResultIfInitial(TestId.TEST_1, TestStatus.PASSED)
                                    runCatching {
                                        discovery.connect(it.endpoint)
                                    }.onFailure { exception ->
                                        testMap.overrideResult(TestId.TEST_2, TestStatus.FAILED, exception)
                                        testMap.overrideResult(TestId.TEST_3, TestStatus.FAILED, exception)
                                        testMap.overrideResult(TestId.TEST_4, TestStatus.FAILED, exception)
                                    }.getOrThrow()
                                }

                                else -> null
                            }?.let { device ->
                                logger.info { "Found device with id implicitly: ${runBlocking { device.endpointIdentifier() }}" }
                                testMap.overrideResult(TestId.TEST_2, TestStatus.PASSED)
                                testMap.overrideResult(TestId.TEST_3, TestStatus.PASSED)
                                testMap.overrideResult(TestId.TEST_4, TestStatus.PASSED)
                                return@innerAsync device
                            }
                        }
                    }
                    null
                }
                val explicitSearch = async {
                    try {
                        logger.info { "Start explicit search" }
                        withTimeout(MAX_WAIT_TIME_FOR_EXPLICIT_SEARCH) {
                            discovery.searchByEndpointIdentifierAsync(endpointIdentifier).await()
                        }.let { endpoint ->
                            logger.info { "Found device with id explicitly: ${endpoint.getOrThrow().endpointIdentifier}" }
                            runCatching {
                                discovery.connect(endpoint.getOrThrow())
                            }.onFailure { exception ->
                                testMap.overrideResult(TestId.TEST_2, TestStatus.FAILED, exception)
                                testMap.overrideResult(TestId.TEST_3, TestStatus.FAILED, exception)
                            }.getOrThrow()
                        }
                    } catch (e: Exception) {
                        logger.info {
                            "Device with id '$endpointIdentifier' could not be found by explicit search. " +
                                    "Keep waiting for implicit results..."
                        }
                        // that's ok, hello/bye subscription is still listening
                        null
                    }
                }

                explicitSearch.await()?.let {
                    implicitSearch.cancel()
                    return@async it
                }

                runCatching {
                    withTimeout(MAX_WAIT_TIME_FOR_IMPLICIT_SEARCH) {
                        return@withTimeout implicitSearch.await()
                    }
                }.onFailure {
                    testMap.overrideResult(TestId.TEST_1, TestStatus.FAILED, it)
                }.getOrThrow() ?: throw Exception("Implicit result exited with null")
            }.await()

            testMap.setResultIfInitial(TestId.TEST_1, TestStatus.PASSED)
            testMap.setResultIfInitial(TestId.TEST_2, TestStatus.PASSED)
            testMap.setResultIfInitial(TestId.TEST_3, TestStatus.PASSED)
            testMap.setResultIfInitial(TestId.TEST_4, TestStatus.PASSED)

            sdcDeviceProxy.let { deviceProxy ->
                logger.info { "Device connected, invoke SCO" }
                invokeScoOperations(sdcDeviceProxy)

                logger.info { "Check existence of context states" }
                val patientContexts = deviceProxy.mdibAccess().entitiesByType(MdibEntity.PatientContext::class)
                if (patientContexts.isEmpty()) {
                    testMap.overrideResult(TestId.TEST_5, TestStatus.FAILED, Exception("No patient context in MDIB"))
                } else {
                    patientContexts.firstOrNull { locCtxt ->
                        locCtxt.states.firstOrNull { it.association() == ContextAssociation.EnumType.Assoc } != null
                    }?.let {
                        testMap.overrideResult(TestId.TEST_5, TestStatus.PASSED)
                    } ?: testMap.overrideResult(
                        TestId.TEST_5,
                        TestStatus.FAILED,
                        Exception("No associated patient context found")
                    )
                }

                val locationContexts = deviceProxy.mdibAccess().entitiesByType(MdibEntity.LocationContext::class)
                if (locationContexts.isEmpty()) {
                    testMap.overrideResult(TestId.TEST_6, TestStatus.FAILED, Exception("No location context in MDIB"))
                } else {
                    locationContexts.firstOrNull { locCtxt ->
                        locCtxt.states.firstOrNull { it.association() == ContextAssociation.EnumType.Assoc } != null
                    }?.let {
                        testMap.overrideResult(TestId.TEST_6, TestStatus.PASSED)
                    } ?: testMap.overrideResult(
                        TestId.TEST_6,
                        TestStatus.FAILED,
                        Exception("No associated location context found")
                    )
                }

                logger.info { "Receive MDIB updates" }
                val maxWait = when (runConformityTest) {
                    true -> 30_000L // 30 seconds wait for incoming updates
                    false -> Long.MAX_VALUE
                }
                deviceProxy.mdibAccess().subscribe().also { subscription ->
                    val metricUpdates = mutableMapOf<String, Int>()
                    val alertUpdates = mutableMapOf<String, Int>()
                    runCatching {
                        withTimeout(maxWait) {
                            while (isActive) {
                                subscription.receive().also { event ->
                                    logger.debug {
                                        "State update: ${runBlocking { event.mdibAccess.mdibVersion() }} {\n${
                                            event.modifiedStates.joinToString(
                                                "\n"
                                            ) { obj -> "    $obj" }
                                        }\n}"
                                    }
                                    val incrementMetricStates: () -> Unit = {
                                        event.modifiedStates.forEach { modification ->
                                            metricUpdates[modification.state.descriptorHandleString()] = 1 +
                                                    (metricUpdates[modification.state.descriptorHandleString()] ?: 0)
                                        }
                                    }
                                    when (event) {
                                        is MdibAccessEvent.AlertStateModification -> {
                                            event.modifiedStates.forEach { modification ->
                                                alertUpdates[modification.state.descriptorHandleString()] = 1 +
                                                        (alertUpdates[modification.state.descriptorHandleString()] ?: 0)
                                            }
                                        }

                                        is MdibAccessEvent.MetricStateModification -> incrementMetricStates()
                                        is MdibAccessEvent.WaveformModification -> incrementMetricStates()
                                        else -> Unit
                                    }
                                }
                            }
                        }
                    }.onFailure {
                        // i.e. timeout

                        if (metricUpdates.filter { keyValue -> keyValue.value >= 5 }.isEmpty()) {
                            testMap.overrideResult(
                                TestId.TEST_7,
                                TestStatus.FAILED,
                                Exception("Not enough metric states collected in 30 seconds")
                            )
                        } else {
                            testMap.overrideResult(TestId.TEST_7, TestStatus.PASSED)
                        }
                        if (alertUpdates.filter { keyValue -> keyValue.value >= 5 }.isEmpty()) {
                            testMap.overrideResult(
                                TestId.TEST_8,
                                TestStatus.FAILED,
                                Exception("Not enough alert states collected in 30 seconds")
                            )
                        } else {
                            testMap.overrideResult(TestId.TEST_8, TestStatus.PASSED)
                        }
                    }
                }

                sdcDeviceProxy.stop()
                testMap.overrideResult(TestId.TEST_10, TestStatus.PASSED)
            }
        }

        if (!runConformityTest) {
            logger.info { "Consumer is running, press Enter to exit application" }
            readLine()
            logger.info { "Received exit signal, shutting down" }
        } else {
            Runtime.getRuntime().addShutdownHook(thread(start = false) {
                runBlocking {
                    println("Test results follow")
                    testMap.getMap().toSortedMap().forEach {
                        println(
                            "${it.key}: ${it.value.testStatus}" +
                                    (it.value.cause?.let { t -> "; cause: ${t.message}" } ?: "")
                        )
                    }
                }
            })

            logger.info { "Test run is active; application immediately shuts down when tests are finished" }
            job.join()
        }

        job.cancel()
        discovery.stop()

        protoComponent.stop()
        protoFramework.stop()

        logger.info { "Application stopped" }
    }

    private suspend fun invokeScoOperations(device: SdcDeviceProxy) {
        val maxWait = 5000L
        val setService = device.setService()
        if (setService == null) {
            testMap.overrideResult(TestId.TEST_9, TestStatus.FAILED, Exception("No set service found"))
            logger.error { "No set service found" }
            return
        }
        runCatching {
            device.mdibAccess().entitiesByType(MdibEntity.ActivateOperation::class).firstOrNull()?.let {
                withTimeout(maxWait) {
                    setService.invokeOperation(
                        AbstractSetOneOf.ChoiceActivate(
                            SetServiceFactory.activate(operationHandleRef = HandleRef(it.handle))
                        )
                    ).waitForSuccess()
                }
            } ?: throw Exception("No Activate operation found")
        }.onFailure {
            testMap.overrideResult(TestId.TEST_9, TestStatus.FAILED, it)
            logger.error { "Invoking Activate operation failed: ${it.message}" }
            logger.trace(it) { "Invoking Activate operation failed: ${it.message}" }
        }
        runCatching {
            device.mdibAccess().entitiesByType(MdibEntity.SetStringOperation::class).firstOrNull()?.let {
                withTimeout(maxWait) {
                    setService.invokeOperation(
                        AbstractSetOneOf.ChoiceSetString(
                            SetServiceFactory.setString(
                                operationHandleRef = HandleRef(it.handle),
                                requestedStringValue = "Test"
                            )
                        )
                    ).waitForSuccess()
                }
            } ?: throw Exception("No SetString operation found")
        }.onFailure {
            testMap.overrideResult(TestId.TEST_9, TestStatus.FAILED, it)
            logger.error { "Invoking SetString operation failed: ${it.message}" }
            logger.trace(it) { "Invoking SetString operation failed: ${it.message}" }
        }
        runCatching {
            device.mdibAccess().entitiesByType(MdibEntity.SetValueOperation::class).firstOrNull()?.let {
                withTimeout(maxWait) {
                    setService.invokeOperation(
                        AbstractSetOneOf.ChoiceSetValue(
                            SetServiceFactory.setValue(
                                operationHandleRef = HandleRef(it.handle),
                                requestedNumericValue = BigDecimal.ONE
                            )
                        )
                    ).waitForSuccess()
                }
            } ?: throw Exception("No SetValue operation found")
        }.onFailure {
            testMap.overrideResult(TestId.TEST_9, TestStatus.FAILED, it)
            logger.error { "Invoking SetValue operation failed: ${it.message}" }
            logger.trace(it) { "Invoking SetValue operation failed: ${it.message}" }
        }
        testMap.setResultIfInitial(TestId.TEST_9, TestStatus.PASSED)
        logger.info { "Successfully invoked Activate, SetString and SetValue" }
    }
}
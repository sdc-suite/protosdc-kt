package org.protosdc.example.provider

import com.akuleshov7.ktoml.Toml
import com.akuleshov7.ktoml.TomlInputConfig
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.file
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.decodeFromString
import org.apache.logging.log4j.kotlin.Logging
import org.protosdc.example.common.ProtoSdcApp
import org.somda.protosdc.proto.model.common.LocalizedString
import org.somda.protosdc.proto.model.metadata.EndpointMetadata
import java.net.URI
import java.util.*

fun main(args: Array<String>) = Main().main(args)

class Main : ProtoSdcApp(name = "example-provider") {
    companion object : Logging

    private val endpointIdentifier by option("--endpointid", help = "Endpoint identifier")
        .default(UUID.randomUUID().toString())

    private val configFile by option("--configfile", help = "Path to MDIB configuration TOML")
        .file()

    override fun runApp() = runBlocking {
        val config = configFile?.let {
            Toml(
                inputConfig = TomlInputConfig(
                    // allow/prohibit unknown names during the deserialization, default false
                    ignoreUnknownNames = false,
                    // allow/prohibit empty values like "a = # comment", default true
                    allowEmptyValues = true,
                    // allow/prohibit escaping of single quotes in literal strings, default true
                    allowEscapedQuotesInLiteralStrings = true
                )
            ).decodeFromString<ProviderMdibConfig>(
                it.readText(Charsets.UTF_8)
            )
        } ?: ProviderMdibConfig()

        val metadata = EndpointMetadata.newBuilder()
            .addFriendlyName(
                LocalizedString.newBuilder().setLocale("en").setValue("protoSDC example provider")
            ).build()

        val sdcProvider = protoComponent.createSdcProvider {
            operationInvocationReceiver(ExampleProviderOperationCallbacks())
            endpointIdentifier(endpointIdentifier)
            endpointMetadata(metadata)
        }

        val exampleProviderMdib = ExampleProviderMdib(
            mdibAccess = sdcProvider.sdcDevice.mdibAccess(),
            config
        )

        protoFramework.start()
        protoComponent.start()

        logger.info { "Setup MDIB for example provider..." }
        exampleProviderMdib.writeDescription()

        logger.info { "Starting example provider..." }
        sdcProvider.start()
        logger.info { "Example provider is running" }


        val job = launch(Dispatchers.Default) {
            exampleProviderMdib.runStateUpdates()
        }

        logger.info { "Press any key to stop the example provider" }
        readLine()
        logger.info { "Stopping example provider..." }
        job.cancel()
        sdcProvider.stop()

        protoComponent.stop()
        protoFramework.stop()

        logger.info { "Example provider has stopped" }
    }
}
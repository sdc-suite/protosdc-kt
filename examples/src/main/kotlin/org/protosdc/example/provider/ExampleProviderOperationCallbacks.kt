package org.protosdc.example.provider

import org.apache.logging.log4j.kotlin.Logging
import org.protosdc.example.common.Handles
import org.somda.protosdc.model.biceps.Activate
import org.somda.protosdc.model.biceps.InvocationState
import org.somda.protosdc.model.biceps.SetString
import org.somda.protosdc.model.biceps.SetValue
import org.somda.protosdc.device.provider.sco.Context
import org.somda.protosdc.device.provider.sco.InvocationResponse
import org.somda.protosdc.device.provider.sco.OperationInvocationReceiver

class ExampleProviderOperationCallbacks : OperationInvocationReceiver {
    private companion object : Logging

    override suspend fun activate(
        operationHandle: String,
        context: Context,
        request: Activate
    ): InvocationResponse {
        if (operationHandle != Handles.ACTIVATE_OPERATION.string) {
            return context.sendUnsuccessfulReport(InvocationState(InvocationState.EnumType.Fail))
        }
        logger.info { "Successfully called activate: $operationHandle" }

        return context.sendSuccessfulReport(InvocationState(InvocationState.EnumType.Fin))
    }

    override suspend fun setString(
        operationHandle: String,
        context: Context,
        request: SetString
    ): InvocationResponse {
        if (operationHandle != Handles.SET_STRING_OPERATION.string) {
            return context.sendUnsuccessfulReport(InvocationState(InvocationState.EnumType.Fail))
        }
        logger.info { "Successfully called setString: $operationHandle" }

        return context.sendSuccessfulReport(InvocationState(InvocationState.EnumType.Fin))
    }

    override suspend fun setValue(
        operationHandle: String,
        context: Context,
        request: SetValue
    ): InvocationResponse {
        if (operationHandle != Handles.SET_VALUE_OPERATION.string) {
            return context.sendUnsuccessfulReport(InvocationState(InvocationState.EnumType.Fail))
        }
        logger.info { "Successfully called setValue: $operationHandle" }

        return context.sendSuccessfulReport(InvocationState(InvocationState.EnumType.Fin))
    }
}
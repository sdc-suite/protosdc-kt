package org.somda.protosdc.device.consumer.sco

import kotlinx.coroutines.channels.ReceiveChannel
import org.somda.protosdc.model.biceps.AbstractSetResponseOneOf
import org.somda.protosdc.model.biceps.OperationInvokedReport

/**
 * Expresses that an SCO invocation did not end up in the delivery of a successful report.
 */
public class UnsuccessfulScoTransactionException(message: String) : Exception(message)

/**
 * Definition of an SDC transaction to track incoming operation invoked report parts.
 */
public interface ScoTransaction {
    public companion object {
        /**
         * The maximum number of reports per transaction.
         */
        public const val MAX_REPORTS: Int = 3
    }

    /**
     * Gets the transaction id.
     *
     * @return the transaction id of this [ScoTransaction].
     */
    public fun transactionId(): Int

    /**
     * Gets all reports received so far.
     *
     * @return snapshot of received reports as a copy.
     */
    public suspend fun reports(): List<OperationInvokedReport.ReportPart>

    /**
     * Gets set response message.
     *
     * @return a copy of the set response message of this [ScoTransaction].
     */
    public fun response(): AbstractSetResponseOneOf

    /**
     * Starts waiting for a final report.
     *
     * A report is final if [OperationInvokedReport.ReportPart.isFinalReport] holds true.
     *
     * The waiting time can be limited by calling [kotlinx.coroutines.withTimeout].
     *
     * @return a list that holds all received reports including the final one.
     */
    public suspend fun waitForFinalReport(): List<OperationInvokedReport.ReportPart>

    /**
     * Starts waiting for a final successful report.
     *
     * A report is final if [OperationInvokedReport.ReportPart.isFinalReport] holds true.
     * A report is successful if [OperationInvokedReport.ReportPart.isSuccess] holds true.
     *
     * The waiting time can be limited by calling [kotlinx.coroutines.withTimeout].
     *
     * @return a list that holds all received reports including the final one or a failed [Result] holding a
     * [UnsuccessfulScoTransactionException].
     */
    public suspend fun waitForSuccess(): Result<List<OperationInvokedReport.ReportPart>>

    /**
     * Gets a [ReceiveChannel] to get notified on incoming reports.
     *
     * @return the [ReceiveChannel] with a capacity of [MAX_REPORTS] elements.
     */
    public fun reportReceiver(): ReceiveChannel<OperationInvokedReport.ReportPart>
}
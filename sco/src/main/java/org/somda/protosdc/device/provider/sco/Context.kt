package org.somda.protosdc.device.provider.sco

import kotlinx.coroutines.channels.SendChannel
import org.somda.protosdc.biceps.common.MdibVersion
import org.somda.protosdc.biceps.provider.access.LocalMdibAccess
import org.somda.protosdc.common.DeviceId
import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.common.InstanceLogger
import org.somda.protosdc.common.Stringified
import org.somda.protosdc.common.stringify
import org.somda.protosdc.model.biceps.AbstractReport
import org.somda.protosdc.model.biceps.AbstractReportPart
import org.somda.protosdc.model.biceps.HandleRef
import org.somda.protosdc.model.biceps.InstanceIdentifier
import org.somda.protosdc.model.biceps.InstanceIdentifierOneOf
import org.somda.protosdc.model.biceps.InvocationError
import org.somda.protosdc.model.biceps.InvocationInfo
import org.somda.protosdc.model.biceps.InvocationState
import org.somda.protosdc.model.biceps.LocalizedText
import org.somda.protosdc.model.biceps.OperationInvokedReport
import org.somda.protosdc.model.biceps.TransactionId

/**
 * Transaction context to be used on incoming set service requests in order to send reports and the initial response.
 *
 * @see InvocationResponse
 */
public class Context constructor(
    @Stringified public val transactionId: TransactionId,
    @Stringified public val operationHandle: String,
    public val invocationSource: InstanceIdentifier,
    public val operationInvokedReportChannel: SendChannel<OperationInvokedReport>,
    public val mdibAccess: LocalMdibAccess,
    instanceId: InstanceId,
    deviceId: DeviceId
) {
    private val logger by InstanceLogger(instanceId(), deviceId())

    // this is used to track whether the last OperationInvokedReport state
    // matches the Responses state, and sends an OperationInvokedReport in case it doesn't
    private var currentReportInvocationState: InvocationState? = null

    /**
     * Creates a successful initial invocation response based on this context.
     *
     * *Creating the response will send an [OperationInvokedReport] matching the response
     * if no report matching this invocation state has been sent before.*
     *
     * @param mdibVersion     the MDIB version that is put to the response message.
     * @param invocationState the invocation state that is put to the response message.
     * The enumeration is not verified.
     * @return the invocation response object.
     */
    public suspend fun createSuccessfulResponse(
        mdibVersion: MdibVersion,
        invocationState: InvocationState
    ): InvocationResponse {
        if (invocationState != currentReportInvocationState) {
            logger.debug {
                "No matching OperationInvokedReport was sent before creating response." +
                        " Sending response as OperationInvokedReport as well." +
                        " Operation: $operationHandle - State: $invocationState"
            }
            sendSuccessfulReport(mdibVersion, invocationState)
        }
        return InvocationResponse(mdibVersion, transactionId, invocationState)
    }

    /**
     * Creates a successful initial invocation response based on this context with latest MDIB version.
     *
     * *Creating the response will send an [OperationInvokedReport] matching the response
     * if no report matching this invocation state has been sent before.*
     *
     * @param invocationState the invocation state that is put to the response message.
     * The enumeration is not verified.
     * @return the invocation response object.
     */
    public suspend fun createSuccessfulResponse(invocationState: InvocationState): InvocationResponse {
        return InvocationResponse(mdibAccess.mdibVersion(), transactionId, invocationState)
    }

    /**
     * Creates an unsuccessful initial invocation response based on this context.
     *
     *
     * *Creating the response will send an [OperationInvokedReport] matching the response
     * if no report matching this invocation state has been sent before.*
     *
     * @param mdibVersion            the MDIB version that is put to the response message.
     * @param invocationState        the invocation state that is put to the response message.
     * The enumeration is not verified.
     * @param invocationError        the specified error.
     * @param invocationErrorMessage a human-readable text to describe the error.
     * @return the invocation response object.
     */
    public fun createUnsuccessfulResponse(
        mdibVersion: MdibVersion,
        invocationState: InvocationState,
        invocationError: InvocationError? = InvocationError(InvocationError.EnumType.Oth),
        invocationErrorMessage: List<LocalizedText> = listOf()
    ): InvocationResponse = InvocationResponse(mdibVersion, transactionId, invocationState, invocationError, invocationErrorMessage)

    /**
     * Creates an unsuccessful initial invocation response based on this context with latest MDIB version.
     *
     *
     * *Creating the response will send an [OperationInvokedReport] matching the response
     * if no report matching this invocation state has been sent before.*
     *
     * @param invocationState        the invocation state that is put to the response message.
     * The enumeration is not verified.
     * @param invocationError        the specified error.
     * @param invocationErrorMessage a human-readable text to describe the error.
     * @return the invocation response object.
     */
    public suspend fun createUnsuccessfulResponse(
        invocationState: InvocationState,
        invocationError: InvocationError = InvocationError(InvocationError.EnumType.Oth),
        invocationErrorMessage: List<LocalizedText> = listOf()
    ): InvocationResponse = InvocationResponse(
        mdibAccess.mdibVersion(),
        transactionId,
        invocationState,
        invocationError,
        invocationErrorMessage
    )

    /**
     * Sends a successful operation invoked report.
     *
     * @param mdibVersion     the MDIB version that is put to the notification message.
     * @param invocationState the invocation state that is put to the notification message.
     * The enumeration is not verified.
     */
    public suspend fun sendSuccessfulReport(
        mdibVersion: MdibVersion,
        invocationState: InvocationState
    ): InvocationResponse {
        sendReport(mdibVersion, invocationState, null, listOf(), null)
        return createSuccessfulResponse(mdibVersion, invocationState)
    }

    /**
     * Sends a successful operation invoked report and returns the initial response.
     *
     * @param mdibVersion     the MDIB version that is put to the notification message.
     * @param invocationState the invocation state that is put to the notification message.
     * The enumeration is not verified.
     * @param operationTarget the operation target if available or null if unknown/irrelevant.
     * @return the report that was sent as [InvocationResponse].
     */
    public suspend fun sendSuccessfulReport(
        mdibVersion: MdibVersion,
        invocationState: InvocationState,
        operationTarget: String?
    ): InvocationResponse {
        sendReport(mdibVersion, invocationState, null, listOf(), operationTarget)
        return createSuccessfulResponse(mdibVersion, invocationState)
    }

    /**
     * Sends a successful operation invoked report with latest MDIB version and returns the initial response.
     *
     * @param invocationState the invocation state that is put to the notification message.
     * The enumeration is not verified.
     * @param operationTarget the operation target if available or null if unknown/irrelevant.
     * @return the report that was sent as [InvocationResponse].
     */
    public suspend fun sendSuccessfulReport(
        invocationState: InvocationState,
        operationTarget: String?
    ): InvocationResponse = mdibAccess.mdibVersion().let {
        sendReport(mdibAccess.mdibVersion(), invocationState, null, listOf(), operationTarget)
        createSuccessfulResponse(it, invocationState)
    }

    /**
     * Sends a successful operation invoked report with latest MDIB version and returns the initial response.
     *
     * @param invocationState the invocation state that is put to the notification message.
     * The enumeration is not verified.
     * @return the report that was sent as [InvocationResponse].
     */
    public suspend fun sendSuccessfulReport(invocationState: InvocationState): InvocationResponse =
        mdibAccess.mdibVersion().let {
            sendReport(it, invocationState, null, listOf(), null)
            createSuccessfulResponse(it, invocationState)
        }

    /**
     * Sends an unsuccessful operation invoked report and returns the initial response.
     *
     * @param mdibVersion            the MDIB version that is put to the notification message.
     * @param invocationState        the invocation state that is put to the notification message.
     * The enumeration is not verified.
     * @param invocationError        the specified error.
     * @param invocationErrorMessage a human-readable text to describe the error.
     * @return the report that was sent as [InvocationResponse].
     */
    public suspend fun sendUnsuccessfulReport(
        mdibVersion: MdibVersion,
        invocationState: InvocationState,
        invocationError: InvocationError? = InvocationError(InvocationError.EnumType.Oth),
        invocationErrorMessage: List<LocalizedText> = listOf()
    ): InvocationResponse {
        sendReport(mdibVersion, invocationState, invocationError, invocationErrorMessage, null)
        return createUnsuccessfulResponse(mdibVersion, invocationState, invocationError, invocationErrorMessage)
    }

    /**
     * Sends an unsuccessful operation invoked report with latest MDIB version and returns the initial response.
     *
     * @param invocationState        the invocation state that is put to the notification message.
     *                               The enumeration is not verified.
     * @param invocationError        the specified error.
     * @param invocationErrorMessage a human-readable text to describe the error.
     * @return the report that was sent as [InvocationResponse].
     */
    public suspend fun sendUnsuccessfulReport(
        invocationState: InvocationState,
        invocationError: InvocationError = InvocationError(InvocationError.EnumType.Oth),
        invocationErrorMessage: List<LocalizedText> = listOf()
    ): InvocationResponse = mdibAccess.mdibVersion().let {
        sendReport(it, invocationState, invocationError, invocationErrorMessage, null)
        createUnsuccessfulResponse(it, invocationState, invocationError, invocationErrorMessage)
    }

    private suspend fun sendReport(
        mdibVersion: MdibVersion,
        invocationState: InvocationState,
        invocationError: InvocationError?,
        invocationErrorMessage: List<LocalizedText>,
        operationTarget: String?
    ) {
        currentReportInvocationState = invocationState
        val invocationInfo = InvocationInfo(
            invocationState = invocationState,
            transactionId = transactionId,
            invocationError = invocationError,
            invocationErrorMessage = invocationErrorMessage
        )
        val reportPart = OperationInvokedReport.ReportPart(
            abstractReportPart = AbstractReportPart(),
            operationHandleRefAttr = HandleRef(operationHandle),
            operationTargetAttr = operationTarget?.let { HandleRef(operationTarget) },
            invocationSource = InstanceIdentifierOneOf.ChoiceInstanceIdentifier(invocationSource),
            invocationInfo = invocationInfo
        )


        val operationInvokedReport = OperationInvokedReport(
            abstractReport = AbstractReport(mdibVersionGroupAttr = mdibVersion.toMdibVersionGroup()),
            reportPart = listOf(reportPart)
        )

        try {
            operationInvokedReportChannel.send(operationInvokedReport)
        } catch (e: Exception) {
            logger.warn {
                "Could not deliver operation invoked report notification of transaction $transactionId " +
                        "with invocation state $invocationState"
            }
        }
    }

    /**
     * Returns the last [InvocationState] for which an [OperationInvokedReport] was sent out.
     *
     * @return The last [InvocationState] sent as a report.
     */
    public fun getCurrentReportInvocationState(): InvocationState? {
        return currentReportInvocationState
    }

    override fun toString(): String = stringify()
}
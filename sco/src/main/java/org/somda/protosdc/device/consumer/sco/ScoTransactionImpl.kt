package org.somda.protosdc.device.consumer.sco

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.isActive
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.somda.protosdc.model.biceps.AbstractSetResponseOneOf
import org.somda.protosdc.model.biceps.OperationInvokedReport
import kotlin.coroutines.coroutineContext

/**
 * Default implementation of [ScoTransaction].
 *
 * In addition to the [ScoTransaction] interface, this class support adding reports to the transaction by using
 * [ScoTransactionImpl.addReport].
 *
 */
public class ScoTransactionImpl(private val response: AbstractSetResponseOneOf) : ScoTransaction {
    private val reportWaitChannel = Channel<OperationInvokedReport.ReportPart>(ScoTransaction.MAX_REPORTS)
    private val reportReceiverChannel = Channel<OperationInvokedReport.ReportPart>(ScoTransaction.MAX_REPORTS)
    private val collectedReports = mutableListOf<OperationInvokedReport.ReportPart>()
    private val mutex = Mutex()

    override fun transactionId(): Int = response.invocationInfo().transactionId.unsignedInt

    override suspend fun reports(): List<OperationInvokedReport.ReportPart> = mutex.withLock {
        collectedReports.toList()
    }

    override fun response(): AbstractSetResponseOneOf = response

    /**
     * Adds a report to the transaction.
     *
     * The report will be distributed to [ScoTransaction.reportReceiver] and
     * triggers [ScoTransaction.waitForFinalReport].
     */
    public fun addReport(report: OperationInvokedReport.ReportPart) {
        reportWaitChannel.trySend(report)
        reportReceiverChannel.trySend(report)
    }

    override suspend fun waitForFinalReport(): List<OperationInvokedReport.ReportPart> {
        mutex.withLock {
            if (collectedReports.any { it.isFinalReport() }) {
                return collectedReports.toList()
            }
        }

        while (coroutineContext.isActive) {
            reportWaitChannel.receive().let {
                mutex.withLock {
                    collectedReports.add(it)
                    if (it.isFinalReport()) {
                        return collectedReports.toList()
                    }
                }
            }
        }

        throw IllegalStateException("Coroutine context has ended before final report could be received")
    }

    override suspend fun waitForSuccess(): Result<List<OperationInvokedReport.ReportPart>> = runCatching {
        waitForFinalReport().also {
            if (!it.last().isSuccess()) {
                throw UnsuccessfulScoTransactionException(
                    "SCO transaction ${transactionId()} finished with a failure: " +
                            it.last().invocationInfo.invocationState
                )
            }
        }
    }

    override fun reportReceiver(): Channel<OperationInvokedReport.ReportPart> = reportReceiverChannel
}
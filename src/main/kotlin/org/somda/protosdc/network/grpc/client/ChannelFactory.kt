package org.somda.protosdc.network.grpc.client

import io.grpc.Channel
import io.grpc.netty.shaded.io.grpc.netty.GrpcSslContexts
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder
import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.common.InstanceLogger
import org.somda.protosdc.crypto.CryptoStore
import org.somda.protosdc.crypto.CryptoUtil
import java.net.InetSocketAddress
import javax.annotation.Nullable
import javax.inject.Inject
import javax.inject.Named

/**
 * Creates channels with TLS support.
 */
internal class ChannelFactory @Inject constructor(
    @Nullable private val cryptoStore: CryptoStore?,
    instanceId: InstanceId
) {
    private companion object : Logging

    private val logger by InstanceLogger(instanceId())

    val secured = cryptoStore != null

    /**
     * Creates a channel that binds on a specific address.
     *
     * @param address to be bound.
     * @return a [Channel] with activated TLS if configured accordingly.
     */
    fun create(address: InetSocketAddress): Channel {
        logger.info { "Connecting to address $address" }
        val channelBuilder = NettyChannelBuilder.forAddress(address)

        val keyStore = CryptoUtil.loadKeyStore(cryptoStore)
        if (keyStore == null) {
            logger.warn { "No key store is found" }
        }
        val trustStore = CryptoUtil.loadTrustStore(cryptoStore)
        if (trustStore == null) {
            logger.warn { "No trust store is found" }
        }

        if (keyStore == null || trustStore == null) {
            logger.warn { "Fallback to plain text since trust and/or key store are missing" }
            channelBuilder.usePlaintext()
        } else {
            channelBuilder.sslContext(
                GrpcSslContexts.forClient().keyManager(keyStore).trustManager(trustStore).build()
            )
        }

        return channelBuilder.build()
    }
}
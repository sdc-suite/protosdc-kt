package org.somda.protosdc.device.common

private fun servicePrefix(): String = "org.somda.protosdc.service."

internal enum class ServiceType(val typeName: String) {
    GET_SERVICE(servicePrefix() + "Get"),
    SET_SERVICE(servicePrefix() + "Set"),
    MDIB_REPORTING_SERVICE(servicePrefix() + "MdibReporting"),
}
package org.somda.protosdc.device.common


private fun actionPrefix(): String = "org.somda.protosdc.biceps.action."

/**
 * Action qualifiers for protoSDC BICEPS messages.
 */
internal enum class Action(val stringRepresentation: String) {
    GET_MDIB(actionPrefix() + "GetMdib"),
    GET_MDIB_RESPONSE(actionPrefix() + "GetMdibResponse"),

    GET_MD_DESCRIPTION(actionPrefix() + "GetMdDescription"),
    GET_MD_DESCRIPTION_RESPONSE(actionPrefix() + "GetMdDescription"),

    GET_MD_STATE(actionPrefix() + "GetMdState"),
    GET_MD_STATE_RESPONSE(actionPrefix() + "GetMdState"),

    GET_CONTEXT_STATES(actionPrefix() + "GetContextStates"),
    GET_CONTEXT_STATES_RESPONSE(actionPrefix() + "GetContextStates"),

    ACTIVATE(actionPrefix() + "Activate"),
    ACTIVATE_RESPONSE(actionPrefix() + "ActivateResponse"),

    SET_ALERT_STATE(actionPrefix() + "SetAlertState"),
    SET_ALERT_STATE_RESPONSE(actionPrefix() + "SetAlertStateResponse"),

    SET_COMPONENT_STATE(actionPrefix() + "SetComponentState"),
    SET_COMPONENT_STATE_RESPONSE(actionPrefix() + "SetComponentStateResponse"),

    SET_CONTEXT_STATE(actionPrefix() + "SetContextState"),
    SET_CONTEXT_STATE_RESPONSE(actionPrefix() + "SetContextStateResponse"),

    SET_METRIC_STATE(actionPrefix() + "SetMetricState"),
    SET_METRIC_STATE_RESPONSE(actionPrefix() + "SetMetricStateResponse"),

    SET_STRING(actionPrefix() + "SetString"),
    SET_STRING_RESPONSE(actionPrefix() + "SetStringResponse"),

    SET_VALUE(actionPrefix() + "SetValue"),
    SET_VALUE_RESPONSE(actionPrefix() + "SetValueResponse"),

    SUBSCRIBE_EPISODIC_REPORT(actionPrefix() + "SubscribeEpisodicReport"),
    EPISODIC_METRIC_REPORT(actionPrefix() + "EpisodicMetricReport"),
    EPISODIC_COMPONENT_REPORT(actionPrefix() + "EpisodicComponentReport"),
    EPISODIC_CONTEXT_REPORT(actionPrefix() + "EpisodicContextReport"),
    EPISODIC_ALERT_REPORT(actionPrefix() + "EpisodicAlertReport"),
    EPISODIC_OPERATIONAL_STATE_REPORT(actionPrefix() + "EpisodicOperationalStateReport"),
    WAVEFORM_STREAM(actionPrefix() + "WaveformStream"),
    DESCRIPTION_MODIFICATION_REPORT(actionPrefix() + "DescriptionModificationReport"),
    SUBSCRIBE_OPERATION_INVOKED_REPORT(actionPrefix() + "SubscribeOperationInvokedReport"),
    OPERATION_INVOKED_REPORT(actionPrefix() + "OperationInvokedReport")
}
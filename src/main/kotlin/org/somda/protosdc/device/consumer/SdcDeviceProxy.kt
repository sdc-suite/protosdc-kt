package org.somda.protosdc.device.consumer

import kotlinx.coroutines.channels.ReceiveChannel
import org.somda.protosdc.biceps.common.access.ObservableMdibAccess
import org.somda.protosdc.common.Service
import org.somda.protosdc.device.consumer.sco.SetServiceAccess
import org.somda.protosdc.metadata.consumer.Metadata
import org.somda.protosdc.proto.model.discovery.Endpoint

public data class ShutDownInfo(val endpoint: Endpoint, val reason: Throwable)

public interface SdcDeviceProxy : Service {
    public interface Factory {
        public fun create(metadata: Metadata): SdcDeviceProxy
    }

    public suspend fun metadata(): Metadata

    public suspend fun endpointIdentifier(): String

    public suspend fun mdibAccess(): ObservableMdibAccess

    public suspend fun setService(): SetServiceAccess?

    public suspend fun subscribeShutDown(): ReceiveChannel<ShutDownInfo>
}
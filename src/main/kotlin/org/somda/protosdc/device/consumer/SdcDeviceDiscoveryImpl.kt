package org.somda.protosdc.device.consumer

import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.common.InstanceLogger
import org.somda.protosdc.common.ServiceDelegate
import org.somda.protosdc.common.ServiceDelegateImpl
import org.somda.protosdc.common.awaitWithIoDispatcher
import org.somda.protosdc.device.common.Constants
import org.somda.protosdc.discovery.consumer.DiscoveryConsumer
import org.somda.protosdc.metadata.consumer.MetadataConsumer
import org.somda.protosdc.network.udp.SocketInfo
import org.somda.protosdc.network.udp.Udp
import org.somda.protosdc.proto.model.discovery.Endpoint
import org.somda.protosdc.proto.model.discovery.SearchFilter
import java.net.InetAddress
import java.net.URI

internal class SdcDeviceDiscoveryImpl @AssistedInject constructor(
    @Assisted private val udp: Udp,
    @Assisted private val discoveryProxyAddress: URI?,
    private val discoveryConsumerFactory: DiscoveryConsumer.Factory,
    private val metadataConsumerFactory: MetadataConsumer.Factory,
    private val sdcDeviceProxyFactory: SdcDeviceProxy.Factory,
    instanceId: InstanceId
) : SdcDeviceDiscovery, ServiceDelegate by ServiceDelegateImpl() {
    private val logger by InstanceLogger(instanceId())

    private lateinit var discoveryConsumer: DiscoveryConsumer

    @AssistedFactory
    interface Factory {
        fun create(
            udp: Udp,
            discoveryProxyAddress: URI?
        ): SdcDeviceDiscoveryImpl
    }

    init {
        onStartUp {
            val socketInfo = awaitWithIoDispatcher {
                SocketInfo(InetAddress.getByName(Constants.DISCOVERY_IP_ADDRESS), Constants.DISCOVERY_PORT)
            }.getOrThrow()

            discoveryConsumer = discoveryConsumerFactory
                .create(udp.messageQueue, discoveryProxyAddress)
                .apply {
                    start()
                }
        }

        onShutDown {
            discoveryConsumer.stop()
        }
    }

    override suspend fun connect(endpoint: Endpoint): SdcDeviceProxy {
        return endpoint.physicalAddressList.firstNotNullOf {
            runCatching {
                val metadataConsumer = metadataConsumerFactory.create(URI.create(it.value))
                metadataConsumer.start()
                metadataConsumer.metadata()
            }.onFailure {
                logger.warn { "Could not retrieve endpoint metadata: ${it.message}" }
                logger.trace(it) { "Could not retrieve endpoint metadata: ${it.message}" }
            }.getOrNull()
        }.let { metadata ->
            sdcDeviceProxyFactory.create(metadata).let {
                it.start()
                it
            }
        }
    }

    override suspend fun subscribe() = discoveryConsumer.subscribe()

    override suspend fun searchAsync(searchFilters: Collection<SearchFilter>, maxResults: Int) =
        discoveryConsumer.searchAsync(searchFilters, maxResults)

    override suspend fun searchAsync(searchFilter: SearchFilter, maxResults: Int) =
        discoveryConsumer.searchAsync(searchFilter, maxResults)

    override suspend fun searchAsync(searchFilter: SearchFilter) =
        discoveryConsumer.searchAsync(searchFilter)

    override suspend fun searchByEndpointIdentifierAsync(endpointIdentifier: String) =
        discoveryConsumer.searchByEndpointIdentifierAsync(endpointIdentifier)
}
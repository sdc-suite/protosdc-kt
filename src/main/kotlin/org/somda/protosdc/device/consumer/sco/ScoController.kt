package org.somda.protosdc.device.consumer.sco

import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import org.somda.protosdc.DeviceConfig
import org.somda.protosdc.addressing.createFor
import org.somda.protosdc.biceps.common.InvalidWhenBranchException
import org.somda.protosdc.common.DeviceId
import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.common.InstanceLogger
import org.somda.protosdc.device.common.Action
import org.somda.protosdc.model.biceps.AbstractSetOneOf
import org.somda.protosdc.model.biceps.AbstractSetResponseOneOf
import org.somda.protosdc.proto.mapping.biceps.KotlinToProto
import org.somda.protosdc.proto.mapping.biceps.ProtoToKotlin
import org.somda.protosdc.proto.model.*
import kotlin.time.Duration.Companion.seconds

/**
 * Controller class that is responsible for invoking set requests and processing incoming operation invoked reports.
 *
 * Moreover, the SCO controller takes over mapping to and from protobuf.
 */
internal class ScoController @AssistedInject constructor(
    @Assisted private val setServiceStub: SetServiceGrpcKt.SetServiceCoroutineStub,
    deviceConfig: DeviceConfig,
    instanceId: InstanceId
) : SetServiceAccess {
    private val logger by InstanceLogger(instanceId())

    private val operationInvocationDispatcher = OperationInvocationDispatcher(
        deviceConfig.awaitingTransactionDurationInMillis.seconds,
        instanceId,
        DeviceId("") // todo populate device id (see https://gitlab.com/sdc-suite/protosdc/protosdc-kt/-/issues/33)
    )

    @AssistedFactory
    interface Factory {
        fun create(
            setServiceStub: SetServiceGrpcKt.SetServiceCoroutineStub
        ): ScoController
    }

    override suspend fun invokeOperation(request: AbstractSetOneOf): ScoTransaction {
        logger.debug { "Invoke operation with request: $request" }
        val response = sendRequest(request)
        logger.debug { "Received response: $response" }
        return ScoTransactionImpl(response).also {
            operationInvocationDispatcher.registerTransaction(it)
        }
    }

    /**
     * Accepts an operation invoked report and dispatches report parts to SCO transactions.
     *
     * @param report the report to dispatch (note that a report can contain multiple report parts
     * that belong to different transaction).
     */
    suspend fun processOperationInvokedReport(report: OperationInvokedReportStream) {
        operationInvocationDispatcher.dispatchReport(
            ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_OperationInvokedReportMsg(
                report.operationInvoked
            )
        )
    }

    private suspend fun sendRequest(setRequest: AbstractSetOneOf): AbstractSetResponseOneOf = when (setRequest) {
        is AbstractSetOneOf.ChoiceActivate -> AbstractSetResponseOneOf.ChoiceActivateResponse(
            ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_ActivateResponseMsg(
                setServiceStub.activate(
                    ActivateRequest.newBuilder()
                        .setAddressing(createFor(Action.ACTIVATE.stringRepresentation))
                        .setPayload(KotlinToProto.map_org_somda_protosdc_model_biceps_Activate(setRequest.value))
                        .build()
                ).payload
            )
        )

        is AbstractSetOneOf.ChoiceSetValue -> AbstractSetResponseOneOf.ChoiceSetValueResponse(
            ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_SetValueResponseMsg(
                setServiceStub.setValue(
                    SetValueRequest.newBuilder()
                        .setAddressing(createFor(Action.SET_VALUE.stringRepresentation))
                        .setPayload(KotlinToProto.map_org_somda_protosdc_model_biceps_SetValue(setRequest.value))
                        .build()
                ).payload
            )
        )

        is AbstractSetOneOf.ChoiceSetString -> AbstractSetResponseOneOf.ChoiceSetStringResponse(
            ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_SetStringResponseMsg(
                setServiceStub.setString(
                    SetStringRequest.newBuilder()
                        .setAddressing(createFor(Action.SET_STRING.stringRepresentation))
                        .setPayload(KotlinToProto.map_org_somda_protosdc_model_biceps_SetString(setRequest.value))
                        .build()
                ).payload
            )
        )

        is AbstractSetOneOf.ChoiceSetAlertState -> AbstractSetResponseOneOf.ChoiceSetAlertStateResponse(
            ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_SetAlertStateResponseMsg(
                setServiceStub.setAlertState(
                    SetAlertStateRequest.newBuilder()
                        .setAddressing(createFor(Action.SET_ALERT_STATE.stringRepresentation))
                        .setPayload(KotlinToProto.map_org_somda_protosdc_model_biceps_SetAlertState(setRequest.value))
                        .build()
                ).payload
            )
        )

        is AbstractSetOneOf.ChoiceSetMetricState -> AbstractSetResponseOneOf.ChoiceSetMetricStateResponse(
            ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_SetMetricStateResponseMsg(
                setServiceStub.setMetricState(
                    SetMetricStateRequest.newBuilder()
                        .setAddressing(createFor(Action.SET_METRIC_STATE.stringRepresentation))
                        .setPayload(KotlinToProto.map_org_somda_protosdc_model_biceps_SetMetricState(setRequest.value))
                        .build()
                ).payload
            )
        )

        is AbstractSetOneOf.ChoiceSetContextState -> AbstractSetResponseOneOf.ChoiceSetContextStateResponse(
            ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_SetContextStateResponseMsg(
                setServiceStub.setContextState(
                    SetContextStateRequest.newBuilder()
                        .setAddressing(createFor(Action.SET_CONTEXT_STATE.stringRepresentation))
                        .setPayload(KotlinToProto.map_org_somda_protosdc_model_biceps_SetContextState(setRequest.value))
                        .build()
                ).payload
            )
        )

        is AbstractSetOneOf.ChoiceSetComponentState -> AbstractSetResponseOneOf.ChoiceSetComponentStateResponse(
            ProtoToKotlin.map_org_somda_protosdc_proto_model_biceps_SetComponentStateResponseMsg(
                setServiceStub.setComponentState(
                    SetComponentStateRequest.newBuilder()
                        .setAddressing(createFor(Action.SET_COMPONENT_STATE.stringRepresentation))
                        .setPayload(KotlinToProto.map_org_somda_protosdc_model_biceps_SetComponentState(setRequest.value))
                        .build()
                ).payload
            )
        )

        else -> throw InvalidWhenBranchException(setRequest)
    }
}
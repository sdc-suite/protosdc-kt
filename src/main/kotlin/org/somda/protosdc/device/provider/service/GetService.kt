package org.somda.protosdc.device.provider.service

import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import org.somda.protosdc.addressing.AddressingValidator
import org.somda.protosdc.addressing.discloseValidationError
import org.somda.protosdc.addressing.replyTo
import org.somda.protosdc.biceps.common.access.MdibAccess
import org.somda.protosdc.biceps.common.handleString
import org.somda.protosdc.device.common.Action
import org.somda.protosdc.device.provider.helper.TreeMapper
import org.somda.protosdc.device.provider.helper.createAbstractGetResponse
import org.somda.protosdc.proto.mapping.biceps.KotlinToProto
import org.somda.protosdc.proto.model.*
import org.somda.protosdc.proto.model.biceps.GetContextStatesResponseMsg
import org.somda.protosdc.proto.model.biceps.GetMdDescriptionResponseMsg
import org.somda.protosdc.proto.model.biceps.GetMdStateResponseMsg
import org.somda.protosdc.proto.model.biceps.GetMdibResponseMsg

internal class GetService @AssistedInject constructor(
    @Assisted private val mdibAccess: MdibAccess,
    private val addressingValidator: AddressingValidator,
    treeMapperFactory: TreeMapper.Factory
) : GetServiceGrpcKt.GetServiceCoroutineImplBase() {
    @AssistedFactory
    interface Factory {
        fun create(mdibAccess: MdibAccess): GetService
    }

    private val treeMapper = treeMapperFactory.create(mdibAccess)

    override suspend fun getContextStates(request: GetContextStatesRequest): GetContextStatesResponse {
        discloseValidationError {
            addressingValidator.create(request.addressing)
                .validateMessageId()
                .validateAction(Action.GET_CONTEXT_STATES.stringRepresentation)
        }

        val handles = request.payload.handleRefList.map { it.string }
        val contextStates = mdibAccess.contextStates()
            .filter { handles.isEmpty() || handles.contains(it.handleString()) }
            .map { KotlinToProto.map_org_somda_protosdc_model_biceps_AbstractContextStateOneOf(it) }

        return GetContextStatesResponse.newBuilder()
            .setAddressing(
                replyTo(
                    request.addressing,
                    Action.GET_CONTEXT_STATES_RESPONSE.stringRepresentation
                )
            )
            .setPayload(
                GetContextStatesResponseMsg.newBuilder()
                    .setAbstractGetResponse(createAbstractGetResponse(mdibAccess))
                    .addAllContextState(contextStates)
            ).build()
    }

    override suspend fun getMdDescription(request: GetMdDescriptionRequest): GetMdDescriptionResponse {
        discloseValidationError {
            addressingValidator.create(request.addressing)
                .validateMessageId()
                .validateAction(Action.GET_MD_DESCRIPTION.stringRepresentation)
        }

        val mdDescription = treeMapper.mapMdDescription(request.payload.handleRefList.map { it.string })

        return GetMdDescriptionResponse.newBuilder()
            .setAddressing(
                replyTo(
                    request.addressing,
                    Action.GET_MD_DESCRIPTION_RESPONSE.stringRepresentation
                )
            )
            .setPayload(
                GetMdDescriptionResponseMsg.newBuilder()
                    .setAbstractGetResponse(createAbstractGetResponse(mdibAccess))
                    .setMdDescription(mdDescription)
            ).build()
    }

    override suspend fun getMdState(request: GetMdStateRequest): GetMdStateResponse {
        discloseValidationError {
            addressingValidator.create(request.addressing)
                .validateMessageId()
                .validateAction(Action.GET_MD_STATE.stringRepresentation)
        }

        val mdState = treeMapper.mapMdState(request.payload.handleRefList.map { it.string })

        return GetMdStateResponse.newBuilder()
            .setAddressing(
                replyTo(
                    request.addressing,
                    Action.GET_MD_STATE_RESPONSE.stringRepresentation
                )
            )
            .setPayload(
                GetMdStateResponseMsg.newBuilder()
                    .setAbstractGetResponse(createAbstractGetResponse(mdibAccess))
                    .setMdState(mdState)
            ).build()
    }

    override suspend fun getMdib(request: GetMdibRequest): GetMdibResponse {
        discloseValidationError {
            addressingValidator.create(request.addressing)
                .validateMessageId()
                .validateAction(Action.GET_MDIB.stringRepresentation)
        }
        val mdib = treeMapper.mapMdib()

        return GetMdibResponse.newBuilder()
            .setAddressing(
                replyTo(
                    request.addressing,
                    Action.GET_MDIB_RESPONSE.stringRepresentation
                )
            )
            .setPayload(
                GetMdibResponseMsg.newBuilder()
                    .setAbstractGetResponse(createAbstractGetResponse(mdibAccess))
                    .setMdib(mdib)
            ).build()
    }
}
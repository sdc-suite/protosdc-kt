package org.somda.protosdc.device.provider

import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import org.somda.protosdc.ProtoSdcConfig
import org.somda.protosdc.biceps.mdibAccessFactory
import org.somda.protosdc.common.ChannelPublisher
import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.common.InstanceLogger
import org.somda.protosdc.common.ServiceDelegate
import org.somda.protosdc.common.ServiceDelegateImpl
import org.somda.protosdc.device.common.ServiceType
import org.somda.protosdc.device.provider.sco.OperationInvocationReceiver
import org.somda.protosdc.device.provider.service.EpisodicReportSet
import org.somda.protosdc.device.provider.service.GetService
import org.somda.protosdc.device.provider.service.MdibReportingService
import org.somda.protosdc.device.provider.service.SetService
import org.somda.protosdc.discovery.provider.DiscoveryProvider
import org.somda.protosdc.metadata.provider.MetadataProvider
import org.somda.protosdc.metadata.provider.MetadataProviderImpl
import org.somda.protosdc.metadata.provider.ServiceMetadata
import org.somda.protosdc.model.biceps.OperationInvokedReport
import org.somda.protosdc.network.grpc.server.Server
import org.somda.protosdc.network.grpc.server.SslSessionInterceptor
import org.somda.protosdc.network.udp.Udp
import org.somda.protosdc.proto.model.common.Uri
import org.somda.protosdc.proto.model.metadata.EndpointMetadata
import java.net.URI

internal class SdcDeviceImpl @AssistedInject internal constructor(
    @Assisted private val udp: Udp,
    @Assisted private val server: Server,
    @Assisted private val endpointIdentifier: String,
    @Assisted private val endpointMetadata: EndpointMetadata,
    @Assisted optionalOperationInvocationReceiver: OperationInvocationReceiver?,
    @Assisted private val discoveryProxyUri: URI?,
    @Assisted private val statisticsCallback: StatisticsCallback? = null,
    discoveryProviderFactory: DiscoveryProvider.Factory,
    metadataProviderFactory: MetadataProviderImpl.Factory,
    mdibReportingServiceFactory: MdibReportingService.Factory,
    getServiceFactory: GetService.Factory,
    setServiceFactory: SetService.Factory,
    protoSdcConfig: ProtoSdcConfig,
    instanceId: InstanceId
) : SdcDevice, ServiceDelegate by ServiceDelegateImpl() {
    @AssistedFactory
    interface Factory {
        fun create(
            udp: Udp,
            server: Server,
            endpointIdentifier: String,
            endpointMetadata: EndpointMetadata,
            operationInvocationReceiver: OperationInvocationReceiver?,
            discoveryProxyUri: URI?,
            statisticsCallback: StatisticsCallback? = null
        ): SdcDeviceImpl
    }

    private val logger by InstanceLogger(instanceId())

    private val operationInvocationReceiver =
        optionalOperationInvocationReceiver ?: object : OperationInvocationReceiver {}
    private val discoveryProvider: DiscoveryProvider
    private val metadataProvider: MetadataProvider
    private val coroutineScopeBinding = CoroutineScope(Dispatchers.Default)

    private val episodicReportPublisher = ChannelPublisher<EpisodicReportSet>("episodicReportPublisher")

    private val operationInvokedReportChannel = Channel<OperationInvokedReport>()

    private val localMdibAccess = mdibAccessFactory {
        config(protoSdcConfig.bicepsConfig)
    }.createLocalMdibAccess()

    private val mdibReportingService =
        mdibReportingServiceFactory.create(localMdibAccess, episodicReportPublisher) { cnt ->
            statisticsCallback?.let { it(Statistics(cnt)) }
        }

    private val getService = getServiceFactory.create(localMdibAccess)
    private val setService = setServiceFactory.create(
        localMdibAccess,
        operationInvocationReceiver,
        operationInvokedReportChannel
    )

    init {
        server.registerService(getService)
        server.registerService(setService)
        server.registerService(mdibReportingService)
        server.registerInterceptor(SslSessionInterceptor())

        val serviceMetadata = ServiceMetadata(
            listOf(
                ServiceType.GET_SERVICE.typeName,
                ServiceType.SET_SERVICE.typeName,
                ServiceType.MDIB_REPORTING_SERVICE.typeName
            ),
            "AllServices",
            server
        )

        discoveryProvider = discoveryProviderFactory.create(
            endpointIdentifier,
            udp.messageQueue,
            discoveryProxyUri
        )

        metadataProvider = metadataProviderFactory.create(
            server,
            discoveryProvider,
            endpointMetadata,
            listOf(serviceMetadata)
        )

        onStartUp {
            coroutineScopeBinding.launch {
                runCatching {
                    mdibReportingService.runEventDistributionInCurrentScope()
                }.onFailure {
                    logger.warn(it) { "Event distribution stopped unexpectedly with: ${it.message}" }
                }
            }

            setService.start()
            metadataProvider.start()
            discoveryProvider.updatePhysicalAddresses(
                listOf(
                    Uri.newBuilder().setValue(server.addressAsUri().toString()).build()
                )
            )
            discoveryProvider.start()
        }

        onShutDown {
            discoveryProvider.stop()
            metadataProvider.stop()
            setService.stop()
        }
    }

    override suspend fun endpointIdentifier(): String = discoveryProvider.endpoint().endpointIdentifier

    override suspend fun mdibAccess() = localMdibAccess

    override suspend fun discoveryProvider() = discoveryProvider

    override suspend fun metadataProvider() = metadataProvider
}
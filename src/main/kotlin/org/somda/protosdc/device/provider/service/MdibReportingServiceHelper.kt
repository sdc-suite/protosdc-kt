package org.somda.protosdc.device.provider.service

import org.somda.protosdc.biceps.common.MdibEntity
import org.somda.protosdc.biceps.common.access.MdibAccess
import org.somda.protosdc.biceps.common.access.MdibAccessEvent
import org.somda.protosdc.device.provider.helper.createAbstractReport
import org.somda.protosdc.proto.mapping.biceps.KotlinToProto
import org.somda.protosdc.proto.model.EpisodicReport
import org.somda.protosdc.proto.model.biceps.AbstractAlertReportMsg
import org.somda.protosdc.proto.model.biceps.AbstractComponentReportMsg
import org.somda.protosdc.proto.model.biceps.AbstractContextReportMsg
import org.somda.protosdc.proto.model.biceps.AbstractMetricReportMsg
import org.somda.protosdc.proto.model.biceps.AbstractOperationalStateReportMsg
import org.somda.protosdc.proto.model.biceps.AbstractReportPartMsg
import org.somda.protosdc.proto.model.biceps.DescriptionModificationReportMsg
import org.somda.protosdc.proto.model.biceps.DescriptionModificationTypeMsg
import org.somda.protosdc.proto.model.biceps.EpisodicAlertReportMsg
import org.somda.protosdc.proto.model.biceps.EpisodicComponentReportMsg
import org.somda.protosdc.proto.model.biceps.EpisodicContextReportMsg
import org.somda.protosdc.proto.model.biceps.EpisodicMetricReportMsg
import org.somda.protosdc.proto.model.biceps.EpisodicOperationalStateReportMsg
import org.somda.protosdc.proto.model.biceps.HandleRefMsg
import org.somda.protosdc.proto.model.biceps.WaveformStreamMsg

internal object MdibReportingServiceHelper {
    suspend fun makeEpisodicAlertReport(
        mdibAccess: MdibAccess,
        event: MdibAccessEvent.AlertStateModification
    ): EpisodicReport {
        return EpisodicReport.newBuilder().setAlert(
            EpisodicAlertReportMsg.newBuilder()
                .setAbstractAlertReport(
                    AbstractAlertReportMsg.newBuilder().apply {
                        setAbstractReport(createAbstractReport(mdibAccess))
                        for (entry in event.groupedBySource()) {
                            addReportPart(
                                AbstractAlertReportMsg.ReportPartMsg.newBuilder()
                                    .setAbstractReportPart(
                                        AbstractReportPartMsg.newBuilder()
                                            .setSourceMds(HandleRefMsg.newBuilder().setString(entry.key))
                                    )
                                    .addAllAlertState(entry.value.map {
                                        KotlinToProto.map_org_somda_protosdc_model_biceps_AbstractAlertStateOneOf(
                                            it.alertState
                                        )
                                    })
                            )
                        }
                    }
                )
        ).build()
    }

    suspend fun makeEpisodicComponentReport(
        mdibAccess: MdibAccess,
        event: MdibAccessEvent.ComponentStateModification
    ): EpisodicReport {
        return EpisodicReport.newBuilder().setComponent(
            EpisodicComponentReportMsg.newBuilder()
                .setAbstractComponentReport(
                    AbstractComponentReportMsg.newBuilder().apply {
                        setAbstractReport(createAbstractReport(mdibAccess))
                        for (entry in event.groupedBySource()) {
                            addReportPart(
                                AbstractComponentReportMsg.ReportPartMsg.newBuilder()
                                    .setAbstractReportPart(
                                        AbstractReportPartMsg.newBuilder()
                                            .setSourceMds(HandleRefMsg.newBuilder().setString(entry.key))
                                    )
                                    .addAllComponentState(entry.value.map {
                                        KotlinToProto.map_org_somda_protosdc_model_biceps_AbstractDeviceComponentStateOneOf(
                                            it.componentState
                                        )
                                    })
                            )
                        }
                    }
                )
        ).build()
    }

    suspend fun makeEpisodicContextReport(
        mdibAccess: MdibAccess,
        event: MdibAccessEvent.ContextStateModification
    ): EpisodicReport {
        return EpisodicReport.newBuilder().setContext(
            EpisodicContextReportMsg.newBuilder()
                .setAbstractContextReport(
                    AbstractContextReportMsg.newBuilder().apply {
                        setAbstractReport(createAbstractReport(mdibAccess))
                        for (entry in event.groupedBySource()) {
                            addReportPart(
                                AbstractContextReportMsg.ReportPartMsg.newBuilder()
                                    .setAbstractReportPart(
                                        AbstractReportPartMsg.newBuilder()
                                            .setSourceMds(HandleRefMsg.newBuilder().setString(entry.key))
                                    )
                                    .addAllContextState(entry.value.map {
                                        KotlinToProto.map_org_somda_protosdc_model_biceps_AbstractContextStateOneOf(
                                            it.contextState
                                        )
                                    })
                            )
                        }
                    }
                )
        ).build()
    }

    suspend fun makeEpisodicMetricReport(
        mdibAccess: MdibAccess,
        event: MdibAccessEvent.MetricStateModification
    ): EpisodicReport {
        return EpisodicReport.newBuilder().setMetric(
            EpisodicMetricReportMsg.newBuilder()
                .setAbstractMetricReport(
                    AbstractMetricReportMsg.newBuilder().apply {
                        setAbstractReport(createAbstractReport(mdibAccess))
                        for (entry in event.groupedBySource()) {
                            addReportPart(
                                AbstractMetricReportMsg.ReportPartMsg.newBuilder()
                                    .setAbstractReportPart(
                                        AbstractReportPartMsg.newBuilder()
                                            .setSourceMds(HandleRefMsg.newBuilder().setString(entry.key))
                                    )
                                    .addAllMetricState(entry.value.map {
                                        KotlinToProto.map_org_somda_protosdc_model_biceps_AbstractMetricStateOneOf(
                                            it.metricState
                                        )
                                    })
                            )
                        }
                    }
                )
        ).build()
    }

    suspend fun makeEpisodicOperationReport(
        mdibAccess: MdibAccess,
        event: MdibAccessEvent.OperationStateModification
    ): EpisodicReport {
        return EpisodicReport.newBuilder().setOperationalState(
            EpisodicOperationalStateReportMsg.newBuilder()
                .setAbstractOperationalStateReport(
                    AbstractOperationalStateReportMsg.newBuilder().apply {
                        setAbstractReport(createAbstractReport(mdibAccess))
                        for (entry in event.groupedBySource()) {
                            addReportPart(
                                AbstractOperationalStateReportMsg.ReportPartMsg.newBuilder()
                                    .setAbstractReportPart(
                                        AbstractReportPartMsg.newBuilder()
                                            .setSourceMds(HandleRefMsg.newBuilder().setString(entry.key))
                                    )
                                    .addAllOperationState(entry.value.map {
                                        KotlinToProto.map_org_somda_protosdc_model_biceps_AbstractOperationStateOneOf(
                                            it.operationState
                                        )
                                    })
                            )
                        }
                    }
                )
        ).build()
    }

    suspend fun makeWaveformStreamReport(
        mdibAccess: MdibAccess,
        event: MdibAccessEvent.WaveformModification
    ): EpisodicReport {
        return EpisodicReport.newBuilder().setWaveform(
            WaveformStreamMsg.newBuilder()
                .setAbstractReport(createAbstractReport(mdibAccess))
                .addAllState(event.modifiedWaveformStates.map {
                    KotlinToProto.map_org_somda_protosdc_model_biceps_RealTimeSampleArrayMetricState(
                        it.waveformState
                    )
                })
        ).build()
    }

    suspend fun makeDescriptionModificationReport(
        mdibAccess: MdibAccess,
        event: MdibAccessEvent.DescriptionModification
    ): EpisodicReport {
        val reportBuilder = DescriptionModificationReportMsg.newBuilder()
            .setAbstractReport(createAbstractReport(mdibAccess))

        val mapFor: (List<MdibEntity>, DescriptionModificationTypeMsg.EnumType) -> Unit = { mdibEntities, changeType ->
            mdibEntities.forEach { mdibEntity ->
                val partBuilder =
                    DescriptionModificationReportMsg.ReportPartMsg.newBuilder()
                        .setAbstractReportPart(
                            AbstractReportPartMsg.newBuilder()
                                .setSourceMds(HandleRefMsg.newBuilder().setString(mdibEntity.sourceMds))
                        )
                partBuilder.addPDescriptor(
                    KotlinToProto.map_org_somda_protosdc_model_biceps_AbstractDescriptorOneOf(
                        mdibEntity.descriptorOneOf
                    )
                )
                mdibEntity.resolveStates().forEach {
                    partBuilder.addState(
                        KotlinToProto.map_org_somda_protosdc_model_biceps_AbstractStateOneOf(
                            it
                        )
                    )
                }
                partBuilder.setModificationTypeAttr(
                    DescriptionModificationTypeMsg.newBuilder()
                        .setEnumType(changeType)
                )
                mdibEntity.resolveParent()?.let {
                    partBuilder.setParentDescriptorAttr(
                        HandleRefMsg.newBuilder().setString(it)
                    )
                }
                reportBuilder.addReportPart(partBuilder)
            }
        }

        mapFor(event.entities.deleted, DescriptionModificationTypeMsg.EnumType.DEL)
        mapFor(event.entities.inserted, DescriptionModificationTypeMsg.EnumType.CRT)
        mapFor(event.entities.updated, DescriptionModificationTypeMsg.EnumType.UPT)

        return EpisodicReport.newBuilder().setDescription(reportBuilder).build()
    }
}
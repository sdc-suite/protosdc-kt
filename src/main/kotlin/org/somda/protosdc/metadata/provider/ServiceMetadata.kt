package org.somda.protosdc.metadata.provider

import org.somda.protosdc.network.grpc.server.Server

internal data class ServiceMetadata(
    val types: Collection<String>,
    val serviceIdentifier: String,
    val server: Server
)
package org.somda.protosdc.metadata.provider

import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.runBlocking
import org.somda.protosdc.addressing.AddressingValidator
import org.somda.protosdc.addressing.discloseValidationError
import org.somda.protosdc.addressing.replyTo
import org.somda.protosdc.common.ServiceDelegate
import org.somda.protosdc.common.ServiceDelegateImpl
import org.somda.protosdc.discovery.provider.DiscoveryProvider
import org.somda.protosdc.metadata.common.Action
import org.somda.protosdc.network.grpc.server.Server
import org.somda.protosdc.proto.model.common.Uri
import org.somda.protosdc.proto.model.metadata.*

internal class MetadataProviderImpl @AssistedInject internal constructor(
    @Assisted private val server: Server,
    @Assisted private val discoveryProvider: DiscoveryProvider,
    @Assisted private val metadata: EndpointMetadata,
    @Assisted private val services: List<ServiceMetadata>,
    private val addressingValidator: AddressingValidator
) : MetadataProvider, ServiceDelegate by ServiceDelegateImpl() {

    @AssistedFactory
    interface Factory {
        fun create(
            server: Server,
            discoveryProvider: DiscoveryProvider,
            metadata: EndpointMetadata,
            services: List<ServiceMetadata>
        ): MetadataProviderImpl
    }

    private val metadataResponseBuilder = GetMetadataResponse.newBuilder()

    init {
        server.registerService(MetadataService())

        onStartUp {
            metadataResponseBuilder.addAllService(services.map { serviceMetadata: ServiceMetadata ->
                Service.newBuilder()
                    .addPhysicalAddress(Uri.newBuilder().setValue(serviceMetadata.server.addressAsUri().toString()))
                    .setServiceIdentifier(serviceMetadata.serviceIdentifier)
                    .addAllType(serviceMetadata.types).build()
            }).metadata = metadata
        }
    }

    private inner class MetadataService : MetadataServiceGrpcKt.MetadataServiceCoroutineImplBase() {
        override suspend fun getMetadata(request: GetMetadataRequest): GetMetadataResponse {
            discloseValidationError {
                addressingValidator.create(request.addressing)
                    .validateMessageId()
                    .validateAction(Action.GET_METADATA_REQUEST.stringRepresentation)
            }
            return metadataResponseBuilder.clone()
                .setAddressing(replyTo(request.addressing, Action.GET_METADATA_RESPONSE.stringRepresentation))
                .setEndpoint(discoveryProvider.endpoint()).build()
        }
    }
}
package org.somda.protosdc.metadata.common

private fun actionPrefix(): String = "org.somda.protosdc.metadata.action."

/**
 * Action qualifiers for protoSDC metadata messages.
 */
internal enum class Action(val stringRepresentation: String) {
    /**
     * Indication of a get metadata request message.
     */
    GET_METADATA_REQUEST(actionPrefix() + "GetMetadataRequest"),

    /**
     * Indication of a get metadata response message.
     */
    GET_METADATA_RESPONSE(actionPrefix() + "GetMetadataResponse"),
}
package org.somda.protosdc.metadata.consumer

import io.grpc.Channel
import kotlinx.coroutines.Deferred
import org.somda.protosdc.common.Service
import java.net.URI

/**
 * Definition of the metadata consumer interface.
 */
public interface MetadataConsumer : Service {
    public interface Factory {
        /**
         * Creates an instance that sets up a dedicated channel to send requests.
         *
         * @param physicalAddress address at which the metadata consumer requests metadata.
         */
        public fun create(physicalAddress: URI): MetadataConsumer

        /**
         * Creates an instance that uses the given channel to send requests.
         *
         * @param channel gRPC channel at which the metadata consumers requests metadata.
         */
        public fun create(channel: Channel): MetadataConsumer
    }

    /**
     * Requests the metadata.
     *
     * @return a [Deferred] that provides the requested metadata of the underlying metadata provider.
     */
    public suspend fun metadata(): Metadata
}
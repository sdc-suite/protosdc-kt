package org.somda.protosdc.metadata.consumer

import org.somda.protosdc.proto.model.discovery.Endpoint
import org.somda.protosdc.proto.model.metadata.EndpointMetadata
import org.somda.protosdc.proto.model.metadata.Service

/**
 * Metadata of an endpoint.
 *
 * Comprises:
 *
 * - Endpoint information as also requestable by discovery providers
 * - Metadata of the endpoint including model names, versions, serial number etc.
 * - Available services of the endpoint
 */
public data class Metadata(
    val endpoint: Endpoint,
    val endpointMetadata: EndpointMetadata,
    val services: List<Service>
)
package org.somda.protosdc.discovery.consumer.common

import org.apache.logging.log4j.kotlin.KotlinLogger

internal class DiscoveryLogger(private val logger: KotlinLogger) {
    private companion object {
        const val ADHOC = "Ad-hoc"
        const val MANAGED = "Managed"
    }

    fun debugAdhoc(debug: () -> String, trace: (() -> String)? = null) {
        logDebug(ADHOC, debug, trace)
    }

    fun debugManaged(debug: () -> String, trace: (() -> String)? = null) {
        logDebug(MANAGED, debug, trace)
    }

    fun infoAdhoc(info: () -> String) {
        logger.info { makeText(ADHOC, info()) }
    }

    fun infoManaged(info: () -> String) {
        logger.info { makeText(MANAGED, info()) }
    }

    fun warnAdhoc(e: Throwable? = null, warn: () -> String) {
        if (e != null) {
            logger.warn(e) { makeText(ADHOC, warn()) }
        } else {
            logger.warn { makeText(ADHOC, warn()) }
        }
    }

    fun warnManaged(e: Throwable? = null, warn: () -> String) {
        if (e != null) {
            logger.warn(e) { makeText(MANAGED, warn()) }
        } else {
            logger.warn { makeText(MANAGED, warn()) }
        }
    }

    private fun logDebug(prefix: String, debug: () -> String, trace: (() -> String)?) {
        logger.debug { makeText(prefix, debug()) }
        if (trace != null) {
            logger.trace { makeText(prefix, trace()) }
        }
    }

    private fun makeText(prefix: String, msg: String) = "[$prefix]: $msg"
}

package org.somda.protosdc.discovery.dagger

import dagger.Module
import dagger.Provides
import org.somda.protosdc.discovery.consumer.DiscoveryConsumer
import org.somda.protosdc.discovery.consumer.DiscoveryConsumerImpl
import org.somda.protosdc.discovery.provider.DiscoveryProvider
import org.somda.protosdc.discovery.provider.DiscoveryProviderImpl
import org.somda.protosdc.discovery.proxy.DiscoveryProxy
import org.somda.protosdc.discovery.proxy.DiscoveryProxyImpl
import org.somda.protosdc.network.grpc.server.Server
import org.somda.protosdc.network.udp.UdpMessageQueue
import java.net.URI
import javax.inject.Singleton

@Module
internal abstract class DiscoveryModule {
    companion object {
        @Singleton
        @Provides
        fun discoveryProviderFactory(implFactory: DiscoveryProviderImpl.Factory) =
            object : DiscoveryProvider.Factory {
                override fun create(
                    endpointIdentifier: String,
                    udpMessageQueue: UdpMessageQueue,
                    discoveryProxyAddress: URI?
                ) = implFactory.create(endpointIdentifier, udpMessageQueue, discoveryProxyAddress)

                override fun create(
                    endpointIdentifier: String,
                    udpMessageQueue: UdpMessageQueue,
                ) = implFactory.create(endpointIdentifier, udpMessageQueue, null)
            }

        @Singleton
        @Provides
        fun discoveryConsumerFactory(implFactory: DiscoveryConsumerImpl.Factory) =
            object : DiscoveryConsumer.Factory {
                override fun create(
                    udpMessageQueue: UdpMessageQueue,
                    discoveryProxyAddress: URI?
                ) = implFactory.create(udpMessageQueue, discoveryProxyAddress)

                override fun create(
                    udpMessageQueue: UdpMessageQueue
                ) = implFactory.create(udpMessageQueue, null)
            }
    }
}
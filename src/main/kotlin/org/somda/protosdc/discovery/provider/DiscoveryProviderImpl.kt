package org.somda.protosdc.discovery.provider

import com.google.protobuf.InvalidProtocolBufferException
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.somda.protosdc.ProtoSdcConfig
import org.somda.protosdc.addressing.AddressingValidator
import org.somda.protosdc.addressing.ValidationException
import org.somda.protosdc.addressing.createFor
import org.somda.protosdc.addressing.replyTo
import org.somda.protosdc.common.*
import org.somda.protosdc.discovery.common.Action
import org.somda.protosdc.discovery.common.UdpProtobufUtil
import org.somda.protosdc.discovery.common.matchSearch
import org.somda.protosdc.discovery.consumer.common.DiscoveryLogger
import org.somda.protosdc.network.grpc.client.ChannelFactory
import org.somda.protosdc.network.udp.UdpMessage
import org.somda.protosdc.network.udp.UdpMessageQueue
import org.somda.protosdc.proto.model.addressing.Addressing
import org.somda.protosdc.proto.model.common.Uri
import org.somda.protosdc.proto.model.discovery.*
import java.io.ByteArrayInputStream
import java.net.InetSocketAddress
import java.net.URI
import java.time.Duration
import kotlin.time.Duration.Companion.milliseconds

internal class DiscoveryProviderImpl @AssistedInject constructor(
    @Assisted private val endpointIdentifier: String,
    @Assisted private val udpMessageQueue: UdpMessageQueue,
    @Assisted private val discoveryProxyAddress: URI?,
    instanceId: InstanceId,
    protoSdcConfig: ProtoSdcConfig,
    private val grpcChannelFactory: ChannelFactory,
    private val addressingValidator: AddressingValidator
) : DiscoveryProvider, ServiceDelegate by ServiceDelegateImpl() {
    private val startupWait = Duration.ofMillis(protoSdcConfig.maxStartupWaitInMillis)

    @AssistedFactory
    interface Factory {
        fun create(
            endpointIdentifier: String,
            udpMessageQueue: UdpMessageQueue,
            discoveryProxyAddress: URI?
        ): DiscoveryProviderImpl
    }

    private lateinit var incomingUdpMessages: ReceiveChannel<UdpMessage>
    private val mutex = Mutex()
    private val managedModeReady = WakeUpSignal()
    private lateinit var discoveryProxyService: ProviderProxyServiceGrpcKt.ProviderProxyServiceCoroutineStub
    private var helloAndByeChannel = Channel<ProxyNotifyHelloAndByeRequest>()
    private val coroutineScopeBinding = CoroutineScope(Dispatchers.Default)
    private val udpUtil = UdpProtobufUtil(udpMessageQueue)
    private var endpoint = Endpoint.newBuilder().setEndpointIdentifier(endpointIdentifier).build()

    private val logger by InstanceLogger(instanceId())
    private val prefixedLogger = DiscoveryLogger(logger)

    init {
        onStartUp {
            logger.info { "Start discovery provider" }
            connectToProxy()
            incomingUdpMessages = udpMessageQueue.subscribe()
            coroutineScopeBinding.launch(CoroutineName("Incoming discovery provider messages")) {
                while (isActive) {
                    try {
                        val udpMessage = incomingUdpMessages.receive()
                        val discoveryMessage = udpMessage.let {
                            awaitWithIoDispatcher {
                                DiscoveryMessage.parseFrom(
                                    ByteArrayInputStream(
                                        it.data,
                                        0,
                                        it.header.length
                                    )
                                )
                            }.getOrThrow()
                        }

                        if (discoveryMessage.typeCase == DiscoveryMessage.TypeCase.SEARCH_REQUEST) {
                            processSearch(
                                udpMessage,
                                discoveryMessage.addressing,
                                discoveryMessage.searchRequest
                            )
                        }
                    } catch (e: ClosedReceiveChannelException) {
                        logger.info { "Outgoing UdpMessageQueue ended: $e" }
                        break
                    } catch (e: InvalidProtocolBufferException) {
                        logger.trace(e) { "Protocol Buffers message processing error: ${e.message}" }
                    } catch (e: ValidationException) {
                        logger.debug { "Validation exception: ${e.message}" }
                    }
                }
            }

            logger.info { "Discovery provider has started" }

            runCatching {
                logger.info { "Send initial hello" }
                sendHello(mutex.withLock { endpoint.toBuilder().build() })
            }.onFailure {
                logger.warn(it) { "Distribution of initial hello message failed: ${it.message}" }
            }
        }

        onShutDown {
            logger.info { "Shut down discovery provider" }
            incomingUdpMessages.cancel()
            coroutineScopeBinding.launch {
                discoveryProxyAddress?.also {
                    runCatching {
                        helloAndByeChannel.send(
                            ProxyNotifyHelloAndByeRequest.newBuilder()
                                .setAddressing(createFor(Action.BYE.stringRepresentation))
                                .setBye(Bye.newBuilder().setEndpoint(endpoint))
                                .build()
                        )
                        helloAndByeChannel.close()
                    }.onFailure {
                        logger.info(it) { "Distribution of bye message failed: ${it.message}" }
                    }
                }
            }.join()
            logger.info { "Discovery provider shut down" }
        }
    }

    override suspend fun updateScopes(scopes: Collection<Uri>) {
        update(scopes, null)
    }

    override suspend fun updatePhysicalAddresses(physicalAddresses: Collection<Uri>) {
        update(null, physicalAddresses)
    }

    override suspend fun update(scopes: Collection<Uri>?, physicalAddresses: Collection<Uri>?) {
        logger.info { "Update scopes and/or physical addresses" }
        logger.trace(
            "Update scopes and/or physical addresses. New scopes: ${scopes?.joinToString(", ") ?: "none"}; " +
                    "Physical addresses: ${physicalAddresses?.joinToString(",") ?: "none"}"
        )
        val endpointCopy = mutex.withLock {
            Endpoint.newBuilder()
                .setEndpointIdentifier(endpointIdentifier)
                .addAllScope(scopes ?: emptyList())
                .addAllPhysicalAddress((physicalAddresses ?: emptyList()))
                .let {
                    endpoint = it.build()
                    it.build()
                }
        }

        if (isRunning()) {
            logger.info { "Send subsequent hello (metadata changed update)" }
            sendHello(endpointCopy)
        } else {
            logger.info { "Discovery provider not running, no hello will be sent" }
        }
    }

    override suspend fun endpoint(): Endpoint = mutex.withLock { endpoint }

    @Throws(ValidationException::class)
    private suspend fun processSearch(
        requestMessage: UdpMessage,
        addressing: Addressing,
        searchRequest: SearchRequest
    ) {
        validateSearchRequest(addressing)
        prefixedLogger.debugAdhoc(
            { "Incoming search request" },
            { "Incoming search request: $searchRequest" }
        )

        if (!matchSearch(endpoint, searchRequest.searchFilterList)) {
            return
        }

        udpUtil.sendResponse(
            DiscoveryMessage.newBuilder()
                .setAddressing(replyTo(addressing, Action.SEARCH_RESPONSE.stringRepresentation))
                .setSearchResponse(SearchResponse.newBuilder().addEndpoint(endpoint)).build().also {
                    prefixedLogger.debugAdhoc(
                        { "Search matches, send response" },
                        { "Search matches, send response: $it" }
                    )
                },
            requestMessage
        )
    }

    private suspend fun sendHello(endpoint: Endpoint) {
        val hello = Hello.newBuilder().setEndpoint(endpoint)
        prefixedLogger.debugAdhoc(
            { "Send hello" },
            { "Send hello: $hello" }
        )
        udpUtil.sendMulticast(
            DiscoveryMessage.newBuilder()
                .setAddressing(createFor(Action.HELLO.stringRepresentation))
                .setHello(hello)
                .build()
        )

        discoveryProxyAddress?.also {
            helloAndByeChannel.send(
                ProxyNotifyHelloAndByeRequest.newBuilder()
                    .setAddressing(createFor(Action.HELLO.stringRepresentation))
                    .setHello(hello)
                    .build()
            )
        }
    }

    @Throws(ValidationException::class)
    private suspend fun validateSearchRequest(addressing: Addressing) {
        addressingValidator.create(addressing)
            .validateMessageId()
            .validateAction(Action.SEARCH_REQUEST.stringRepresentation)
    }

    private suspend fun connectToProxy() {
        if (discoveryProxyAddress == null) {
            logger.info { "No discovery proxy address set. Skip connecting to discovery proxy" }
            return
        }

        InetSocketAddress(discoveryProxyAddress.host, discoveryProxyAddress.port).also { socketAddr ->
            grpcChannelFactory.create(socketAddr).also { channel ->
                discoveryProxyService = ProviderProxyServiceGrpcKt.ProviderProxyServiceCoroutineStub(channel)
                coroutineScopeBinding.launch {
                    discoveryProxyService.notifyHelloAndBye(helloAndByeChannel
                        .receiveAsFlow()
                        .onStart {
                            prefixedLogger.infoManaged {
                                "No discovery proxy address set. Skip connecting to discovery proxy"
                            }
                            // flow start is done in a separate job, inform start routine about readiness
                            managedModeReady.sendSignal()
                        }
                        .onEach {
                            prefixedLogger.debugManaged(
                                { "Send hello" },
                                { "Send hello: $it" }
                            )
                        }.catch {
                            prefixedLogger.warnManaged(it) {
                                "Exception while notifying hello and bye messages to a discovery proxy: ${it.message}"
                            }
                        })
                }
            }
        }

        runCatching {
            managedModeReady.awaitSignal(startupWait.toMillis().milliseconds) // wait for the notifyHelloAndBye flow to be started
        }.onFailure {
            throw Exception("Managed notifyHelloAndBye() could not be started", it)
        }
    }
}

package org.somda.protosdc.discovery.provider

import org.somda.protosdc.common.Service
import org.somda.protosdc.network.udp.UdpMessageQueue
import org.somda.protosdc.proto.model.common.Uri
import org.somda.protosdc.proto.model.discovery.Endpoint
import java.net.URI

/**
 * Specification of a discovery provider to expose endpoint information on a network.
 *
 * Invoke [update], [updateScopes] or [updatePhysicalAddresses] to send the initial hello.
 */
public interface DiscoveryProvider : Service {
    public interface Factory {
        /**
         * @param endpointIdentifier the endpoint's id.
         * @param udpMessageQueue a UDP message queue used to capture and send UDP messages in ad-hoc mode.
         * @param discoveryProxyAddress optional discovery proxy to be used.
         */
        public fun create(
            endpointIdentifier: String,
            udpMessageQueue: UdpMessageQueue,
            discoveryProxyAddress: URI?
        ): DiscoveryProvider

        /**
         * @param endpointIdentifier the endpoint's id.
         * @param udpMessageQueue a UDP message queue used to capture and send UDP messages in ad-hoc mode.
         */
        public fun create(
            endpointIdentifier: String,
            udpMessageQueue: UdpMessageQueue
        ): DiscoveryProvider
    }

    /**
     * Updates the scopes of the provider, which subsequently triggers distribution of a hello message.
     *
     * @param scopes the updated scopes.
     */
    public suspend fun updateScopes(scopes: Collection<Uri>)

    /**
     * Updates the physical addresses of the provider, which subsequently triggers distribution of a hello message.
     *
     * @param physicalAddresses the updated physical addresses.
     */
    public suspend fun updatePhysicalAddresses(physicalAddresses: Collection<Uri>)

    /**
     * Updates scopes and/or physical addresses of the provider, which subsequently triggers distribution of a hello
     * message.
     *
     * @param scopes updated scopes or null if no scopes have been updated.
     * @param physicalAddresses updated physical addresses of null if no physical addresses have been updated.
     */
    public suspend fun update(scopes: Collection<Uri>?, physicalAddresses: Collection<Uri>?)

    /**
     * Gets the endpoint information as seen by other network peers.
     *
     * @return the endpoint instance assembled from the endpoint identifier and current scopes and physical addresses.
     */
    public suspend fun endpoint(): Endpoint
}

package it.org.somda.protosdc.discovery

import it.org.somda.protosdc.device.dagger.DaggerTestProtoSdcComponent
import it.org.somda.protosdc.network.udp.UdpBindingMock
import it.org.somda.protosdc.network.udp.udpMock
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.somda.protosdc.GrpcConfig
import org.somda.protosdc.ProtoSdcConfig
import org.somda.protosdc.discovery.consumer.DiscoveryConsumer
import org.somda.protosdc.discovery.consumer.DiscoveryEvent
import org.somda.protosdc.discovery.provider.DiscoveryProvider
import org.somda.protosdc.network.udp.UdpMessage
import org.somda.protosdc.network.udp.UdpMessageQueue
import org.somda.protosdc.proto.model.common.Uri
import org.somda.protosdc.proto.model.discovery.ScopeMatcher
import org.somda.protosdc.proto.model.discovery.SearchFilter

class AdHocDiscoveryTest {
    companion object {
        private const val timeout = 2000L
    }

    private val protoSdcComponent = DaggerTestProtoSdcComponent.builder()
        .bind(ProtoSdcConfig(grpc = GrpcConfig(true)))
        .build()
    private lateinit var channel: Channel<UdpMessage>
    private lateinit var udpBindingMock: UdpBindingMock
    private lateinit var udpMessageQueue: UdpMessageQueue
    private val endpointIdentifier = "endpoint-id"

    private lateinit var discoveryProvider: DiscoveryProvider

    private lateinit var discoveryConsumer: DiscoveryConsumer

    @BeforeEach
    fun beforeEach(): Unit = runBlocking {
        channel = Channel(100)
        udpBindingMock = UdpBindingMock(channel).also { it.start() }
        val udpMock = udpMock { messageChannel(channel) }
        udpMessageQueue = udpMock.messageQueue.apply { start() }
        discoveryConsumer = protoSdcComponent.discoveryConsumerFactory().create(
            udpMessageQueue,
            null
        ).also { it.start() }
        discoveryProvider = protoSdcComponent.discoveryProviderFactory().create(
            endpointIdentifier,
            udpMessageQueue,
            null
        ).also { it.start() }
    }

    @AfterEach
    fun afterEach(): Unit = runBlocking {
        channel.cancel()
        udpBindingMock.stop()
        udpMessageQueue.stop()
        discoveryConsumer.stop()
        discoveryProvider.stop()
    }

    @Test
    fun `test ad-hoc mode hello`(): Unit = runBlocking {
        val subscription = discoveryConsumer.subscribe()
        discoveryProvider.update(null, null)
        val event = withTimeout(timeout) {
            subscription.receive()
        }
        check(event is DiscoveryEvent.EndpointEntered)
        assertFalse(event.secure)
        assertEquals(discoveryProvider.endpoint(), event.endpoint)
    }

    @Test
    fun `test ad-hoc mode search based on scopes`(): Unit = runBlocking {
        run {
            val expectedScopes = listOf("http://host/path1", "http://host/path2")
                .map { Uri.newBuilder().setValue(it).build() }

            discoveryProvider.updateScopes(expectedScopes)
            val searchFilters = expectedScopes
                .map {
                    ScopeMatcher.newBuilder()
                        .setAlgorithm(ScopeMatcher.Algorithm.STRING_COMPARE)
                        .setScope(it)
                        .build()
                }
                .map {
                    SearchFilter.newBuilder().setScopeMatcher(it)
                        .build()
                }

            val endpoints = withTimeout(timeout) {
                discoveryConsumer.searchAsync(searchFilters, 1).await()
            }
            assertEquals(1, endpoints.size)
            assertEquals(discoveryProvider.endpoint(), endpoints.first())
        }
        run {
            val expectedScopes = listOf("http://host/path1", "http://host/path2")
                .map { Uri.newBuilder().setValue(it).build() }

            discoveryProvider.updateScopes(expectedScopes)
            val searchFilters = expectedScopes
                .map {
                    ScopeMatcher.newBuilder()
                        .setAlgorithm(ScopeMatcher.Algorithm.RFC_3986)
                        .setScope(it)
                        .build()
                }
                .map {
                    SearchFilter.newBuilder().setScopeMatcher(it)
                        .build()
                }

            val endpoints = withTimeout(timeout) {
                discoveryConsumer.searchAsync(searchFilters, 1).await()
            }
            assertEquals(1, endpoints.size)
            assertEquals(discoveryProvider.endpoint(), endpoints.first())
        }
        run {
            val expectedScopes = listOf("http://host/path1/", "HTTP://host/path2")
                .map { Uri.newBuilder().setValue(it).build() }

            val modifiedExpectedScopes =
                listOf("HTTP://host/path1/", "http://host/path2")
                    .map { Uri.newBuilder().setValue(it).build() }

            discoveryProvider.updateScopes(modifiedExpectedScopes)
            val searchFilters = expectedScopes
                .map {
                    ScopeMatcher.newBuilder()
                        .setAlgorithm(ScopeMatcher.Algorithm.RFC_3986)
                        .setScope(it)
                        .build()
                }
                .map {
                    SearchFilter.newBuilder().setScopeMatcher(it)
                        .build()
                }

            val endpoints = withTimeout(timeout) {
                discoveryConsumer.searchAsync(searchFilters, 1).await()
            }
            assertEquals(1, endpoints.size)
            assertEquals(discoveryProvider.endpoint(), endpoints.first())
        }
        run {
            val searchFilters = listOf(
                SearchFilter.newBuilder().setScopeMatcher(
                    ScopeMatcher.newBuilder()
                        .setScope(Uri.newBuilder().setValue("http://unknown"))
                ).build()
            )

            // assertThrows is not applicable as a suspending caller is required for withTimeout()
            assertTrue(runCatching {
                withTimeout(timeout) {
                    discoveryConsumer.searchAsync(searchFilters, 1).await()
                }
            }.isFailure)
        }
    }

    @Test
    fun `test ad-hoc mode search based on no filter and empty filter`(): Unit = runBlocking {
        run {
            val searchFilters = emptyList<SearchFilter>()
            val endpoints = withTimeout(timeout) {
                discoveryConsumer.searchAsync(searchFilters, 1).await()
            }
            assertEquals(1, endpoints.size)
            assertEquals(discoveryProvider.endpoint(), endpoints.first())
        }
        run {
            val searchFilters = listOf(SearchFilter.newBuilder().build())
            assertTrue(runCatching {
                withTimeout(timeout) {
                    discoveryConsumer.searchAsync(searchFilters, 1).await()
                }
            }.isFailure)
        }
    }

    @Test
    fun `test ad-hoc mode search based on endpoint`(): Unit = runBlocking {
        run {
            val searchFilters = listOf(SearchFilter.newBuilder().setEndpointIdentifier(endpointIdentifier).build())
            val endpoints = withTimeout(timeout) {
                discoveryConsumer.searchAsync(searchFilters, 1).await()
            }
            assertEquals(1, endpoints.size)
            assertEquals(discoveryProvider.endpoint(), endpoints.first())
        }
        run {
            val searchFilters = listOf(SearchFilter.newBuilder().setEndpointIdentifier("<unknown>").build())
            assertTrue(runCatching {
                withTimeout(timeout) {
                    discoveryConsumer.searchAsync(searchFilters, 1).await()
                }
            }.isFailure)
        }
    }
}

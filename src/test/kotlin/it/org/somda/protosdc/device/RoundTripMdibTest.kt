package it.org.somda.protosdc.device

import com.google.protobuf.StringValue
import it.org.somda.protosdc.device.dagger.DaggerTestProtoSdcComponent
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.somda.protosdc.GrpcConfig
import org.somda.protosdc.ProtoSdcConfig
import org.somda.protosdc.biceps.common.AlertStates
import org.somda.protosdc.biceps.common.ComponentStates
import org.somda.protosdc.biceps.common.ContextStates
import org.somda.protosdc.biceps.common.MdibEntity
import org.somda.protosdc.biceps.common.MdibStateModifications
import org.somda.protosdc.biceps.common.MetricStates
import org.somda.protosdc.biceps.common.OperationStates
import org.somda.protosdc.biceps.common.access.MdibAccessEvent
import org.somda.protosdc.biceps.common.descriptorHandleString
import org.somda.protosdc.biceps.common.handleString
import org.somda.protosdc.biceps.common.stateVersion
import org.somda.protosdc.biceps.test.Handles
import org.somda.protosdc.biceps.test.MdibModificationsPresets
import org.somda.protosdc.model.biceps.AbstractContextStateOneOf
import org.somda.protosdc.network.ip.ipVersion
import org.somda.protosdc.network.udp.SocketInfo
import org.somda.protosdc.network.udp.UdpBindingConfig
import org.somda.protosdc.network.udp.udp
import org.somda.protosdc.proto.model.common.Uri
import org.somda.protosdc.proto.model.metadata.EndpointMetadata
import org.somda.protosdc.test.util.DefaultWait
import org.somda.protosdc.test.util.NetworkInterfaceWithMulticastSupport
import java.net.InetAddress
import java.net.InetSocketAddress

class RoundTripMdibTest {
    private val protoSdcComponent = DaggerTestProtoSdcComponent.builder()
        .bind(
            ProtoSdcConfig(
                grpc = GrpcConfig(true)
            )
        )
        .build()

    private val sdcDeviceFactory = protoSdcComponent.sdcDeviceFactory()
    private val networkInterface = NetworkInterfaceWithMulticastSupport.findFor("239.255.255.250")
        ?: throw Exception("No suitable multicast interface found")
    private val address = InetAddress.getByName("239.255.255.250")
    private var udp = createUdp()
    private var server = createServer()
    private val endpointIdentifier = "72bed30b-0000-0000-0000-9ea0a2dd6139"
    private val mdibModificationsPresets = MdibModificationsPresets()
    private var sdcDevice = createDevice()

    private val allHandles = setOf(
        Handles.MDS_0,
        Handles.ALERTSYSTEM_0,
        Handles.ALERTCONDITION_0,
        Handles.ALERTCONDITION_1,
        Handles.ALERTSIGNAL_0,
        Handles.ALERTSIGNAL_1,
        Handles.ALERTSYSTEM_1,
        Handles.CHANNEL_0,
        Handles.METRIC_0,
        Handles.METRIC_1,
        Handles.CHANNEL_1,
        Handles.SYSTEMCONTEXT_0,
        Handles.CONTEXTDESCRIPTOR_0,
        Handles.CONTEXTDESCRIPTOR_1,
        Handles.CONTEXTDESCRIPTOR_2,
        Handles.SCO_0,
        Handles.OPERATION_0,
        Handles.OPERATION_1,
        Handles.CLOCK_0,
        Handles.BATTERY_0,
        Handles.BATTERY_1,
        Handles.MDS_1
    )

    @BeforeEach
    fun beforeEach(): Unit = runBlocking {
        udp = createUdp()
        server = protoSdcComponent.serverFactory().create(InetSocketAddress("localhost", 0))
        sdcDevice = createDevice()
        server.start()
        udp.start()
    }

    @AfterEach
    fun afterEach(): Unit = runBlocking {
        udp.stop()
        server.stop()
    }

    @Test
    fun `test get mdib`(): Unit = runBlocking {
        sdcDevice.mdibAccess().writeDescription(mdibModificationsPresets.fullTree())
        sdcDevice.start()

        val sdcDeviceDiscovery = protoSdcComponent.sdcDeviceDiscoveryFactory()
            .create(udp)
            .also { it.start() }

        val endpoint = withTimeout(DefaultWait.MILLIS) {
            sdcDeviceDiscovery.searchByEndpointIdentifierAsync(endpointIdentifier).await()
        }

        val sdcDeviceProxy = sdcDeviceDiscovery.connect(endpoint.getOrThrow())

        allHandles.forEach { assertTrue(sdcDeviceProxy.mdibAccess().anyEntity(it).isSuccess) }

        sdcDeviceProxy.stop()
        sdcDeviceDiscovery.stop()
        sdcDevice.stop()
    }

    @Test
    fun `test reports`(): Unit = runBlocking {
        sdcDevice.start()
        val sdcDeviceDiscovery = protoSdcComponent.sdcDeviceDiscoveryFactory()
            .create(udp)
            .also { it.start() }

        val endpoint = withTimeout(DefaultWait.MILLIS) {
            sdcDeviceDiscovery.searchByEndpointIdentifierAsync(endpointIdentifier).await()
        }

        val sdcDeviceProxy = sdcDeviceDiscovery.connect(endpoint.getOrThrow())

        val subscription = sdcDeviceProxy.mdibAccess().subscribe()

        // description change
        run {
            allHandles.forEach { assertTrue(sdcDeviceProxy.mdibAccess().anyEntity(it).isFailure) }
            sdcDevice.mdibAccess().writeDescription(mdibModificationsPresets.fullTree())
            val modification = subscription.receive()
            check(modification is MdibAccessEvent.DescriptionModification)
            allHandles.forEach { handle -> assertNotNull(modification.entities.inserted.first { it.handle == handle }) }
            allHandles.forEach { assertTrue(sdcDeviceProxy.mdibAccess().anyEntity(it).isSuccess) }
        }

        // metric change
        run {
            val oldNumericMetric =
                sdcDeviceProxy.mdibAccess().entity(Handles.METRIC_0, MdibEntity.NumericMetric::class).getOrThrow()
            val oldStringMetric =
                sdcDeviceProxy.mdibAccess().entity(Handles.METRIC_1, MdibEntity.StringMetric::class).getOrThrow()

            MdibStateModifications.Metric(
                MetricStates()
                    .add(
                        sdcDevice.mdibAccess().entity(Handles.METRIC_0, MdibEntity.NumericMetric::class)
                            .getOrThrow().state
                    )
                    .add(
                        sdcDevice.mdibAccess().entity(Handles.METRIC_1, MdibEntity.StringMetric::class)
                            .getOrThrow().state
                    )
            ).also {
                sdcDevice.mdibAccess().writeStates(it)
            }

            val modification = subscription.receive()
            check(modification is MdibAccessEvent.MetricStateModification)
            listOf(Handles.METRIC_0, Handles.METRIC_1).forEach { handle ->
                assertNotNull(modification.modifiedMetricStates.firstOrNull {
                    it.state.descriptorHandleString() == handle
                })
            }

            sdcDeviceProxy.mdibAccess().entity(Handles.METRIC_0, MdibEntity.NumericMetric::class)
                .getOrThrow().also {
                    assertEquals(oldNumericMetric.state.stateVersion() + 1, it.state.stateVersion())
                }

            sdcDeviceProxy.mdibAccess().entity(Handles.METRIC_1, MdibEntity.StringMetric::class)
                .getOrThrow().also {
                    assertEquals(oldStringMetric.state.stateVersion() + 1, it.state.stateVersion())
                }
        }

        // operation change
        run {
            val oldActivate =
                sdcDeviceProxy.mdibAccess().entity(Handles.OPERATION_0, MdibEntity.ActivateOperation::class)
                    .getOrThrow()
            val oldSetString =
                sdcDeviceProxy.mdibAccess().entity(Handles.OPERATION_1, MdibEntity.SetStringOperation::class)
                    .getOrThrow()

            MdibStateModifications.Operation(
                OperationStates()
                    .add(
                        sdcDevice.mdibAccess().entity(Handles.OPERATION_0, MdibEntity.ActivateOperation::class)
                            .getOrThrow().state
                    )
                    .add(
                        sdcDevice.mdibAccess()
                            .entity(Handles.OPERATION_1, MdibEntity.SetStringOperation::class)
                            .getOrThrow().state
                    )
            ).also {
                sdcDevice.mdibAccess().writeStates(it)
            }

            val modification = subscription.receive()
            check(modification is MdibAccessEvent.OperationStateModification)
            listOf(Handles.OPERATION_0, Handles.OPERATION_1).forEach { handle ->
                assertNotNull(modification.modifiedOperationStates.firstOrNull {
                    it.state.descriptorHandleString() == handle
                })
            }

            sdcDeviceProxy.mdibAccess().entity(Handles.OPERATION_0, MdibEntity.ActivateOperation::class)
                .getOrThrow().also {
                    assertEquals(oldActivate.state.stateVersion() + 1, it.state.stateVersion())
                }

            sdcDeviceProxy.mdibAccess().entity(Handles.OPERATION_1, MdibEntity.SetStringOperation::class)
                .getOrThrow().also {
                    assertEquals(oldSetString.state.stateVersion() + 1, it.state.stateVersion())
                }
        }

        // component change
        run {
            val oldMds =
                sdcDeviceProxy.mdibAccess().entity(Handles.MDS_0, MdibEntity.Mds::class).getOrThrow()
            val oldVmd =
                sdcDeviceProxy.mdibAccess().entity(Handles.VMD_0, MdibEntity.Vmd::class).getOrThrow()

            MdibStateModifications.Component(
                ComponentStates()
                    .add(
                        sdcDevice.mdibAccess().entity(Handles.MDS_0, MdibEntity.Mds::class)
                            .getOrThrow().state
                    )
                    .add(
                        sdcDevice.mdibAccess().entity(Handles.VMD_0, MdibEntity.Vmd::class)
                            .getOrThrow().state
                    )
            ).also {
                sdcDevice.mdibAccess().writeStates(it)
            }

            val modification = subscription.receive()
            check(modification is MdibAccessEvent.ComponentStateModification)
            listOf(Handles.MDS_0, Handles.VMD_0).forEach { handle ->
                assertNotNull(modification.modifiedComponentStates.firstOrNull {
                    it.state.descriptorHandleString() == handle
                })
            }

            sdcDeviceProxy.mdibAccess().entity(Handles.MDS_0, MdibEntity.Mds::class)
                .getOrThrow().also {
                    assertEquals(oldMds.state.stateVersion() + 1, it.state.stateVersion())
                }

            sdcDeviceProxy.mdibAccess().entity(Handles.VMD_0, MdibEntity.Vmd::class)
                .getOrThrow().also {
                    assertEquals(oldVmd.state.stateVersion() + 1, it.state.stateVersion())
                }
        }

        // alert change
        run {
            val oldAlertSystem =
                sdcDeviceProxy.mdibAccess().entity(Handles.ALERTSYSTEM_0, MdibEntity.AlertSystem::class)
                    .getOrThrow()
            val oldAlertCondition =
                sdcDeviceProxy.mdibAccess().entity(Handles.ALERTCONDITION_0, MdibEntity.AlertCondition::class)
                    .getOrThrow()

            MdibStateModifications.Alert(
                AlertStates()
                    .add(
                        sdcDevice.mdibAccess().entity(Handles.ALERTSYSTEM_0, MdibEntity.AlertSystem::class)
                            .getOrThrow().state
                    )
                    .add(
                        sdcDevice.mdibAccess()
                            .entity(Handles.ALERTCONDITION_0, MdibEntity.AlertCondition::class)
                            .getOrThrow().state
                    )
            ).also {
                sdcDevice.mdibAccess().writeStates(it)
            }

            val modification = subscription.receive()
            check(modification is MdibAccessEvent.AlertStateModification)
            listOf(Handles.ALERTSYSTEM_0, Handles.ALERTCONDITION_0).forEach { handle ->
                assertNotNull(modification.modifiedAlertStates.firstOrNull {
                    it.state.descriptorHandleString() == handle
                })
            }

            sdcDeviceProxy.mdibAccess().entity(Handles.ALERTSYSTEM_0, MdibEntity.AlertSystem::class)
                .getOrThrow().also {
                    assertEquals(oldAlertSystem.state.stateVersion() + 1, it.state.stateVersion())
                }

            sdcDeviceProxy.mdibAccess().entity(Handles.ALERTCONDITION_0, MdibEntity.AlertCondition::class)
                .getOrThrow().also {
                    assertEquals(oldAlertCondition.state.stateVersion() + 1, it.state.stateVersion())
                }
        }

        // context change
        run {
            val oldPatientContext =
                sdcDeviceProxy.mdibAccess()
                    .contextState(Handles.CONTEXT_0, AbstractContextStateOneOf.ChoicePatientContextState::class)
                    .getOrThrow()
            val oldEnsembleContext =
                sdcDeviceProxy.mdibAccess()
                    .contextState(Handles.CONTEXT_2, AbstractContextStateOneOf.ChoiceEnsembleContextState::class)
                    .getOrThrow()

            MdibStateModifications.Context(
                ContextStates()
                    .add(
                        sdcDevice.mdibAccess().contextState(
                            Handles.CONTEXT_0,
                            AbstractContextStateOneOf.ChoicePatientContextState::class
                        )
                            .getOrThrow()
                    )
                    .add(
                        sdcDevice.mdibAccess().contextState(
                            Handles.CONTEXT_2,
                            AbstractContextStateOneOf.ChoiceEnsembleContextState::class
                        )
                            .getOrThrow()
                    )
            ).also {
                sdcDevice.mdibAccess().writeStates(it)
            }

            val modification = subscription.receive()
            check(modification is MdibAccessEvent.ContextStateModification)
            listOf(Handles.CONTEXT_0, Handles.CONTEXT_2).forEach { handle ->
                assertNotNull(modification.modifiedContextStates.firstOrNull {
                    it.contextState.handleString() == handle
                })
            }

            sdcDeviceProxy.mdibAccess()
                .contextState(Handles.CONTEXT_0, AbstractContextStateOneOf.ChoicePatientContextState::class)
                .getOrThrow().also {
                    assertEquals(oldPatientContext.stateVersion() + 1, it.stateVersion())
                }

            sdcDeviceProxy.mdibAccess()
                .contextState(Handles.CONTEXT_2, AbstractContextStateOneOf.ChoiceEnsembleContextState::class)
                .getOrThrow().also {
                    assertEquals(oldEnsembleContext.stateVersion() + 1, it.stateVersion())
                }
        }

        sdcDeviceProxy.stop()
        sdcDeviceDiscovery.stop()
        sdcDevice.stop()
    }

    private fun createUdp() = udp {
        binding(
            UdpBindingConfig(
                networkInterface,
                address.ipVersion(),
                SocketInfo(address, 6464)
            )
        )
    }

    private fun createServer() = protoSdcComponent.serverFactory().create(InetSocketAddress("localhost", 0))

    private fun createDevice() = sdcDeviceFactory.create(
        udp,
        server,
        endpointIdentifier,
        EndpointMetadata.newBuilder().setSerialNumber(StringValue.of("1234-5678-90")).build(),
        null,
        null
    )
}
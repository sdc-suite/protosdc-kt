package it.org.somda.protosdc.device

import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.somda.protosdc.biceps.common.AlertStates
import org.somda.protosdc.biceps.common.ComponentStates
import org.somda.protosdc.biceps.common.MdibEntity
import org.somda.protosdc.biceps.common.MdibStateModifications
import org.somda.protosdc.biceps.mdibAccessFactory
import org.somda.protosdc.biceps.test.Handles
import org.somda.protosdc.biceps.test.MdibModificationsPresets
import org.somda.protosdc.common.ChannelPublisher
import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.device.provider.service.EpisodicReportSet
import org.somda.protosdc.device.provider.service.MdibReportingService

class MdibReportingServiceTest {
    @Test
    fun `test source mds presence`(): Unit = runBlocking {

        val mdibAccess = mdibAccessFactory().createLocalMdibAccess()
        val reportPublisher = ChannelPublisher<EpisodicReportSet>("test", 2)
        val mdibReportingService = MdibReportingService(
            mdibAccess,
            reportPublisher,
            {},
            InstanceId("test")
        )

        val receiver = reportPublisher.subscribe()

        val reportingServiceJob = launch {
            mdibReportingService.runEventDistributionInCurrentScope()
        }

        launch {
            mdibAccess.writeDescription(MdibModificationsPresets().fullTree())
            val descriptionReport = receiver.receive()
            assertNotNull(descriptionReport.generatedReport.description)
        }.join()

        launch {
            val vmd = mdibAccess.anyEntity(Handles.VMD_0).getOrThrow() as MdibEntity.Vmd
            mdibAccess.writeStates(MdibStateModifications.Component(ComponentStates().apply {
                add(vmd.state)
            }))
            val stateReport = receiver.receive()
            assertNotNull(stateReport.generatedReport.component)
            assertEquals(
                Handles.MDS_0,
                stateReport.generatedReport.component.abstractComponentReport.reportPartList.first().abstractReportPart.sourceMds.string
            )
        }.join()

        launch {
            val mds = mdibAccess.anyEntity(Handles.MDS_1).getOrThrow() as MdibEntity.Mds
            mdibAccess.writeStates(MdibStateModifications.Component(ComponentStates().apply {
                add(mds.state)
            }))
            val stateReport = receiver.receive()
            assertNotNull(stateReport.generatedReport.component)
            assertEquals(
                Handles.MDS_1,
                stateReport.generatedReport.component.abstractComponentReport.reportPartList.first().abstractReportPart.sourceMds.string
            )
        }.join()

        launch {
            val alert = mdibAccess.anyEntity(Handles.ALERTCONDITION_0).getOrThrow() as MdibEntity.AlertCondition
            mdibAccess.writeStates(MdibStateModifications.Alert(AlertStates().apply {
                add(alert.state)
            }))
            val stateReport = receiver.receive()
            assertNotNull(stateReport.generatedReport.alert)
            assertEquals(
                Handles.MDS_0,
                stateReport.generatedReport.alert.abstractAlertReport.reportPartList.first().abstractReportPart.sourceMds.string
            )
        }.join()

        reportingServiceJob.cancel()
    }
}
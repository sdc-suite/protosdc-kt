package it.org.somda.protosdc.device

import it.org.somda.protosdc.network.grpc.CryptoStoreGenerator
import org.somda.protosdc.common.ServiceDelegate
import org.somda.protosdc.common.ServiceDelegateImpl
import org.somda.protosdc.dagger.DaggerProtoSdcComponent
import org.somda.protosdc.discovery.proxy.DiscoveryProxy
import java.net.InetSocketAddress

class TestDiscoveryProxy(socketAddress: InetSocketAddress) : ServiceDelegate by ServiceDelegateImpl() {
    private val protoSdcComponent = DaggerProtoSdcComponent.builder()
        .bind(CryptoStoreGenerator.setupDiscoveryProxy())
        .build()

    private val discoveryProxyFactory = protoSdcComponent.discoveryProxyFactory()
    private val server = protoSdcComponent.serverFactory().create(socketAddress)
    private var discoveryProxy = discoveryProxyFactory.create(server)

    init {
        onStartUp {
            discoveryProxy.start()
            server.start()
        }

        onShutDown {
            server.stop()
            discoveryProxy.stop()
        }
    }

    suspend fun address() = server.addressAsUri()

    suspend fun discoveryProxy(): DiscoveryProxy = discoveryProxy
}
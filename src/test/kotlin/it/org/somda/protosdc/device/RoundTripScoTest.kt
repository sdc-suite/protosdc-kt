package it.org.somda.protosdc.device

import com.google.protobuf.StringValue
import it.org.somda.protosdc.device.dagger.DaggerTestProtoSdcComponent
import it.org.somda.protosdc.network.grpc.CryptoStoreGenerator
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.somda.protosdc.biceps.common.DescriptionItem
import org.somda.protosdc.biceps.common.MdibDescriptionModifications
import org.somda.protosdc.biceps.test.Handles
import org.somda.protosdc.biceps.test.MdibModificationsPresets
import org.somda.protosdc.device.consumer.sco.invocationState
import org.somda.protosdc.device.provider.sco.Context
import org.somda.protosdc.device.provider.sco.InvocationResponse
import org.somda.protosdc.device.provider.sco.OperationInvocationReceiver
import org.somda.protosdc.model.biceps.AbstractAlertState
import org.somda.protosdc.model.biceps.AbstractAlertStateOneOf
import org.somda.protosdc.model.biceps.AbstractContextState
import org.somda.protosdc.model.biceps.AbstractContextStateOneOf
import org.somda.protosdc.model.biceps.AbstractDescriptor
import org.somda.protosdc.model.biceps.AbstractDeviceComponentState
import org.somda.protosdc.model.biceps.AbstractDeviceComponentStateOneOf
import org.somda.protosdc.model.biceps.AbstractMetricState
import org.somda.protosdc.model.biceps.AbstractMetricStateOneOf
import org.somda.protosdc.model.biceps.AbstractMultiState
import org.somda.protosdc.model.biceps.AbstractOperationDescriptor
import org.somda.protosdc.model.biceps.AbstractOperationState
import org.somda.protosdc.model.biceps.AbstractSet
import org.somda.protosdc.model.biceps.AbstractSetOneOf
import org.somda.protosdc.model.biceps.AbstractSetStateOperationDescriptor
import org.somda.protosdc.model.biceps.AbstractState
import org.somda.protosdc.model.biceps.Activate
import org.somda.protosdc.model.biceps.ActivateOperationDescriptor
import org.somda.protosdc.model.biceps.ActivateOperationState
import org.somda.protosdc.model.biceps.AlertActivation
import org.somda.protosdc.model.biceps.AlertSystemState
import org.somda.protosdc.model.biceps.ClockState
import org.somda.protosdc.model.biceps.EnsembleContextState
import org.somda.protosdc.model.biceps.Handle
import org.somda.protosdc.model.biceps.HandleRef
import org.somda.protosdc.model.biceps.InvocationState
import org.somda.protosdc.model.biceps.OperatingMode
import org.somda.protosdc.model.biceps.SetAlertState
import org.somda.protosdc.model.biceps.SetAlertStateOperationDescriptor
import org.somda.protosdc.model.biceps.SetAlertStateOperationState
import org.somda.protosdc.model.biceps.SetComponentState
import org.somda.protosdc.model.biceps.SetComponentStateOperationDescriptor
import org.somda.protosdc.model.biceps.SetComponentStateOperationState
import org.somda.protosdc.model.biceps.SetContextState
import org.somda.protosdc.model.biceps.SetContextStateOperationDescriptor
import org.somda.protosdc.model.biceps.SetContextStateOperationState
import org.somda.protosdc.model.biceps.SetMetricState
import org.somda.protosdc.model.biceps.SetMetricStateOperationDescriptor
import org.somda.protosdc.model.biceps.SetMetricStateOperationState
import org.somda.protosdc.model.biceps.SetString
import org.somda.protosdc.model.biceps.SetStringOperationDescriptor
import org.somda.protosdc.model.biceps.SetStringOperationState
import org.somda.protosdc.model.biceps.SetValue
import org.somda.protosdc.model.biceps.SetValueOperationDescriptor
import org.somda.protosdc.model.biceps.SetValueOperationState
import org.somda.protosdc.model.biceps.StringMetricState
import org.somda.protosdc.network.ip.ipVersion
import org.somda.protosdc.network.udp.SocketInfo
import org.somda.protosdc.network.udp.UdpBindingConfig
import org.somda.protosdc.network.udp.udp
import org.somda.protosdc.proto.model.metadata.EndpointMetadata
import org.somda.protosdc.test.util.DefaultWait
import org.somda.protosdc.test.util.NetworkInterfaceWithMulticastSupport
import java.math.BigDecimal
import java.net.InetAddress
import java.net.InetSocketAddress

class ImmediateFinOperationInvocationReceiver : OperationInvocationReceiver {
    override suspend fun activate(operationHandle: String, context: Context, request: Activate) =
        returnSuccess(context)

    override suspend fun setAlertState(operationHandle: String, context: Context, request: SetAlertState) =
        returnSuccess(context)

    override suspend fun setComponentState(operationHandle: String, context: Context, request: SetComponentState) =
        returnSuccess(context)

    override suspend fun setContextState(operationHandle: String, context: Context, request: SetContextState) =
        returnSuccess(context)

    override suspend fun setMetricState(operationHandle: String, context: Context, request: SetMetricState) =
        returnSuccess(context)

    override suspend fun setString(operationHandle: String, context: Context, request: SetString) =
        returnSuccess(context)

    override suspend fun setValue(operationHandle: String, context: Context, request: SetValue) =
        returnSuccess(context)

    private suspend fun returnSuccess(context: Context): InvocationResponse {
        context.sendSuccessfulReport(InvocationState(InvocationState.EnumType.Fin))
        return context.createSuccessfulResponse(InvocationState(InvocationState.EnumType.Fin))
    }
}

class QueuedOperationInvocationReceiver : OperationInvocationReceiver {
    companion object {
        const val CANCEL = "cancel"
        const val SUCCESS = "success"
    }

    override suspend fun setString(operationHandle: String, context: Context, request: SetString): InvocationResponse {
        context.sendSuccessfulReport(InvocationState(InvocationState.EnumType.Wait))
        context.sendSuccessfulReport(InvocationState(InvocationState.EnumType.Start))
        when (request.requestedStringValue) {
            CANCEL -> {
                context.sendUnsuccessfulReport(InvocationState(InvocationState.EnumType.Cnclld))
            }

            SUCCESS -> {
                context.sendSuccessfulReport(InvocationState(InvocationState.EnumType.Fin))
            }

            else -> throw Exception("Unexpected string input")
        }
        return context.createUnsuccessfulResponse(InvocationState(InvocationState.EnumType.Wait))
    }
}

class CancelledManuallyOperationInvocationReceiver : OperationInvocationReceiver {
    override suspend fun setString(operationHandle: String, context: Context, request: SetString): InvocationResponse {
        context.sendUnsuccessfulReport(InvocationState(InvocationState.EnumType.CnclldMan))
        return context.createUnsuccessfulResponse(InvocationState(InvocationState.EnumType.CnclldMan))
    }
}

class RoundTripScoTest {
    private val protoSdcComponent = DaggerTestProtoSdcComponent.builder()
        .bind(CryptoStoreGenerator.setupProvider())
        .build()

    private val sdcDeviceFactory = protoSdcComponent.sdcDeviceFactory()
    private val networkInterface = NetworkInterfaceWithMulticastSupport.findFor("239.255.255.250")
        ?: throw Exception("No suitable multicast interface found")
    private val address = InetAddress.getByName("239.255.255.250")
    private val endpointIdentifier = "72bed30b-0000-0000-0000-9ea0a2dd6139"
    private val mdibModificationsPresets = MdibModificationsPresets()

    private var udp = createUdp()
    private var server = createServer()
    private var sdcDevice = createDevice()

    private val protoSdcClientComponent = DaggerTestProtoSdcComponent.builder()
        .bind(CryptoStoreGenerator.setupConsumer())
        .build()

    @Test
    fun `test activate`(): Unit = runBlocking {
        runImmediateFinScoInvocation(
            DescriptionItem(
                ActivateOperationDescriptor(abstractSetStateOpDescr()),
                ActivateOperationState(abstractOpState()),
                Handles.SCO_0
            ),
            AbstractSetOneOf.ChoiceActivate(
                Activate(
                    AbstractSet(operationHandleRef = HandleRef(Handles.OPERATION_2))
                )
            )
        )
    }

    @Test
    fun `test set string`(): Unit = runBlocking {
        runImmediateFinScoInvocation(
            DescriptionItem(
                SetStringOperationDescriptor(abstractOperationDescriptor()),
                SetStringOperationState(abstractOpState()),
                Handles.SCO_0
            ),
            AbstractSetOneOf.ChoiceSetString(
                SetString(
                    AbstractSet(operationHandleRef = HandleRef(Handles.OPERATION_2)),
                    "Test"
                )
            )
        )
    }

    @Test
    fun `test set value`(): Unit = runBlocking {
        runImmediateFinScoInvocation(
            DescriptionItem(
                SetValueOperationDescriptor(abstractOperationDescriptor()),
                SetValueOperationState(abstractOpState()),
                Handles.SCO_0
            ),
            AbstractSetOneOf.ChoiceSetValue(
                SetValue(
                    AbstractSet(operationHandleRef = HandleRef(Handles.OPERATION_2)),
                    BigDecimal.ONE
                )
            )
        )
    }

    @Test
    fun `test set metric state`(): Unit = runBlocking {
        runImmediateFinScoInvocation(
            DescriptionItem(
                SetMetricStateOperationDescriptor(abstractSetStateOpDescr()),
                SetMetricStateOperationState(abstractOpState()),
                Handles.SCO_0
            ),
            AbstractSetOneOf.ChoiceSetMetricState(
                SetMetricState(
                    AbstractSet(operationHandleRef = HandleRef(Handles.OPERATION_2)),
                    listOf(
                        AbstractMetricStateOneOf.ChoiceStringMetricState(
                            StringMetricState(
                                AbstractMetricState(
                                    AbstractState(descriptorHandleAttr = HandleRef(Handles.MDS_0))
                                )
                            )
                        )
                    )
                )
            )
        )
    }

    @Test
    fun `test set component state`(): Unit = runBlocking {
        runImmediateFinScoInvocation(
            DescriptionItem(
                SetComponentStateOperationDescriptor(abstractSetStateOpDescr()),
                SetComponentStateOperationState(abstractOpState()),
                Handles.SCO_0
            ),
            AbstractSetOneOf.ChoiceSetComponentState(
                SetComponentState(
                    AbstractSet(operationHandleRef = HandleRef(Handles.OPERATION_2)),
                    listOf(
                        AbstractDeviceComponentStateOneOf.ChoiceClockState(
                            ClockState(
                                abstractDeviceComponentState = AbstractDeviceComponentState(
                                    AbstractState(descriptorHandleAttr = HandleRef(Handles.MDS_0))
                                ),
                                remoteSyncAttr = true
                            )
                        )
                    )
                )
            )
        )
    }

    @Test
    fun `test set alert state`(): Unit = runBlocking {
        runImmediateFinScoInvocation(
            DescriptionItem(
                SetAlertStateOperationDescriptor(abstractSetStateOpDescr()),
                SetAlertStateOperationState(abstractOpState()),
                Handles.SCO_0
            ),
            AbstractSetOneOf.ChoiceSetAlertState(
                SetAlertState(
                    AbstractSet(operationHandleRef = HandleRef(Handles.OPERATION_2)),
                    AbstractAlertStateOneOf.ChoiceAlertSystemState(
                        AlertSystemState(
                            abstractAlertState = AbstractAlertState(
                                AbstractState(descriptorHandleAttr = HandleRef(Handles.MDS_0)),
                                activationStateAttr = AlertActivation(AlertActivation.EnumType.On)
                            )
                        )
                    )
                )
            )
        )
    }

    @Test
    fun `test set context state`(): Unit = runBlocking {
        runImmediateFinScoInvocation(
            DescriptionItem(
                SetContextStateOperationDescriptor(abstractSetStateOpDescr()),
                SetContextStateOperationState(abstractOpState()),
                Handles.SCO_0
            ),
            AbstractSetOneOf.ChoiceSetContextState(
                SetContextState(
                    AbstractSet(operationHandleRef = HandleRef(Handles.OPERATION_2)),
                    listOf(
                        AbstractContextStateOneOf.ChoiceEnsembleContextState(
                            EnsembleContextState(
                                AbstractContextState(
                                    AbstractMultiState(
                                        AbstractState(
                                            descriptorHandleAttr = HandleRef(
                                                Handles.MDS_0
                                            )
                                        ),
                                        handleAttr = Handle("")
                                    )
                                )
                            )
                        )
                    )
                )
            )
        )
    }

    @Test
    fun `test immediate cancel`(): Unit = runBlocking {
        udp = createUdp().apply { start() }
        server = createServer()

        sdcDevice = createDevice(CancelledManuallyOperationInvocationReceiver())
        sdcDevice.mdibAccess().writeDescription(mdibModificationsPresets.fullTree())
        sdcDevice.mdibAccess().writeDescription(
            MdibDescriptionModifications().insert(
                DescriptionItem(
                    SetStringOperationDescriptor(abstractOperationDescriptor()),
                    SetStringOperationState(abstractOpState()),
                    Handles.SCO_0
                )
            )
        )
        server.start()
        sdcDevice.start()

        val sdcDeviceDiscovery = protoSdcClientComponent.sdcDeviceDiscoveryFactory()
            .create(udp)
            .also { it.start() }

        val endpoint = withTimeout(DefaultWait.MILLIS) {
            sdcDeviceDiscovery.searchByEndpointIdentifierAsync(endpointIdentifier).await()
        }

        val sdcDeviceProxy = sdcDeviceDiscovery.connect(endpoint.getOrThrow())
        val setService = sdcDeviceProxy.setService()
        require(setService != null)
        val transaction = setService.invokeOperation(
            AbstractSetOneOf.ChoiceSetString(
                SetString(
                    AbstractSet(operationHandleRef = HandleRef(Handles.OPERATION_2)),
                    "Test"
                )
            )
        )

        assertEquals(
            InvocationState.EnumType.CnclldMan,
            transaction.response().invocationState()
        )

        val reports = withTimeout(DefaultWait.MILLIS) {
            transaction.waitForFinalReport()
        }

        assertEquals(1, reports.size)
        assertEquals(InvocationState.EnumType.CnclldMan, reports[0].invocationState())

        sdcDeviceProxy.stop()
        sdcDeviceDiscovery.stop()
        sdcDevice.stop()
    }

    @Test
    fun `test queued invocation`(): Unit = runBlocking {
        udp = createUdp().apply { start() }
        server = createServer()

        sdcDevice = createDevice(QueuedOperationInvocationReceiver())
        sdcDevice.mdibAccess().writeDescription(mdibModificationsPresets.fullTree())
        sdcDevice.mdibAccess().writeDescription(
            MdibDescriptionModifications().insert(
                DescriptionItem(
                    SetStringOperationDescriptor(abstractOperationDescriptor()),
                    SetStringOperationState(abstractOpState()),
                    Handles.SCO_0
                )
            )
        )
        server.start()
        sdcDevice.start()

        val sdcDeviceDiscovery = protoSdcClientComponent.sdcDeviceDiscoveryFactory()
            .create(udp)
            .also { it.start() }

        val endpoint = withTimeout(DefaultWait.MILLIS) {
            sdcDeviceDiscovery.searchByEndpointIdentifierAsync(endpointIdentifier).await()
        }

        val sdcDeviceProxy = sdcDeviceDiscovery.connect(endpoint.getOrThrow())
        val setService = sdcDeviceProxy.setService()
        require(setService != null)
        run {
            val transaction = setService.invokeOperation(
                AbstractSetOneOf.ChoiceSetString(
                    SetString(
                        AbstractSet(operationHandleRef = HandleRef(Handles.OPERATION_2)),
                        QueuedOperationInvocationReceiver.SUCCESS
                    )
                )
            )

            assertEquals(
                InvocationState.EnumType.Wait,
                transaction.response().invocationState()
            )

            val reports = withTimeout(DefaultWait.MILLIS) {
                transaction.waitForFinalReport()
            }

            assertEquals(3, reports.size)
            assertEquals(InvocationState.EnumType.Wait, reports[0].invocationState())
            assertEquals(InvocationState.EnumType.Start, reports[1].invocationState())
            assertEquals(InvocationState.EnumType.Fin, reports[2].invocationState())
        }

        run {
            val transaction = setService.invokeOperation(
                AbstractSetOneOf.ChoiceSetString(
                    SetString(
                        AbstractSet(operationHandleRef = HandleRef(Handles.OPERATION_2)),
                        QueuedOperationInvocationReceiver.CANCEL
                    )
                )
            )

            assertEquals(
                InvocationState.EnumType.Wait,
                transaction.response().invocationState()
            )

            val reports = withTimeout(DefaultWait.MILLIS) {
                transaction.waitForFinalReport()
            }

            assertEquals(3, reports.size)
            assertEquals(InvocationState.EnumType.Wait, reports[0].invocationState())
            assertEquals(InvocationState.EnumType.Start, reports[1].invocationState())
            assertEquals(InvocationState.EnumType.Cnclld, reports[2].invocationState())
        }

        sdcDeviceProxy.stop()
        sdcDeviceDiscovery.stop()
        sdcDevice.stop()
    }

    private suspend fun runImmediateFinScoInvocation(
        descriptionItem: DescriptionItem,
        operationRequest: AbstractSetOneOf
    ) {
        udp = createUdp().apply { start() }
        server = createServer()

        sdcDevice = createDevice(ImmediateFinOperationInvocationReceiver())
        sdcDevice.mdibAccess().writeDescription(mdibModificationsPresets.fullTree())
        sdcDevice.mdibAccess().writeDescription(MdibDescriptionModifications().insert(descriptionItem))

        server.start()
        sdcDevice.start()

        val sdcDeviceDiscovery = protoSdcClientComponent.sdcDeviceDiscoveryFactory()
            .create(udp)
            .also { it.start() }

        val endpoint = withTimeout(DefaultWait.MILLIS) {
            sdcDeviceDiscovery.searchByEndpointIdentifierAsync(endpointIdentifier).await()
        }

        val sdcDeviceProxy = sdcDeviceDiscovery.connect(endpoint.getOrThrow())
        val setService = sdcDeviceProxy.setService()
        require(setService != null)
        val transaction = setService.invokeOperation(operationRequest)

        assertEquals(InvocationState.EnumType.Fin, transaction.response().invocationState())

        assertEquals(InvocationState.EnumType.Fin, withTimeout(DefaultWait.MILLIS) {
            transaction.waitForFinalReport().first().invocationState()
        })

        sdcDeviceProxy.stop()
        sdcDeviceDiscovery.stop()
        sdcDevice.stop()
        server.stop()
        udp.stop()
    }

    private fun createUdp() = udp {
        binding(
            UdpBindingConfig(
                networkInterface,
                address.ipVersion(),
                SocketInfo(address, 6464)
            )
        )
    }

    private fun createServer() = protoSdcComponent.serverFactory().create(InetSocketAddress("localhost", 0))

    private fun createDevice(operationReceiver: OperationInvocationReceiver? = null) = sdcDeviceFactory.create(
        udp,
        server,
        endpointIdentifier,
        EndpointMetadata.newBuilder().setSerialNumber(StringValue.of("1234-5678-90")).build(),
        operationReceiver,
        null
    )

    private fun abstractSetStateOpDescr() = AbstractSetStateOperationDescriptor(
        abstractOperationDescriptor = abstractOperationDescriptor()
    )

    private fun abstractOperationDescriptor() = AbstractOperationDescriptor(
        abstractDescriptor = AbstractDescriptor(handleAttr = Handle(Handles.OPERATION_2)),
        operationTargetAttr = HandleRef(Handles.MDS_0)
    )

    private fun abstractOpState() = AbstractOperationState(
        abstractState = AbstractState(
            descriptorHandleAttr = HandleRef(Handles.OPERATION_2)
        ),
        operatingModeAttr = OperatingMode(OperatingMode.EnumType.En),
    )
}
package org.somda.protosdc.crypto

import java.net.Socket
import java.security.KeyStore
import java.security.NoSuchAlgorithmException
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.SSLEngine
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509ExtendedTrustManager
import javax.net.ssl.X509TrustManager

/**
 * Delegates interactions to the default Java [X509ExtendedTrustManager].
 *
 * All interactions are delegated except that it disables the verification of hostnames and IPs in certificates.
 */
public class ClientTrustManager(trustStore: KeyStore?) : X509ExtendedTrustManager(), X509TrustManager {
    private val trustManager: X509ExtendedTrustManager

    init {
        val trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
        trustManagerFactory.init(trustStore)
        var trustManagerToFind: X509ExtendedTrustManager? = null
        for (manager in trustManagerFactory.trustManagers) {
            if (manager is X509ExtendedTrustManager) {
                trustManagerToFind = manager
                break
            }
        }
        when (trustManagerToFind == null) {
            true -> throw NoSuchAlgorithmException(
                "Could not initialize TrustManager, no X509ExtendedTrustManager base instance found."
            )
            false -> trustManager = trustManagerToFind
        }
    }

    @Throws(CertificateException::class)
    override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String, socket: Socket) {
        trustManager.checkClientTrusted(chain, authType, socket)
    }

    @Throws(CertificateException::class)
    override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String, socket: Socket) {
        trustManager.checkServerTrusted(chain, authType, socket)
    }

    @Throws(CertificateException::class)
    override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String, engine: SSLEngine) {
        trustManager.checkClientTrusted(chain, authType, engine)
    }

    @Throws(CertificateException::class)
    override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String, engine: SSLEngine) {
        // this SHOULD only prevent hostname/ip checks for the certificate
        // TODO LDe: Evaluate the consequences of doing this
        //  It overrides the endpoint identification algorithm inside the ssl engine,
        //  which is the behavior we want for sdc. I cannot in good conscious say that I've
        //  evaluated all the side effects this can have, so take it with a huge grain of salt.
        val param = engine.sslParameters
        param.endpointIdentificationAlgorithm = null
        engine.sslParameters = param
        trustManager.checkServerTrusted(chain, authType, engine)
    }

    @Throws(CertificateException::class)
    override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
        trustManager.checkClientTrusted(chain, authType)
    }

    @Throws(CertificateException::class)
    override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
        trustManager.checkServerTrusted(chain, authType)
    }

    override fun getAcceptedIssuers(): Array<X509Certificate> {
        return trustManager.acceptedIssuers
    }
}
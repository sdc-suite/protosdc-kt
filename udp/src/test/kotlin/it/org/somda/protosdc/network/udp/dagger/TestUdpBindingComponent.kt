package it.org.somda.protosdc.network.udp.dagger

import dagger.BindsInstance
import dagger.Component
import kotlinx.coroutines.channels.Channel
import org.jetbrains.annotations.Nullable
import org.somda.protosdc.common.ComponentConfig
import org.somda.protosdc.network.udp.UdpBinding
import org.somda.protosdc.network.udp.UdpBindingConfig
import org.somda.protosdc.network.udp.UdpConfig
import org.somda.protosdc.network.udp.UdpMessage
import javax.inject.Singleton

@Singleton
@Component(modules = [TestUdpBindingModule::class])
internal interface TestUdpBindingComponent {
    @Component.Builder
    interface Builder {
        fun bind(@BindsInstance @ComponentConfig @Nullable messageChannel: Channel<UdpMessage>): Builder
        fun bind(@BindsInstance @ComponentConfig udpBindings: List<UdpBindingConfig>): Builder
        fun bind(@BindsInstance @ComponentConfig @Nullable udpConfig: UdpConfig): Builder
        fun build(): TestUdpBindingComponent
    }

    fun udpBindings(): List<@JvmSuppressWildcards UdpBinding>
}
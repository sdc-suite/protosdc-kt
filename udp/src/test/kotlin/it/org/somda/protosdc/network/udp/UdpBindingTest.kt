package it.org.somda.protosdc.network.udp

import it.org.somda.protosdc.network.udp.dagger.DaggerTestUdpBindingComponent
import kotlinx.coroutines.async
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.somda.protosdc.network.ip.ipVersion
import org.somda.protosdc.network.udp.SocketInfo
import org.somda.protosdc.network.udp.UdpBindingConfig
import org.somda.protosdc.network.udp.UdpMessage
import org.somda.protosdc.test.util.DefaultWait
import org.somda.protosdc.test.util.NetworkInterfaceWithMulticastSupport
import java.net.InetAddress
import java.net.NetworkInterface
import java.util.*

class UdpBindingTest {
    private val channelCapacity = 100
    private val ipV4Address = InetAddress.getByName("239.255.255.250")
    private val ipV4NetworkInterface = NetworkInterfaceWithMulticastSupport.findFor(ipV4Address.hostAddress).let {
        check(it != null)
        it
    }

    private val ipV6Address = InetAddress.getByName("FF02::C")
    private val ipV6NetworkInterface = NetworkInterfaceWithMulticastSupport.findFor(ipV6Address.hostAddress).let {
        check(it != null)
        it
    }

    @BeforeEach
    fun beforeEach() {
        // this random wait helps in stabilizing the UDP tests
        // I don't know why, maybe IGMP routines need to settle
        Thread.sleep(500)
    }

    @Test
    fun `test IPv4 multicast delivery with retransmission`(): Unit = runBlocking {
        testMulticastDeliveryWithRetransmissionFor(ipV4NetworkInterface, ipV4Address)
    }

    @Test
    fun `test IPv6 multicast delivery with retransmission`(): Unit = runBlocking {
        testMulticastDeliveryWithRetransmissionFor(ipV6NetworkInterface, ipV6Address)
    }

    private suspend fun testMulticastDeliveryWithRetransmissionFor(
        networkInterface: NetworkInterface,
        address: InetAddress
    ) {
        val expectedBytes = uniqueMessage()

        // start bindings only, as starting the message queues would block
        // reception of messages when requesting the channels directly

        val channel = Channel<UdpMessage>(channelCapacity)
        val receiverComponent = createComponent(networkInterface, address, channel)
        val receiver = receiverComponent.udpBindings().first().apply { start() }
        val senderComponent = createComponent(networkInterface, address)
        val sender = senderComponent.udpBindings().first().apply { start() }

        try {
            coroutineScope {
                launch {
                    sender.sendMulticastMessage(expectedBytes)
                }
                withTimeout(DefaultWait.MILLIS) {
                    for (i in 1..2) {
                        val data = channel.receive()
                        assertTrue(udpMessageEqualsBytes(data, expectedBytes))
                    }
                }
            }
        } finally {
            receiver.stop()
            sender.stop()
        }
    }

    @Test
    fun `test IPv4 multicast message with unicast reply`(): Unit = runBlocking {
        testSendMulticastMessageWithUnicastReply(ipV4NetworkInterface, ipV4Address)
    }

    @Test
    fun `test IPv6 multicast message with unicast reply`(): Unit = runBlocking {
        testSendMulticastMessageWithUnicastReply(ipV6NetworkInterface, ipV6Address)
    }

    private suspend fun testSendMulticastMessageWithUnicastReply(
        networkInterface: NetworkInterface,
        address: InetAddress
    ) {
        coroutineScope {
            val expectedRequestBytes = uniqueMessage()
            val expectedResponseBytes = uniqueMessage()

            // start bindings, only, as starting the components would also start the message queues which then block
            // reception of messages when requesting the channels directly

            val channel1 = Channel<UdpMessage>(channelCapacity)
            val senderReceiver1Component = createComponent(networkInterface, address, channel1)
            val senderReceiver1 = senderReceiver1Component.udpBindings().first().apply { start() }

            val channel2 = Channel<UdpMessage>(channelCapacity)
            val senderReceiver2Component = createComponent(networkInterface, address, channel2)
            val senderReceiver2 = senderReceiver2Component.udpBindings().first().apply { start() }

            // non blocking responder
            val senderTask1 = async {
                var udpMessage: UdpMessage
                do {
                    udpMessage = channel2.receive()
                } while (!udpMessageEqualsBytes(udpMessage, expectedRequestBytes))

                senderReceiver2.sendMessage(
                    expectedResponseBytes,
                    udpMessage.header.ipHeader.sourceAddress(),
                    udpMessage.header.sourcePort
                )
            }

            senderReceiver1.sendMulticastMessage(expectedRequestBytes)

            withTimeout(DefaultWait.MILLIS) {
                assertTrue(run {
                    do {
                        val udpMessage = try {
                            channel1.receive()
                        } catch (e: Exception) {
                            return@run false
                        }
                    } while (!udpMessageEqualsBytes(udpMessage, expectedRequestBytes))
                    true
                })
                senderTask1.cancel()
            }

            senderReceiver1.stop()
            senderReceiver2.stop()
        }
    }

    private fun createComponent(
        networkInterface: NetworkInterface,
        address: InetAddress,
        channel: Channel<UdpMessage> = Channel()
    ) = DaggerTestUdpBindingComponent.builder()
        .bind(channel)
        .bind(
            listOf(
                UdpBindingConfig(
                    networkInterface,
                    address.ipVersion(),
                    SocketInfo(address, 6464)
                )
            )
        )
        .build()

    private fun udpMessageEqualsBytes(msg: UdpMessage, bytes: ByteArray) =
        msg.data.copyOfRange(0, msg.header.length).contentEquals(bytes)

    private fun uniqueMessage() = UUID.randomUUID().toString().toByteArray()
}
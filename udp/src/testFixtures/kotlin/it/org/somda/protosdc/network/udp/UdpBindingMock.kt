package it.org.somda.protosdc.network.udp

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc.common.ServiceDelegate
import org.somda.protosdc.common.ServiceDelegateImpl
import org.somda.protosdc.network.ip.IpHeader
import org.somda.protosdc.network.udp.UdpBinding
import org.somda.protosdc.network.udp.UdpHeader
import org.somda.protosdc.network.udp.UdpMessage
import java.net.InetAddress

class UdpBindingMock constructor(receiver: Channel<UdpMessage>) : UdpBinding, ServiceDelegate by ServiceDelegateImpl() {
    internal companion object : Logging {
        private val mutex = Mutex()
        private val receivers = mutableMapOf<Int, Channel<UdpMessage>>()
        private var receiverCount = 0

        internal fun resetReceivers() {
            receivers.clear()
        }
    }

    private var receiverNr = 0

    init {
        onStartUp {
            mutex.withLock {
                receiverNr = ++receiverCount
                logger.debug { "Add receiver #${receiverNr}" }
                receivers[receiverNr] = receiver
            }
        }

        onShutDown {
            mutex.withLock {
                logger.debug { "Remove receiver #${receiverNr}" }
                receivers.remove(receiverNr)
            }
        }
    }

    override suspend fun sendMulticastMessage(data: ByteArray) {
        sendMessage(data, "", 0)
    }

    override suspend fun sendMessage(data: ByteArray, destinationAddress: String, destinationPort: Int) {
        mutex.withLock { receivers.toMap() }.forEach {
            logger.debug { "Send message to receiver #${it.key}" }
            it.value.trySend(
                UdpMessage(
                    data,
                    UdpHeader(
                        0,
                        destinationPort,
                        data.size,
                        IpHeader.IpV4(InetAddress.getLoopbackAddress(), InetAddress.getByName(destinationAddress))
                    )
                )
            )
        }
    }

    override suspend fun sendMessage(data: ByteArray, destinationAddress: InetAddress, destinationPort: Int) {
        sendMessage(data, destinationAddress.hostAddress, destinationPort)
    }
}
package it.org.somda.protosdc.network.udp

import it.org.somda.protosdc.network.udp.dagger.DaggerTestUdpMockComponent
import kotlinx.coroutines.channels.Channel
import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.common.ServiceDelegate
import org.somda.protosdc.common.ServiceDelegateImpl
import org.somda.protosdc.network.udp.Udp
import org.somda.protosdc.network.udp.UdpConfig
import org.somda.protosdc.network.udp.UdpInitDsl
import org.somda.protosdc.network.udp.UdpInitImpl
import org.somda.protosdc.network.udp.UdpMessage
import org.somda.protosdc.network.udp.UdpMessageQueue

public interface UdpMock : Udp {
    @UdpInitDsl
    public interface Init {
        val messageChannel: Channel<UdpMessage>
        val instanceId: InstanceId
        val config: UdpConfig

        public fun messageChannel(messageChannel: Channel<UdpMessage>)
        public fun instanceId(instanceId: InstanceId)
        public fun config(config: UdpConfig)
    }
}

@UdpInitDsl
private class UdpMockImpl(init: Init) : UdpMock, ServiceDelegate by ServiceDelegateImpl() {
    private val daggerComponent = DaggerTestUdpMockComponent.builder()
        .bind(init.config)
        .bind(init.messageChannel)
        .build()

    init {
        this.onStartUp {
            UdpBindingMock.resetReceivers()
            daggerComponent.udpMessageQueue().start()
            daggerComponent.udpBindings().forEach { it.start() }
        }

        this.onShutDown {
            daggerComponent.udpBindings().forEach { it.stop() }
            daggerComponent.udpMessageQueue().stop()
        }
    }

    override val messageQueue: UdpMessageQueue
        get() = daggerComponent.udpMessageQueue()

    internal class Init : UdpMock.Init, Udp.Init by UdpInitImpl() {
        internal var _messageChannel: Channel<UdpMessage> = Channel()

        override val messageChannel: Channel<UdpMessage>
            get() = _messageChannel

        override fun messageChannel(messageChannel: Channel<UdpMessage>) {
            this._messageChannel = messageChannel
        }
    }
}

public fun udpMock(init: UdpMock.Init.() -> Unit = {}): UdpMock {
    return UdpMockImpl(UdpMockImpl.Init().apply(init))
}
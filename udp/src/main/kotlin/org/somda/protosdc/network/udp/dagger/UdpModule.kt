package org.somda.protosdc.network.udp.dagger

import dagger.Module
import dagger.Provides
import kotlinx.coroutines.channels.Channel
import org.somda.protosdc.common.ComponentConfig
import org.somda.protosdc.network.udp.UdpBinding
import org.somda.protosdc.network.udp.UdpBindingConfig
import org.somda.protosdc.network.udp.UdpBindingImpl
import org.somda.protosdc.network.udp.UdpConfig
import org.somda.protosdc.network.udp.UdpMessage
import org.somda.protosdc.network.udp.UdpMessageQueue
import org.somda.protosdc.network.udp.UdpMessageQueueImpl
import javax.annotation.Nullable
import javax.inject.Singleton

@Module
internal abstract class UdpModule {
    companion object {
        @Singleton
        @Provides
        fun udpBindings(
            @ComponentConfig udpBindingConfigs: List<UdpBindingConfig>,
            messageChannel: Channel<UdpMessage>,
            udpBindingFactory: UdpBindingImpl.Factory
        ): List<@JvmSuppressWildcards UdpBinding> =
            udpBindingConfigs.map {
                udpBindingFactory.create(
                    messageChannel,
                    it.networkInterface,
                    it.ipVersion,
                    it.multicastSocket,
                    it.messageInterceptor
                )
            }

        @Singleton
        @Provides
        fun udpMessageQueue(
            messageChannel: Channel<UdpMessage>,
            udpBindings: List<@JvmSuppressWildcards UdpBinding>,
            udpMessageQueueFactory: UdpMessageQueueImpl.Factory
        ): UdpMessageQueue = udpMessageQueueFactory.create(messageChannel, udpBindings)

        @Singleton
        @Provides
        fun udpConfig(@ComponentConfig @Nullable udpConfig: UdpConfig?) = udpConfig ?: UdpConfig()

        @Singleton
        @Provides
        fun messageChannel(): Channel<UdpMessage> = Channel()
    }
}
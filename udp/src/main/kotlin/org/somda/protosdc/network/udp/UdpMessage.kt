package org.somda.protosdc.network.udp

import java.nio.charset.StandardCharsets

/**
 * A UDP message data container.
 */
public data class UdpMessage(val data: ByteArray, val header: UdpHeader) {

    override fun toString(): String {
        return "[header=%s, data=%s]".format(header, String(data, 0, header.length, StandardCharsets.UTF_8))
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UdpMessage

        if (header != other.header) return false

        if (!data.sliceArray(0 until header.length)
                .contentEquals(other.data.sliceArray(0 until header.length))
        ) return false

        return true
    }

    override fun hashCode(): Int {
        var result = data.sliceArray(0 until header.length).contentHashCode()
        result = 31 * result + header.hashCode()
        return result
    }
}
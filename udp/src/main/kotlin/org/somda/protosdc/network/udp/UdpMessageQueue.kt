package org.somda.protosdc.network.udp

import kotlinx.coroutines.channels.ReceiveChannel
import org.somda.protosdc.common.Service
import java.net.InetAddress

/**
 * A message queue to send and receive UDP messages.
 */
public interface UdpMessageQueue : Service {
    /**
     * Queues an outgoing UDP message to be send via multicast.
     *
     * Important note: this method does only work if the outgoing UDP bindings are bound to multicast sockets.
     *
     * @param data the data to send.
     * @return true if the message could be queued, otherwise false (queue overflow).
     */
    public suspend fun sendMulticastMessage(data: ByteArray): Boolean

    /**
     * Queues an outgoing UDP message with specific destination.
     *
     * @param data the data to send.
     * @param destinationAddress the destination address.
     * @param destinationPort the destination port.
     * @return true if the message could be queued, otherwise false (queue overflow).
     */
    public suspend fun sendMessage(data: ByteArray, destinationAddress: InetAddress, destinationPort: Int): Boolean

    /**
     * Queues an outgoing UDP message with specific destination.
     *
     * @param data the data to send.
     * @param destinationAddress the destination address as string (will be resolved by underlying UDP bindings).
     * @param destinationPort the destination port.
     * @return true if the message could be queued, otherwise false (queue overflow).
     */
    public suspend fun sendMessage(data: ByteArray, destinationAddress: String, destinationPort: Int): Boolean

    /**
     * Creates a channel of incoming UDP messages.
     *
     * @return a channel to receive UDP messages of this queue.
     */
    public suspend fun subscribe(): ReceiveChannel<UdpMessage>
}
plugins {
    id("org.somda.protosdc.sdc.shared")
    alias(libs.plugins.org.somda.gitlab.maven.publishing)
    alias(libs.plugins.org.somda.append.snapshot.id)
}

dependencies {
    api(projects.common)

    testImplementation(testFixtures(projects.common))
}

kotlin {
    explicitApi()
}
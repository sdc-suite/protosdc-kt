package org.somda.protosdc.biceps.common.storage

/**
 *  Expresses that an object was not found.
 *
 *  The object type depends on the context of use.
 */
internal class NotFoundException(message: String) : Exception(message) {
}
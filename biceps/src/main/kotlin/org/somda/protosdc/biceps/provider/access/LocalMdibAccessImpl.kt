package org.somda.protosdc.biceps.provider.access

import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.somda.protosdc.biceps.common.MdibDescriptionModifications
import org.somda.protosdc.biceps.common.MdibEntity
import org.somda.protosdc.biceps.common.MdibStateModifications
import org.somda.protosdc.biceps.common.MdibVersion
import org.somda.protosdc.biceps.common.access.AsyncStorageFacade
import org.somda.protosdc.biceps.common.access.MdibAccess
import org.somda.protosdc.biceps.common.access.MdibAccessEvent
import org.somda.protosdc.biceps.common.helper.EventDistributor
import org.somda.protosdc.biceps.common.helper.WriteUtil
import org.somda.protosdc.biceps.common.preprocessing.DescriptorChildRemover
import org.somda.protosdc.biceps.common.storage.DescriptionPreprocessingSegment
import org.somda.protosdc.biceps.common.storage.MdibStorageImpl
import org.somda.protosdc.biceps.common.storage.MdibStoragePreprocessingChainImpl
import org.somda.protosdc.biceps.common.storage.StatePreprocessingSegment
import org.somda.protosdc.biceps.provider.preprocessing.*
import org.somda.protosdc.common.ChannelPublisher
import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.model.biceps.AbstractContextStateOneOf
import org.somda.protosdc.model.biceps.Handle
import kotlin.reflect.KClass

internal class LocalMdibAccessImpl(
    private var mdibVersion: MdibVersion,
    descriptionPreprocessingSegments: List<DescriptionPreprocessingSegment>,
    statePreprocessingSegments: List<StatePreprocessingSegment>,
    mdibStorageFactory: MdibStorageImpl.Factory,
    preProcessingChainFactory: MdibStoragePreprocessingChainImpl.Factory,
    instanceId: InstanceId
) : LocalMdibAccess {

    @AssistedInject
    internal constructor(
        @Assisted mdibVersion: MdibVersion = MdibVersion.create(),
        mdibStorageFactory: MdibStorageImpl.Factory,
        preProcessingChainFactory: MdibStoragePreprocessingChainImpl.Factory,
        cardinalityChecker: CardinalityChecker,
        duplicateChecker: DuplicateChecker,
        handleReferenceHandler: HandleReferenceChecker,
        typeConsistencyChecker: TypeConsistencyChecker,
        versionHandler: VersionHandler,
        descriptorChildRemover: DescriptorChildRemover,
        duplicateContextStateHandleHandler: DuplicateContextStateHandleHandler,
        instanceId: InstanceId
    ) : this(
        mdibVersion,
        listOf(
            duplicateChecker,
            handleReferenceHandler,
            cardinalityChecker,
            typeConsistencyChecker,
            versionHandler,
            descriptorChildRemover
        ),
        listOf(
            duplicateContextStateHandleHandler,
            versionHandler
        ),
        mdibStorageFactory,
        preProcessingChainFactory,
        instanceId
    )

    @AssistedFactory
    interface Factory {
        fun create(initialMdibVersion: MdibVersion = MdibVersion.create()): LocalMdibAccessImpl
    }

    private val readMutex = Mutex()

    private val publisher = ChannelPublisher<MdibAccessEvent>("$instanceId@LocalMdib")

    private val eventDistributor = EventDistributor(publisher)

    private var mdDescriptionVersion = 0L
    private var mdStateVersion = 0L
    private val mdibStorage = mdibStorageFactory.create(mdibVersion, mdDescriptionVersion, mdStateVersion)

    private val writeUtil = WriteUtil(
        instanceId,
        eventDistributor,
        preProcessingChainFactory.create(mdibStorage, descriptionPreprocessingSegments, statePreprocessingSegments),
        readMutex,
        this
    )

    override suspend fun writeDescription(descriptionModifications: MdibDescriptionModifications) =
        writeUtil.writeDescription(descriptionModifications) {
            mdibVersion = mdibVersion.inc()
            mdDescriptionVersion++
            mdStateVersion++
            mdibStorage.apply(mdibVersion.version, mdDescriptionVersion, mdStateVersion, it)
        }

    override suspend fun writeStates(stateModifications: MdibStateModifications) =
        writeUtil.writeStates(stateModifications) {
            mdibVersion = mdibVersion.inc()
            mdStateVersion++
            mdibStorage.apply(mdibVersion.version, mdStateVersion, it)
        }

    override suspend fun <T> withReadLock(action: suspend (MdibAccess) -> T): T = readMutex.withLock {
        action(AsyncStorageFacade(mdibStorage))
    }

    override suspend fun subscribe() = publisher.subscribe()

    override suspend fun unsubscribeAll() = publisher.unsubscribeAll()

    override suspend fun mdibVersion() = withReadLock { it.mdibVersion() }

    override suspend fun mdDescriptionVersion() = withReadLock { it.mdDescriptionVersion() }

    override suspend fun mdStateVersion() = withReadLock { it.mdStateVersion() }

    override suspend fun anyEntity(handle: String) = withReadLock { it.anyEntity(handle) }

    override suspend fun <T : MdibEntity> entity(handle: String, type: KClass<out T>) = withReadLock {
        it.entity(handle, type)
    }

    override suspend fun rootEntities() = withReadLock { it.rootEntities() }

    override suspend fun abstractContextState(handle: String) = withReadLock { it.abstractContextState(handle) }

    override suspend fun <T : AbstractContextStateOneOf> contextState(handle: String, type: KClass<out T>) =
        withReadLock {
            it.contextState(handle, type)
        }

    override suspend fun contextStates() = withReadLock { it.contextStates() }

    override suspend fun <T : MdibEntity> entitiesByType(type: KClass<out T>) = withReadLock { it.entitiesByType(type) }

    override suspend fun <T : MdibEntity> childrenByType(handle: String, type: KClass<out T>) = withReadLock {
        it.childrenByType(handle, type)
    }

    override suspend fun abstractContextState(handle: Handle) = abstractContextState(handle.string)

    override suspend fun anyEntity(handle: Handle) = anyEntity(handle.string)

    override suspend fun <T : MdibEntity> entity(handle: Handle, type: KClass<out T>) = entity(handle.string, type)

    override suspend fun <T : AbstractContextStateOneOf> contextState(handle: Handle, type: KClass<out T>) =
        contextState(handle.string, type)

    override suspend fun <T : MdibEntity> childrenByType(handle: Handle, type: KClass<out T>) =
        childrenByType(handle.string, type)
}
package org.somda.protosdc.biceps.common.storage

import org.somda.protosdc.biceps.common.InvalidWhenBranchException
import org.somda.protosdc.biceps.common.descriptorHandleString
import org.somda.protosdc.model.biceps.AbstractAlertDescriptor
import org.somda.protosdc.model.biceps.AbstractAlertState
import org.somda.protosdc.model.biceps.AbstractComplexDeviceComponentDescriptor
import org.somda.protosdc.model.biceps.AbstractComplexDeviceComponentState
import org.somda.protosdc.model.biceps.AbstractContextDescriptor
import org.somda.protosdc.model.biceps.AbstractContextState
import org.somda.protosdc.model.biceps.AbstractDescriptor
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf
import org.somda.protosdc.model.biceps.AbstractDeviceComponentDescriptor
import org.somda.protosdc.model.biceps.AbstractDeviceComponentState
import org.somda.protosdc.model.biceps.AbstractMetricDescriptor
import org.somda.protosdc.model.biceps.AbstractMetricState
import org.somda.protosdc.model.biceps.AbstractMultiState
import org.somda.protosdc.model.biceps.AbstractOperationDescriptor
import org.somda.protosdc.model.biceps.AbstractOperationState
import org.somda.protosdc.model.biceps.AbstractSetStateOperationDescriptor
import org.somda.protosdc.model.biceps.AbstractState
import org.somda.protosdc.model.biceps.AbstractStateOneOf
import org.somda.protosdc.model.biceps.ActivateOperationDescriptor
import org.somda.protosdc.model.biceps.ActivateOperationState
import org.somda.protosdc.model.biceps.AlertActivation
import org.somda.protosdc.model.biceps.AlertConditionDescriptor
import org.somda.protosdc.model.biceps.AlertConditionKind
import org.somda.protosdc.model.biceps.AlertConditionMonitoredLimits
import org.somda.protosdc.model.biceps.AlertConditionPriority
import org.somda.protosdc.model.biceps.AlertConditionState
import org.somda.protosdc.model.biceps.AlertSignalDescriptor
import org.somda.protosdc.model.biceps.AlertSignalManifestation
import org.somda.protosdc.model.biceps.AlertSignalState
import org.somda.protosdc.model.biceps.AlertSystemDescriptor
import org.somda.protosdc.model.biceps.AlertSystemState
import org.somda.protosdc.model.biceps.BatteryDescriptor
import org.somda.protosdc.model.biceps.BatteryState
import org.somda.protosdc.model.biceps.ChannelDescriptor
import org.somda.protosdc.model.biceps.ChannelState
import org.somda.protosdc.model.biceps.ClockDescriptor
import org.somda.protosdc.model.biceps.ClockState
import org.somda.protosdc.model.biceps.CodeIdentifier
import org.somda.protosdc.model.biceps.CodedValue
import org.somda.protosdc.model.biceps.ContextAssociation
import org.somda.protosdc.model.biceps.DistributionSampleArrayMetricDescriptor
import org.somda.protosdc.model.biceps.DistributionSampleArrayMetricState
import org.somda.protosdc.model.biceps.EnsembleContextDescriptor
import org.somda.protosdc.model.biceps.EnsembleContextState
import org.somda.protosdc.model.biceps.EnumStringMetricDescriptor
import org.somda.protosdc.model.biceps.EnumStringMetricState
import org.somda.protosdc.model.biceps.Handle
import org.somda.protosdc.model.biceps.HandleRef
import org.somda.protosdc.model.biceps.LimitAlertConditionDescriptor
import org.somda.protosdc.model.biceps.LimitAlertConditionState
import org.somda.protosdc.model.biceps.LocationContextDescriptor
import org.somda.protosdc.model.biceps.LocationContextState
import org.somda.protosdc.model.biceps.MdsDescriptor
import org.somda.protosdc.model.biceps.MdsState
import org.somda.protosdc.model.biceps.MeansContextDescriptor
import org.somda.protosdc.model.biceps.MeansContextState
import org.somda.protosdc.model.biceps.MetricAvailability
import org.somda.protosdc.model.biceps.MetricCategory
import org.somda.protosdc.model.biceps.NumericMetricDescriptor
import org.somda.protosdc.model.biceps.NumericMetricState
import org.somda.protosdc.model.biceps.OperatingMode
import org.somda.protosdc.model.biceps.OperatorContextDescriptor
import org.somda.protosdc.model.biceps.OperatorContextState
import org.somda.protosdc.model.biceps.PatientContextDescriptor
import org.somda.protosdc.model.biceps.PatientContextState
import org.somda.protosdc.model.biceps.Range
import org.somda.protosdc.model.biceps.RealTimeSampleArrayMetricDescriptor
import org.somda.protosdc.model.biceps.RealTimeSampleArrayMetricState
import org.somda.protosdc.model.biceps.ScoDescriptor
import org.somda.protosdc.model.biceps.ScoState
import org.somda.protosdc.model.biceps.SetAlertStateOperationDescriptor
import org.somda.protosdc.model.biceps.SetAlertStateOperationState
import org.somda.protosdc.model.biceps.SetComponentStateOperationDescriptor
import org.somda.protosdc.model.biceps.SetComponentStateOperationState
import org.somda.protosdc.model.biceps.SetContextStateOperationDescriptor
import org.somda.protosdc.model.biceps.SetContextStateOperationState
import org.somda.protosdc.model.biceps.SetMetricStateOperationDescriptor
import org.somda.protosdc.model.biceps.SetMetricStateOperationState
import org.somda.protosdc.model.biceps.SetStringOperationDescriptor
import org.somda.protosdc.model.biceps.SetStringOperationState
import org.somda.protosdc.model.biceps.SetValueOperationDescriptor
import org.somda.protosdc.model.biceps.SetValueOperationState
import org.somda.protosdc.model.biceps.StringMetricDescriptor
import org.somda.protosdc.model.biceps.StringMetricState
import org.somda.protosdc.model.biceps.SystemContextDescriptor
import org.somda.protosdc.model.biceps.SystemContextState
import org.somda.protosdc.model.biceps.VmdDescriptor
import org.somda.protosdc.model.biceps.VmdState
import org.somda.protosdc.model.biceps.WorkflowContextDescriptor
import org.somda.protosdc.model.biceps.WorkflowContextState
import java.math.BigDecimal
import kotlin.time.Duration.Companion.seconds

/**
 * Helper class to create default instances of descriptors and states.
 *
 * Descriptors need a handle and states a descriptor handle, plus in case of a context state, a state handle.
 * Other attributes instantiated with defaults.
 */
public class DefaultInstantiation constructor() {

    /**
     * Given a state, this function creates a default-instantiated matching descriptor.
     *
     * @param state the state for which a descriptor shall be generated.
     * @return a suitable descriptor with default attributes.
     */
    public fun descriptorOf(state: AbstractStateOneOf): AbstractDescriptorOneOf = when (state) {
        is AbstractStateOneOf.ChoiceActivateOperationState -> {
            AbstractDescriptorOneOf.ChoiceActivateOperationDescriptor(
                defaultActivateOperationDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceAlertConditionState -> {
            AbstractDescriptorOneOf.ChoiceAlertConditionDescriptor(
                defaultAlertConditionDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceAlertSignalState -> {
            AbstractDescriptorOneOf.ChoiceAlertSignalDescriptor(
                defaultAlertSignalDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceAlertSystemState -> {
            AbstractDescriptorOneOf.ChoiceAlertSystemDescriptor(
                defaultAlertSystemDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceBatteryState -> {
            AbstractDescriptorOneOf.ChoiceBatteryDescriptor(
                defaultBatteryDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceChannelState -> {
            AbstractDescriptorOneOf.ChoiceChannelDescriptor(
                defaultChannelDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceClockState -> {
            AbstractDescriptorOneOf.ChoiceClockDescriptor(
                defaultClockDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceDistributionSampleArrayMetricState -> {
            AbstractDescriptorOneOf.ChoiceDistributionSampleArrayMetricDescriptor(
                defaultDistributionSampleArrayMetricDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceEnsembleContextState -> {
            AbstractDescriptorOneOf.ChoiceEnsembleContextDescriptor(
                defaultEnsembleContextDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceEnumStringMetricState -> {
            AbstractDescriptorOneOf.ChoiceEnumStringMetricDescriptor(
                defaultEnumStringMetricDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceLimitAlertConditionState -> {
            AbstractDescriptorOneOf.ChoiceLimitAlertConditionDescriptor(
                defaultLimitAlertConditionDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceLocationContextState -> {
            AbstractDescriptorOneOf.ChoiceLocationContextDescriptor(
                defaultLocationContextDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceMdsState -> {
            AbstractDescriptorOneOf.ChoiceMdsDescriptor(
                defaultMdsDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceMeansContextState -> {
            AbstractDescriptorOneOf.ChoiceMeansContextDescriptor(
                defaultMeansContextDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceNumericMetricState -> {
            AbstractDescriptorOneOf.ChoiceNumericMetricDescriptor(
                defaultNumericMetricDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceOperatorContextState -> {
            AbstractDescriptorOneOf.ChoiceOperatorContextDescriptor(
                defaultOperatorContextDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoicePatientContextState -> {
            AbstractDescriptorOneOf.ChoicePatientContextDescriptor(
                defaultPatientContextDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState -> {
            AbstractDescriptorOneOf.ChoiceRealTimeSampleArrayMetricDescriptor(
                defaultRealTimeSampleArrayMetricDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceScoState -> {
            AbstractDescriptorOneOf.ChoiceScoDescriptor(
                defaultScoDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceSetAlertStateOperationState -> {
            AbstractDescriptorOneOf.ChoiceSetAlertStateOperationDescriptor(
                defaultSetAlertStateOperationDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceSetComponentStateOperationState -> {
            AbstractDescriptorOneOf.ChoiceSetComponentStateOperationDescriptor(
                defaultSetComponentStateOperationDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceSetContextStateOperationState -> {
            AbstractDescriptorOneOf.ChoiceSetContextStateOperationDescriptor(
                defaultSetContextStateOperationDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceSetMetricStateOperationState -> {
            AbstractDescriptorOneOf.ChoiceSetMetricStateOperationDescriptor(
                defaultSetMetricStateOperationDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceSetStringOperationState -> {
            AbstractDescriptorOneOf.ChoiceSetStringOperationDescriptor(
                defaultSetStringOperationDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceSetValueOperationState -> {
            AbstractDescriptorOneOf.ChoiceSetValueOperationDescriptor(
                defaultSetValueOperationDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceStringMetricState -> {
            AbstractDescriptorOneOf.ChoiceStringMetricDescriptor(
                defaultStringMetricDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceSystemContextState -> {
            AbstractDescriptorOneOf.ChoiceSystemContextDescriptor(
                defaultSystemContextDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceVmdState -> {
            AbstractDescriptorOneOf.ChoiceVmdDescriptor(
                defaultVmdDescriptor(state.descriptorHandleString())
            )
        }

        is AbstractStateOneOf.ChoiceWorkflowContextState -> {
            AbstractDescriptorOneOf.ChoiceWorkflowContextDescriptor(
                defaultWorkflowContextDescriptor(state.descriptorHandleString())
            )
        }

        else -> throw InvalidWhenBranchException(state)
    }

    // --- descriptors

    public fun defaultAbstractAlertDescriptor(handle: String): AbstractAlertDescriptor = AbstractAlertDescriptor(
        abstractDescriptor = defaultAbstractDescriptor(handle)
    )

    public fun defaultAbstractSetStateOperationDescriptor(handle: String): AbstractSetStateOperationDescriptor =
        AbstractSetStateOperationDescriptor(
            abstractOperationDescriptor = defaultAbstractOperationDescriptor(handle)
        )

    public fun defaultAbstractOperationDescriptor(handle: String): AbstractOperationDescriptor =
        AbstractOperationDescriptor(
            abstractDescriptor = defaultAbstractDescriptor(handle),
            operationTargetAttr = HandleRef("")
        )

    public fun defaultAbstractDeviceComponentDescriptor(handle: String): AbstractDeviceComponentDescriptor =
        AbstractDeviceComponentDescriptor(
            abstractDescriptor = defaultAbstractDescriptor(handle)
        )

    public fun defaultAbstractComplexDeviceComponentDescriptor(handle: String): AbstractComplexDeviceComponentDescriptor =
        AbstractComplexDeviceComponentDescriptor(
            abstractDeviceComponentDescriptor = defaultAbstractDeviceComponentDescriptor(handle)
        )

    public fun defaultAbstractMetricDescriptor(handle: String): AbstractMetricDescriptor = AbstractMetricDescriptor(
        abstractDescriptor = defaultAbstractDescriptor(handle),
        unit = defaultUnit(),
        metricAvailabilityAttr = MetricAvailability(MetricAvailability.EnumType.Cont),
        metricCategoryAttr = MetricCategory(MetricCategory.EnumType.Unspec)
    )

    public fun defaultAbstractContextDescriptor(handle: String): AbstractContextDescriptor = AbstractContextDescriptor(
        abstractDescriptor = defaultAbstractDescriptor(handle)
    )

    public fun defaultAbstractDescriptor(handle: String): AbstractDescriptor = AbstractDescriptor(
        handleAttr = Handle(handle)
    )

    public fun defaultUnit(): CodedValue = CodedValue(
        codeAttr = CodeIdentifier("262656") // DIMLESS
    )

    public fun defaultActivateOperationDescriptor(handle: String): ActivateOperationDescriptor =
        ActivateOperationDescriptor(
            abstractSetStateOperationDescriptor = defaultAbstractSetStateOperationDescriptor(handle)
        )

    public fun defaultAlertConditionDescriptor(handle: String): AlertConditionDescriptor = AlertConditionDescriptor(
        abstractAlertDescriptor = defaultAbstractAlertDescriptor(handle),
        kindAttr = AlertConditionKind(AlertConditionKind.EnumType.Oth),
        priorityAttr = AlertConditionPriority(AlertConditionPriority.EnumType.None)
    )

    public fun defaultAlertSignalDescriptor(handle: String): AlertSignalDescriptor = AlertSignalDescriptor(
        abstractAlertDescriptor = defaultAbstractAlertDescriptor(handle),
        manifestationAttr = AlertSignalManifestation(AlertSignalManifestation.EnumType.Oth),
        latchingAttr = false
    )

    public fun defaultAlertSystemDescriptor(handle: String): AlertSystemDescriptor = AlertSystemDescriptor(
        abstractAlertDescriptor = defaultAbstractAlertDescriptor(handle)
    )

    public fun defaultBatteryDescriptor(handle: String): BatteryDescriptor = BatteryDescriptor(
        abstractDeviceComponentDescriptor = defaultAbstractDeviceComponentDescriptor(handle)
    )

    public fun defaultChannelDescriptor(handle: String): ChannelDescriptor = ChannelDescriptor(
        abstractDeviceComponentDescriptor = defaultAbstractDeviceComponentDescriptor(handle)
    )

    public fun defaultClockDescriptor(handle: String): ClockDescriptor = ClockDescriptor(
        abstractDeviceComponentDescriptor = defaultAbstractDeviceComponentDescriptor(handle)
    )

    public fun defaultDistributionSampleArrayMetricDescriptor(handle: String): DistributionSampleArrayMetricDescriptor =
        DistributionSampleArrayMetricDescriptor(
            abstractMetricDescriptor = defaultAbstractMetricDescriptor(handle),
            domainUnit = defaultUnit(),
            distributionRange = Range(),
            resolutionAttr = BigDecimal.ZERO,
        )

    public fun defaultEnsembleContextDescriptor(handle: String): EnsembleContextDescriptor = EnsembleContextDescriptor(
        abstractContextDescriptor = defaultAbstractContextDescriptor(handle)
    )

    public fun defaultEnumStringMetricDescriptor(handle: String): EnumStringMetricDescriptor =
        EnumStringMetricDescriptor(
            stringMetricDescriptor = StringMetricDescriptor(defaultAbstractMetricDescriptor(handle))
        )

    public fun defaultLimitAlertConditionDescriptor(handle: String): LimitAlertConditionDescriptor =
        LimitAlertConditionDescriptor(
            alertConditionDescriptor = AlertConditionDescriptor(
                abstractAlertDescriptor = defaultAbstractAlertDescriptor(handle),
                kindAttr = AlertConditionKind(AlertConditionKind.EnumType.Oth),
                priorityAttr = AlertConditionPriority(AlertConditionPriority.EnumType.None)
            ),
            maxLimits = Range()
        )

    public fun defaultLocationContextDescriptor(handle: String): LocationContextDescriptor = LocationContextDescriptor(
        abstractContextDescriptor = defaultAbstractContextDescriptor(handle)
    )

    public fun defaultMdsDescriptor(handle: String): MdsDescriptor = MdsDescriptor(
        abstractComplexDeviceComponentDescriptor = defaultAbstractComplexDeviceComponentDescriptor(handle)
    )

    public fun defaultMeansContextDescriptor(handle: String): MeansContextDescriptor = MeansContextDescriptor(
        abstractContextDescriptor = defaultAbstractContextDescriptor(handle)
    )

    public fun defaultNumericMetricDescriptor(handle: String): NumericMetricDescriptor = NumericMetricDescriptor(
        abstractMetricDescriptor = defaultAbstractMetricDescriptor(handle),
        resolutionAttr = BigDecimal.ZERO
    )

    public fun defaultOperatorContextDescriptor(handle: String): OperatorContextDescriptor = OperatorContextDescriptor(
        abstractContextDescriptor = defaultAbstractContextDescriptor(handle)
    )

    public fun defaultPatientContextDescriptor(handle: String): PatientContextDescriptor = PatientContextDescriptor(
        abstractContextDescriptor = defaultAbstractContextDescriptor(handle)
    )

    public fun defaultRealTimeSampleArrayMetricDescriptor(handle: String): RealTimeSampleArrayMetricDescriptor =
        RealTimeSampleArrayMetricDescriptor(
            abstractMetricDescriptor = defaultAbstractMetricDescriptor(handle),
            resolutionAttr = BigDecimal.ZERO,
            samplePeriodAttr = 0.seconds
        )

    public fun defaultScoDescriptor(handle: String): ScoDescriptor = ScoDescriptor(
        abstractDeviceComponentDescriptor = defaultAbstractDeviceComponentDescriptor(handle)
    )

    public fun defaultSetAlertStateOperationDescriptor(handle: String): SetAlertStateOperationDescriptor =
        SetAlertStateOperationDescriptor(
            abstractSetStateOperationDescriptor = defaultAbstractSetStateOperationDescriptor(handle)
        )

    public fun defaultSetComponentStateOperationDescriptor(handle: String): SetComponentStateOperationDescriptor =
        SetComponentStateOperationDescriptor(
            abstractSetStateOperationDescriptor = defaultAbstractSetStateOperationDescriptor(handle)
        )

    public fun defaultSetContextStateOperationDescriptor(handle: String): SetContextStateOperationDescriptor =
        SetContextStateOperationDescriptor(
            abstractSetStateOperationDescriptor = defaultAbstractSetStateOperationDescriptor(handle)
        )

    public fun defaultSetMetricStateOperationDescriptor(handle: String): SetMetricStateOperationDescriptor =
        SetMetricStateOperationDescriptor(
            abstractSetStateOperationDescriptor = defaultAbstractSetStateOperationDescriptor(handle)
        )

    public fun defaultSetStringOperationDescriptor(handle: String): SetStringOperationDescriptor =
        SetStringOperationDescriptor(
            abstractOperationDescriptor = defaultAbstractOperationDescriptor(handle)
        )

    public fun defaultSetValueOperationDescriptor(handle: String): SetValueOperationDescriptor =
        SetValueOperationDescriptor(
            abstractOperationDescriptor = defaultAbstractOperationDescriptor(handle)
        )

    public fun defaultStringMetricDescriptor(handle: String): StringMetricDescriptor = StringMetricDescriptor(
        abstractMetricDescriptor = defaultAbstractMetricDescriptor(handle)
    )

    public fun defaultSystemContextDescriptor(handle: String): SystemContextDescriptor = SystemContextDescriptor(
        abstractDeviceComponentDescriptor = defaultAbstractDeviceComponentDescriptor(handle)
    )

    public fun defaultVmdDescriptor(handle: String): VmdDescriptor = VmdDescriptor(
        abstractComplexDeviceComponentDescriptor = defaultAbstractComplexDeviceComponentDescriptor(handle)
    )

    public fun defaultWorkflowContextDescriptor(handle: String): WorkflowContextDescriptor = WorkflowContextDescriptor(
        abstractContextDescriptor = defaultAbstractContextDescriptor(handle)
    )

    // --- states

    public fun defaultAbstractAlertState(descriptorHandle: String): AbstractAlertState = AbstractAlertState(
        abstractState = defaultAbstractState(descriptorHandle),
        activationStateAttr = AlertActivation(AlertActivation.EnumType.On)
    )

    public fun defaultAbstractOperationState(descriptorHandle: String): AbstractOperationState = AbstractOperationState(
        abstractState = defaultAbstractState(descriptorHandle),
        operatingModeAttr = OperatingMode(OperatingMode.EnumType.En)
    )

    public fun defaultAbstractDeviceComponentState(descriptorHandle: String): AbstractDeviceComponentState =
        AbstractDeviceComponentState(
            abstractState = defaultAbstractState(descriptorHandle)
        )

    public fun defaultAbstractComplexDeviceComponentState(descriptorHandle: String): AbstractComplexDeviceComponentState =
        AbstractComplexDeviceComponentState(
            abstractDeviceComponentState = defaultAbstractDeviceComponentState(descriptorHandle)
        )

    public fun defaultAbstractMetricState(descriptorHandle: String): AbstractMetricState = AbstractMetricState(
        abstractState = defaultAbstractState(descriptorHandle),
    )

    public fun defaultAbstractMultiState(descriptorHandle: String, handle: String): AbstractMultiState =
        AbstractMultiState(
            abstractState = defaultAbstractState(descriptorHandle),
            handleAttr = Handle(handle)
        )

    public fun defaultAbstractContextState(descriptorHandle: String, handle: String): AbstractContextState =
        AbstractContextState(
            abstractMultiState = defaultAbstractMultiState(descriptorHandle, handle),
            contextAssociationAttr = ContextAssociation(ContextAssociation.EnumType.Assoc)
        )

    public fun defaultAbstractState(descriptorHandle: String): AbstractState = AbstractState(
        descriptorHandleAttr = HandleRef(descriptorHandle)
    )

    public fun defaultActivateOperationState(descriptorHandle: String): ActivateOperationState = ActivateOperationState(
        abstractOperationState = defaultAbstractOperationState(descriptorHandle)
    )

    public fun defaultAlertConditionState(descriptorHandle: String): AlertConditionState = AlertConditionState(
        abstractAlertState = defaultAbstractAlertState(descriptorHandle)
    )

    public fun defaultAlertSignalState(descriptorHandle: String): AlertSignalState = AlertSignalState(
        abstractAlertState = defaultAbstractAlertState(descriptorHandle)
    )

    public fun defaultAlertSystemState(descriptorHandle: String): AlertSystemState = AlertSystemState(
        abstractAlertState = defaultAbstractAlertState(descriptorHandle)
    )

    public fun defaultBatteryState(descriptorHandle: String): BatteryState = BatteryState(
        abstractDeviceComponentState = defaultAbstractDeviceComponentState(descriptorHandle)
    )

    public fun defaultChannelState(descriptorHandle: String): ChannelState = ChannelState(
        abstractDeviceComponentState = defaultAbstractDeviceComponentState(descriptorHandle)
    )

    public fun defaultClockState(descriptorHandle: String): ClockState = ClockState(
        abstractDeviceComponentState = defaultAbstractDeviceComponentState(descriptorHandle),
        remoteSyncAttr = false
    )

    public fun defaultDistributionSampleArrayMetricState(descriptorHandle: String): DistributionSampleArrayMetricState =
        DistributionSampleArrayMetricState(
            abstractMetricState = defaultAbstractMetricState(descriptorHandle)
        )

    public fun defaultEnsembleContextState(descriptorHandle: String, handle: String): EnsembleContextState =
        EnsembleContextState(
            abstractContextState = defaultAbstractContextState(descriptorHandle, handle)
        )

    public fun defaultEnumStringMetricState(descriptorHandle: String): EnumStringMetricState = EnumStringMetricState(
        stringMetricState = defaultStringMetricState(descriptorHandle)
    )

    public fun defaultLimitAlertConditionState(descriptorHandle: String): LimitAlertConditionState =
        LimitAlertConditionState(
            alertConditionState = defaultAlertConditionState(descriptorHandle),
            limits = Range(),
            monitoredAlertLimitsAttr = AlertConditionMonitoredLimits(AlertConditionMonitoredLimits.EnumType.All)
        )

    public fun defaultLocationContextState(descriptorHandle: String, handle: String): LocationContextState =
        LocationContextState(
            abstractContextState = defaultAbstractContextState(descriptorHandle, handle)
        )

    public fun defaultMdsState(descriptorHandle: String): MdsState = MdsState(
        abstractComplexDeviceComponentState = defaultAbstractComplexDeviceComponentState(descriptorHandle)
    )

    public fun defaultMeansContextState(descriptorHandle: String, handle: String): MeansContextState =
        MeansContextState(
            abstractContextState = defaultAbstractContextState(descriptorHandle, handle)
        )

    public fun defaultNumericMetricState(descriptorHandle: String): NumericMetricState = NumericMetricState(
        abstractMetricState = defaultAbstractMetricState(descriptorHandle)
    )

    public fun defaultOperatorContextState(descriptorHandle: String, handle: String): OperatorContextState =
        OperatorContextState(
            abstractContextState = defaultAbstractContextState(descriptorHandle, handle)
        )

    public fun defaultPatientContextState(descriptorHandle: String, handle: String): PatientContextState =
        PatientContextState(
            abstractContextState = defaultAbstractContextState(descriptorHandle, handle)
        )

    public fun defaultRealTimeSampleArrayMetricState(descriptorHandle: String): RealTimeSampleArrayMetricState =
        RealTimeSampleArrayMetricState(
            abstractMetricState = defaultAbstractMetricState(descriptorHandle)
        )

    public fun defaultScoState(descriptorHandle: String): ScoState = ScoState(
        abstractDeviceComponentState = defaultAbstractDeviceComponentState(descriptorHandle)
    )

    public fun defaultSetAlertStateOperationState(descriptorHandle: String): SetAlertStateOperationState =
        SetAlertStateOperationState(
            abstractOperationState = defaultAbstractOperationState(descriptorHandle)
        )

    public fun defaultSetComponentStateOperationState(descriptorHandle: String): SetComponentStateOperationState =
        SetComponentStateOperationState(
            abstractOperationState = defaultAbstractOperationState(descriptorHandle)
        )

    public fun defaultSetContextStateOperationState(descriptorHandle: String): SetContextStateOperationState =
        SetContextStateOperationState(
            abstractOperationState = defaultAbstractOperationState(descriptorHandle)
        )

    public fun defaultSetMetricStateOperationState(descriptorHandle: String): SetMetricStateOperationState =
        SetMetricStateOperationState(
            abstractOperationState = defaultAbstractOperationState(descriptorHandle)
        )

    public fun defaultSetStringOperationState(descriptorHandle: String): SetStringOperationState =
        SetStringOperationState(
            abstractOperationState = defaultAbstractOperationState(descriptorHandle)
        )

    public fun defaultSetValueOperationState(descriptorHandle: String): SetValueOperationState = SetValueOperationState(
        abstractOperationState = defaultAbstractOperationState(descriptorHandle)
    )

    public fun defaultStringMetricState(descriptorHandle: String): StringMetricState = StringMetricState(
        abstractMetricState = defaultAbstractMetricState(descriptorHandle)
    )

    public fun defaultSystemContextState(descriptorHandle: String): SystemContextState = SystemContextState(
        abstractDeviceComponentState = defaultAbstractDeviceComponentState(descriptorHandle)
    )

    public fun defaultVmdState(descriptorHandle: String): VmdState = VmdState(
        abstractComplexDeviceComponentState = defaultAbstractComplexDeviceComponentState(descriptorHandle)
    )

    public fun defaultWorkflowContextState(descriptorHandle: String, handle: String): WorkflowContextState =
        WorkflowContextState(
            abstractContextState = defaultAbstractContextState(descriptorHandle, handle)
        )
}
package org.somda.protosdc.biceps.common.helper

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.somda.protosdc.biceps.common.MdibDescriptionModifications
import org.somda.protosdc.biceps.common.MdibStateModifications
import org.somda.protosdc.biceps.common.access.MdibAccess
import org.somda.protosdc.biceps.common.access.WriteDescriptionResult
import org.somda.protosdc.biceps.common.access.WriteStateResult
import org.somda.protosdc.biceps.common.storage.MdibStoragePreprocessingChain
import org.somda.protosdc.biceps.common.storage.PreprocessingException
import org.somda.protosdc.common.InstanceId
import org.somda.protosdc.common.InstanceLogger

/**
 * Common code for writing description and states that can be used by BICEPS providers and consumers.
 *
 * @param instanceId instance id used for [InstanceLogger].
 * @param eventDistributor the event distributor to send event messages after write operation is done.
 * @param mdibAccessPreprocessing the preprocessing chain that is invoked before writing to the MDIB storage.
 * @param readMutex a read lock to protect against read access when preprocessing and writing to the MDIB storage.
 * @param mdibAccess the MDIB access that is passed on event distribution.
 */
internal class WriteUtil(
    instanceId: InstanceId,
    private val eventDistributor: EventDistributor,
    private val mdibAccessPreprocessing: MdibStoragePreprocessingChain,
    private val readMutex: Mutex,
    private val mdibAccess: MdibAccess
) {
    private val logger by InstanceLogger(instanceId())

    private val writeMutex = Mutex()

    /**
     * Performs preprocessing, write operation and event distribution of description modifications.
     *
     * This operation is protected against concurrent write operations by using an internal write lock and against
     * read operations by using the object's read lock dependency.
     *
     * @param lockedWriteDescription   locked callback to finally write description.
     * @param descriptionModifications the description modifications to write.
     * @return a write description result that contains inserted, updated and deleted entities.
     * @throws PreprocessingException in case a consistency check or modifier fails.
     */
    @Throws(PreprocessingException::class)
    suspend fun writeDescription(
        descriptionModifications: MdibDescriptionModifications,
        lockedWriteDescription: (MdibDescriptionModifications) -> WriteDescriptionResult
    ): WriteDescriptionResult {
        var startTime = 0L
        logger.debug {
            startTime = System.currentTimeMillis()
            "Start writing description"
        }
        return writeMutex.withLock {
            val modificationResult = readMutex.withLock {
                try {
                    lockedWriteDescription(
                        mdibAccessPreprocessing.processDescriptionModifications(descriptionModifications)
                    )
                } catch (e: PreprocessingException) {
                    logger.warn { e.message }
                    throw e
                }
            }
            var writeEndTime = 0L
            logger.debug {
                writeEndTime = System.currentTimeMillis()
                "MDIB version ${modificationResult.mdibVersion} written in ${writeEndTime - startTime} ms"
            }

            eventDistributor.sendDescriptionModificationEvent(mdibAccess, modificationResult.entities)

            logger.debug {
                val distEndTime = System.currentTimeMillis()
                "Distributing description changes with ${modificationResult.mdibVersion} took ${distEndTime - writeEndTime} ms"
            }

            modificationResult
        }
    }

    /**
     * Performs preprocessing, write operation and event distribution of state modifications.
     *
     * This operation is protected against concurrent write operations by using an internal write lock and against
     * read operations by using the object's read lock dependency.
     *
     * @param lockedWriteStates  locked callback to finally write states.
     * @param stateModifications the state modifications to write.
     * @return a write state result that contains updated states.
     * @throws PreprocessingException in case a consistency check or modifier fails.
     */
    @Throws(PreprocessingException::class)
    suspend fun writeStates(
        stateModifications: MdibStateModifications,
        lockedWriteStates: (MdibStateModifications) -> WriteStateResult
    ): WriteStateResult {
        var startTime: Long = 0
        logger.debug {
            startTime = System.currentTimeMillis()
            "Start writing states"
        }
        return writeMutex.withLock {
            val modificationResult = readMutex.withLock {
                try {
                    lockedWriteStates(
                        mdibAccessPreprocessing.processStateModifications(stateModifications)
                    )
                } catch (e: PreprocessingException) {
                    logger.warn { e.message }
                    throw e
                }
            }

            var writeEndTime = 0L
            logger.debug {
                writeEndTime = System.currentTimeMillis()
                "MDIB version ${modificationResult.mdibVersion} written in ${writeEndTime - startTime} ms"
            }

            eventDistributor.sendStateModificationEvent(
                mdibAccess,
                modificationResult.states
            )

            logger.debug {
                val distEndTime = System.currentTimeMillis()
                "Distributing state changes with ${modificationResult.mdibVersion} took ${distEndTime - writeEndTime} ms"
            }

            modificationResult
        }
    }
}
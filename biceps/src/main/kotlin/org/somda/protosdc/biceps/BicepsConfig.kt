package org.somda.protosdc.biceps

@kotlinx.serialization.Serializable
public data class BicepsConfig(
    /**
     * If true, context states which are not associated are not stored.
     *
     * Since there is no mechanism which allows discarding context states, enabling this feature automatically
     * prunes likely irrelevant data from the storage.
     */
    val storeNotAssociatedContextStates: Boolean = false
)

package org.somda.protosdc.biceps.common.storage

import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import org.somda.protosdc.biceps.common.MdibDescriptionModifications
import org.somda.protosdc.biceps.common.MdibStateModifications

/**
 * Provides a processing chain that is supposed to be run before any interaction with an [MdibStorageImpl] instance.
 *
 * The [MdibStoragePreprocessingChainImpl] offers processing functions for description and state change sets.
 */
internal open class MdibStoragePreprocessingChainImpl @AssistedInject constructor(
    @Assisted private val mdibStorage: MdibStorage,
    @Assisted private val descriptionChainSegments: List<DescriptionPreprocessingSegment>,
    @Assisted private val stateChainSegments: List<StatePreprocessingSegment>
) : MdibStoragePreprocessingChain {
    @AssistedFactory
    interface Factory {
        fun create(
            @Assisted mdibStorage: MdibStorage,
            @Assisted descriptionChainSegments: List<DescriptionPreprocessingSegment>,
            @Assisted stateChainSegments: List<StatePreprocessingSegment>
        ): MdibStoragePreprocessingChainImpl
    }

    override fun processDescriptionModifications(modifications: MdibDescriptionModifications): MdibDescriptionModifications {
        var nextModifications = modifications
        for (chainSegment in descriptionChainSegments) {
            try {
                nextModifications = chainSegment.process(nextModifications, mdibStorage)
            } catch (e: Exception) {
                throw PreprocessingException(
                    "Exception during description modifications processing in segment $chainSegment: ${e.message}",
                    e
                )
            }
        }
        return nextModifications
    }

    override fun processStateModifications(modifications: MdibStateModifications): MdibStateModifications {
        var nextModifications = modifications
        for (chainSegment in stateChainSegments) {
            try {
                nextModifications = chainSegment.process(nextModifications, mdibStorage)
            } catch (e: Exception) {
                throw PreprocessingException(
                    "Exception during state modifications processing in segment $chainSegment: ${e.message}",
                    e
                )
            }
        }
        return nextModifications
    }
}
package org.somda.protosdc.biceps.provider.preprocessing

import org.somda.protosdc.biceps.common.MdibDescriptionModifications
import org.somda.protosdc.biceps.common.descriptorHandleString
import org.somda.protosdc.biceps.common.handleString
import org.somda.protosdc.biceps.common.storage.DescriptionPreprocessingSegment
import org.somda.protosdc.biceps.common.storage.MdibStorage
import javax.inject.Inject

/**
 * Preprocessing segment that checks descriptor references in states.
 *
 * Throws an [IllegalArgumentException] if a state does not reference the descriptor of a modification correctly.
 */
internal class HandleReferenceChecker @Inject constructor() : DescriptionPreprocessingSegment {
    override fun process(
        modifications: MdibDescriptionModifications,
        storage: MdibStorage
    ) = modifications.also {
        modifications.asList().forEach { modification ->
            modification.states
                .firstOrNull { it.descriptorHandleString() != modification.descriptor.handleString() }
                ?.let {
                    throw IllegalArgumentException(
                        "A state's descriptor handle reference of the modification with descriptor handle " +
                                "${modification.descriptor.handleString()} does not match: is ${it.descriptorHandleString()}"
                    )
                }
        }
    }

    override fun toString(): String = javaClass.simpleName
}
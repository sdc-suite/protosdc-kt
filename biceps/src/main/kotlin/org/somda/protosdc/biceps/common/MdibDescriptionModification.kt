package org.somda.protosdc.biceps.common

import org.somda.protosdc.model.biceps.*

/**
 * A container to be used to submit an MDIB description modification.
 *
 * Depending on the enclosed data class, this modification reflects a(n)
 *
 * - insertion: [MdibDescriptionModification.Insert]
 * - update: [MdibDescriptionModification.Insert]
 * - deletion: [MdibDescriptionModification.Delete]
 */
public sealed class MdibDescriptionModification {
    public abstract val handle: String
    public abstract val descriptor: AbstractDescriptorOneOf
    public abstract val states: List<AbstractStateOneOf>
    public abstract val parentHandle: String?

    public data class Insert(public val item: DescriptionItem) : MdibDescriptionModification() {
        public constructor(entity: MdibEntity): this(
            DescriptionItem(
                entity.descriptorOneOf,
                entity.resolveStates(),
                entity.resolveParent()
            )
        )

        override val handle: String = item.descriptor.handleString()
        override val descriptor: AbstractDescriptorOneOf = item.descriptor
        override val states: List<AbstractStateOneOf> = item.states
        override val parentHandle: String? = item.parentHandle
    }

    public data class Update(public val item: DescriptionItem) : MdibDescriptionModification() {
        override public val handle: String = item.descriptor.handleString()
        override public val descriptor: AbstractDescriptorOneOf = item.descriptor
        override public val states: List<AbstractStateOneOf> = item.states
        override public val parentHandle: String? = item.parentHandle
    }

    public data class Delete(public val item: DescriptionItem) : MdibDescriptionModification() {
        public constructor(handle: String): this(
            DescriptionItem(
                AbstractDescriptorOneOf.ChoiceAbstractDescriptor(
                    AbstractDescriptor(handleAttr = Handle(handle))
                ),
                listOf(),
                null
            )
        )

        override val handle: String = item.descriptor.handleString()
        override val descriptor: AbstractDescriptorOneOf = item.descriptor
        override val states: List<AbstractStateOneOf> = item.states
        override val parentHandle: String? = item.parentHandle
    }

    public fun copy(
        descriptor: AbstractDescriptorOneOf = this.descriptor,
        states: List<AbstractStateOneOf> = this.states,
        parentHandle: String? = this.parentHandle
    ): MdibDescriptionModification = when (this) {
        is Delete -> Delete(descriptor.handleString())
        is Insert -> Insert(DescriptionItem(descriptor, states, parentHandle))
        is Update -> Update(DescriptionItem(descriptor, states, parentHandle))
    }
}

public class DescriptionItem public constructor(
    public val descriptor: AbstractDescriptorOneOf,
    public val states: List<AbstractStateOneOf> = listOf(),
    public val parentHandle: String? = null
) {
    public constructor(
        descriptor: ActivateOperationDescriptor,
        state: ActivateOperationState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceActivateOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceActivateOperationState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: AlertConditionDescriptor,
        state: AlertConditionState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceAlertConditionDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceAlertConditionState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: AlertSignalDescriptor,
        state: AlertSignalState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceAlertSignalDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceAlertSignalState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: AlertSystemDescriptor,
        state: AlertSystemState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceAlertSystemDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceAlertSystemState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: BatteryDescriptor,
        state: BatteryState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceBatteryDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceBatteryState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: ChannelDescriptor,
        state: ChannelState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceChannelDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceChannelState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: ClockDescriptor,
        state: ClockState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceClockDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceClockState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: DistributionSampleArrayMetricDescriptor,
        state: DistributionSampleArrayMetricState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceDistributionSampleArrayMetricDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceDistributionSampleArrayMetricState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: EnumStringMetricDescriptor,
        state: EnumStringMetricState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceEnumStringMetricDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceEnumStringMetricState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: LimitAlertConditionDescriptor,
        state: LimitAlertConditionState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceLimitAlertConditionDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceLimitAlertConditionState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: MdsDescriptor,
        state: MdsState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceMdsDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceMdsState(state)) } ?: listOf()
    )

    public constructor(
        descriptor: NumericMetricDescriptor,
        state: NumericMetricState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceNumericMetricDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceNumericMetricState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: RealTimeSampleArrayMetricDescriptor,
        state: RealTimeSampleArrayMetricState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceRealTimeSampleArrayMetricDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: ScoDescriptor,
        state: ScoState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceScoDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceScoState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: SetAlertStateOperationDescriptor,
        state: SetAlertStateOperationState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceSetAlertStateOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSetAlertStateOperationState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: SetComponentStateOperationDescriptor,
        state: SetComponentStateOperationState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceSetComponentStateOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSetComponentStateOperationState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: SetContextStateOperationDescriptor,
        state: SetContextStateOperationState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceSetContextStateOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSetContextStateOperationState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: SetMetricStateOperationDescriptor,
        state: SetMetricStateOperationState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceSetMetricStateOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSetMetricStateOperationState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: SetStringOperationDescriptor,
        state: SetStringOperationState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceSetStringOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSetStringOperationState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: SetValueOperationDescriptor,
        state: SetValueOperationState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceSetValueOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSetValueOperationState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: StringMetricDescriptor,
        state: StringMetricState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceStringMetricDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceStringMetricState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: SystemContextDescriptor,
        state: SystemContextState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceSystemContextDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSystemContextState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: VmdDescriptor,
        state: VmdState? = null,
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceVmdDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceVmdState(state)) } ?: listOf(),
        parentHandle
    )

    public constructor(
        descriptor: EnsembleContextDescriptor,
        states: List<EnsembleContextState> = listOf(),
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceEnsembleContextDescriptor(descriptor),
        states.map { AbstractStateOneOf.ChoiceEnsembleContextState(it) },
        parentHandle
    )

    public constructor(
        descriptor: LocationContextDescriptor,
        states: List<LocationContextState> = listOf(),
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceLocationContextDescriptor(descriptor),
        states.map { AbstractStateOneOf.ChoiceLocationContextState(it) },
        parentHandle
    )

    public constructor(
        descriptor: MeansContextDescriptor,
        states: List<MeansContextState> = listOf(),
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceMeansContextDescriptor(descriptor),
        states.map { AbstractStateOneOf.ChoiceMeansContextState(it) },
        parentHandle
    )

    public constructor(
        descriptor: OperatorContextDescriptor,
        states: List<OperatorContextState> = listOf(),
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceOperatorContextDescriptor(descriptor),
        states.map { AbstractStateOneOf.ChoiceOperatorContextState(it) },
        parentHandle
    )

    public constructor(
        descriptor: PatientContextDescriptor,
        states: List<PatientContextState> = listOf(),
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoicePatientContextDescriptor(descriptor),
        states.map { AbstractStateOneOf.ChoicePatientContextState(it) },
        parentHandle
    )

    public constructor(
        descriptor: WorkflowContextDescriptor,
        states: List<WorkflowContextState> = listOf(),
        parentHandle: String = ""
    ) : this(
        AbstractDescriptorOneOf.ChoiceWorkflowContextDescriptor(descriptor),
        states.map { AbstractStateOneOf.ChoiceWorkflowContextState(it) },
        parentHandle
    )

    // ctor without parent

    public constructor(
        descriptor: ActivateOperationDescriptor,
        state: ActivateOperationState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceActivateOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceActivateOperationState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: AlertConditionDescriptor,
        state: AlertConditionState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceAlertConditionDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceAlertConditionState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: AlertSignalDescriptor,
        state: AlertSignalState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceAlertSignalDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceAlertSignalState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: AlertSystemDescriptor,
        state: AlertSystemState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceAlertSystemDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceAlertSystemState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: BatteryDescriptor,
        state: BatteryState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceBatteryDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceBatteryState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: ChannelDescriptor,
        state: ChannelState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceChannelDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceChannelState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: ClockDescriptor,
        state: ClockState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceClockDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceClockState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: DistributionSampleArrayMetricDescriptor,
        state: DistributionSampleArrayMetricState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceDistributionSampleArrayMetricDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceDistributionSampleArrayMetricState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: EnumStringMetricDescriptor,
        state: EnumStringMetricState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceEnumStringMetricDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceEnumStringMetricState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: LimitAlertConditionDescriptor,
        state: LimitAlertConditionState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceLimitAlertConditionDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceLimitAlertConditionState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: NumericMetricDescriptor,
        state: NumericMetricState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceNumericMetricDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceNumericMetricState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: RealTimeSampleArrayMetricDescriptor,
        state: RealTimeSampleArrayMetricState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceRealTimeSampleArrayMetricDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: ScoDescriptor,
        state: ScoState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceScoDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceScoState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: SetAlertStateOperationDescriptor,
        state: SetAlertStateOperationState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceSetAlertStateOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSetAlertStateOperationState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: SetComponentStateOperationDescriptor,
        state: SetComponentStateOperationState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceSetComponentStateOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSetComponentStateOperationState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: SetContextStateOperationDescriptor,
        state: SetContextStateOperationState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceSetContextStateOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSetContextStateOperationState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: SetMetricStateOperationDescriptor,
        state: SetMetricStateOperationState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceSetMetricStateOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSetMetricStateOperationState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: SetStringOperationDescriptor,
        state: SetStringOperationState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceSetStringOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSetStringOperationState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: SetValueOperationDescriptor,
        state: SetValueOperationState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceSetValueOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSetValueOperationState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: StringMetricDescriptor,
        state: StringMetricState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceStringMetricDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceStringMetricState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: SystemContextDescriptor,
        state: SystemContextState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceSystemContextDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSystemContextState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: VmdDescriptor,
        state: VmdState? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceVmdDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceVmdState(state)) } ?: listOf(),
        null
    )

    public constructor(
        descriptor: EnsembleContextDescriptor,
        states: List<EnsembleContextState> = listOf()
    ) : this(
        AbstractDescriptorOneOf.ChoiceEnsembleContextDescriptor(descriptor),
        states.map { AbstractStateOneOf.ChoiceEnsembleContextState(it) },
        null
    )

    public constructor(
        descriptor: LocationContextDescriptor,
        states: List<LocationContextState> = listOf()
    ) : this(
        AbstractDescriptorOneOf.ChoiceLocationContextDescriptor(descriptor),
        states.map { AbstractStateOneOf.ChoiceLocationContextState(it) },
        null
    )

    public constructor(
        descriptor: MeansContextDescriptor,
        states: List<MeansContextState> = listOf()
    ) : this(
        AbstractDescriptorOneOf.ChoiceMeansContextDescriptor(descriptor),
        states.map { AbstractStateOneOf.ChoiceMeansContextState(it) },
        null
    )

    public constructor(
        descriptor: OperatorContextDescriptor,
        states: List<OperatorContextState> = listOf()
    ) : this(
        AbstractDescriptorOneOf.ChoiceOperatorContextDescriptor(descriptor),
        states.map { AbstractStateOneOf.ChoiceOperatorContextState(it) },
        null
    )

    public constructor(
        descriptor: PatientContextDescriptor,
        states: List<PatientContextState> = listOf()
    ) : this(
        AbstractDescriptorOneOf.ChoicePatientContextDescriptor(descriptor),
        states.map { AbstractStateOneOf.ChoicePatientContextState(it) },
        null
    )

    public constructor(
        descriptor: WorkflowContextDescriptor,
        states: List<WorkflowContextState> = listOf()
    ) : this(
        AbstractDescriptorOneOf.ChoiceWorkflowContextDescriptor(descriptor),
        states.map { AbstractStateOneOf.ChoiceWorkflowContextState(it) },
        null
    )

    // --- ctor overload with Handle class

    public constructor(
        descriptor: ActivateOperationDescriptor,
        state: ActivateOperationState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceActivateOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceActivateOperationState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: AlertConditionDescriptor,
        state: AlertConditionState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceAlertConditionDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceAlertConditionState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: AlertSignalDescriptor,
        state: AlertSignalState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceAlertSignalDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceAlertSignalState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: AlertSystemDescriptor,
        state: AlertSystemState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceAlertSystemDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceAlertSystemState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: BatteryDescriptor,
        state: BatteryState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceBatteryDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceBatteryState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: ChannelDescriptor,
        state: ChannelState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceChannelDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceChannelState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: ClockDescriptor,
        state: ClockState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceClockDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceClockState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: DistributionSampleArrayMetricDescriptor,
        state: DistributionSampleArrayMetricState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceDistributionSampleArrayMetricDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceDistributionSampleArrayMetricState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: EnumStringMetricDescriptor,
        state: EnumStringMetricState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceEnumStringMetricDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceEnumStringMetricState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: LimitAlertConditionDescriptor,
        state: LimitAlertConditionState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceLimitAlertConditionDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceLimitAlertConditionState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: NumericMetricDescriptor,
        state: NumericMetricState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceNumericMetricDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceNumericMetricState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: RealTimeSampleArrayMetricDescriptor,
        state: RealTimeSampleArrayMetricState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceRealTimeSampleArrayMetricDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: ScoDescriptor,
        state: ScoState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceScoDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceScoState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: SetAlertStateOperationDescriptor,
        state: SetAlertStateOperationState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceSetAlertStateOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSetAlertStateOperationState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: SetComponentStateOperationDescriptor,
        state: SetComponentStateOperationState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceSetComponentStateOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSetComponentStateOperationState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: SetContextStateOperationDescriptor,
        state: SetContextStateOperationState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceSetContextStateOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSetContextStateOperationState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: SetMetricStateOperationDescriptor,
        state: SetMetricStateOperationState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceSetMetricStateOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSetMetricStateOperationState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: SetStringOperationDescriptor,
        state: SetStringOperationState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceSetStringOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSetStringOperationState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: SetValueOperationDescriptor,
        state: SetValueOperationState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceSetValueOperationDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSetValueOperationState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: StringMetricDescriptor,
        state: StringMetricState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceStringMetricDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceStringMetricState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: SystemContextDescriptor,
        state: SystemContextState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceSystemContextDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceSystemContextState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: VmdDescriptor,
        state: VmdState? = null,
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceVmdDescriptor(descriptor),
        state?.let { listOf(AbstractStateOneOf.ChoiceVmdState(state)) } ?: listOf(),
        parentHandle?.string
    )

    public constructor(
        descriptor: EnsembleContextDescriptor,
        states: List<EnsembleContextState> = listOf(),
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceEnsembleContextDescriptor(descriptor),
        states.map { AbstractStateOneOf.ChoiceEnsembleContextState(it) },
        parentHandle?.string
    )

    public constructor(
        descriptor: LocationContextDescriptor,
        states: List<LocationContextState> = listOf(),
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceLocationContextDescriptor(descriptor),
        states.map { AbstractStateOneOf.ChoiceLocationContextState(it) },
        parentHandle?.string
    )

    public constructor(
        descriptor: MeansContextDescriptor,
        states: List<MeansContextState> = listOf(),
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceMeansContextDescriptor(descriptor),
        states.map { AbstractStateOneOf.ChoiceMeansContextState(it) },
        parentHandle?.string
    )

    public constructor(
        descriptor: OperatorContextDescriptor,
        states: List<OperatorContextState> = listOf(),
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceOperatorContextDescriptor(descriptor),
        states.map { AbstractStateOneOf.ChoiceOperatorContextState(it) },
        parentHandle?.string
    )

    public constructor(
        descriptor: PatientContextDescriptor,
        states: List<PatientContextState> = listOf(),
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoicePatientContextDescriptor(descriptor),
        states.map { AbstractStateOneOf.ChoicePatientContextState(it) },
        parentHandle?.string
    )

    public constructor(
        descriptor: WorkflowContextDescriptor,
        states: List<WorkflowContextState> = listOf(),
        parentHandle: HandleRef? = null
    ) : this(
        AbstractDescriptorOneOf.ChoiceWorkflowContextDescriptor(descriptor),
        states.map { AbstractStateOneOf.ChoiceWorkflowContextState(it) },
        parentHandle?.string
    )
}
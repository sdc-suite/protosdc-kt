package org.somda.protosdc.biceps.common.access

import org.somda.protosdc.biceps.common.MdibVersion
import org.somda.protosdc.biceps.common.WrittenMdibStateModifications

/**
 * Result set of a write states call.
 *
 * @param mdibVersion the written MDIB version.
 * @param states the list of states that were written.
 */
public data class WriteStateResult(val mdibVersion: MdibVersion, val states: WrittenMdibStateModifications)
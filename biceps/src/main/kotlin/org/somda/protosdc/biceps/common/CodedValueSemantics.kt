package org.somda.protosdc.biceps.common

import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc.model.biceps.CodedValue
import org.somda.protosdc.common.Oid
import java.lang.NumberFormatException

private object Logger : Logging

// checks if a coded value with a versioned coding system does not contain a coding system version attribute,
// moves the version from the OID to the attribute and returns a new coded value based on moved data.
private fun versioned10101Check(codedValue: CodedValue, against: Oid): CodedValue {
    val versionedCodingSystem = codedValue.codingSystemOid()
    require(versionedCodingSystem.components.size == against.components.size + 1) {
        "Expected ${codedValue.codingSystem()} to contain a version sub-arc"
    }
    require(codedValue.codingSystemVersionAttr == null) {
        "CodedValue with versioned coding system must not have a codingSystemVersion attribute: $codedValue"
    }
    return codedValue.copy(
        codingSystemAttr = NomenclatureConstants.IEEE_11073_10101_OID.appendArc(versionedCodingSystem.components.last())
            .toUri()
            .toString()
    )
}

// checks for 11073-10101 synonyms and replaces it with the official nomenclature coding system OID
// caution: does not support Constants.IEEE_NOMENCLATURE_URN
private fun sanitizeCodedValue(codedValue: CodedValue) = runCatching {
    codedValue.codingSystemOid().let {
        if (
            it == NomenclatureConstants.IEEE_11073_10101_OID ||
            it == NomenclatureConstants.IEEE_11073_10101_LEGACY_OID ||
            it == NomenclatureConstants.IEEE_11073_10101_HL7_OID
        ) {
            require(!codedValue.codingSystemVersionAttr.isNullOrEmpty()) {
                "An IEEE 11073-10101 coded value needs to be versioned"
            }
            return@let CodedValue(
                codingSystemAttr = NomenclatureConstants.IEEE_11073_10101_OID.toUri().toString(),
                codingSystemVersionAttr = codedValue.codingSystemVersionAttr,
                codeAttr = codedValue.codeAttr
            )
        } else if (NomenclatureConstants.IEEE_11073_10101_OID.isPrefixOf(it)) {
            return@let versioned10101Check(codedValue, NomenclatureConstants.IEEE_11073_10101_OID)
        } else if (NomenclatureConstants.IEEE_11073_10101_LEGACY_OID.isPrefixOf(it)) {
            return@let versioned10101Check(codedValue, NomenclatureConstants.IEEE_11073_10101_LEGACY_OID)
        } else if (NomenclatureConstants.IEEE_11073_10101_HL7_OID.isPrefixOf(it)) {
            return@let versioned10101Check(codedValue, NomenclatureConstants.IEEE_11073_10101_HL7_OID)
        }
        throw Exception("Unknown coding system")
    }
}.getOrThrow()

/**
 * Checks against semantic equality based on IEEE 11073-10101 code semantics.
 *
 * Uses the OID sub-arcs or version attribute for version comparison, if available.
 *
 * - Throws [IllegalArgumentException] if a coded value with a versioned coding system contains a present version attribute.
 * - Throws [NumberFormatException] if the function attempted to convert an IEEE 11073-10101 code string to an integer,
 *   but that the string does not have the appropriate format.
 *
 * Be advised that there is another OID scheme representing the 10101 nomenclature standards, which does not support a
 * versioning scheme: *1.3.6.1.4.1.19376.1.6.7.1* as described [here](https://wiki.ihe.net/index.php/PCD_OID_Management).
 * As this scheme appears to be broken and is not officially listed in the IEEE 11073-10101 standards, it is not being
 * used as a valid coding system herein.
 *
 * @param rhs right-hand-side to compare this coded value with.
 * @return true if the coded values reference the same concept, false if there was not enough information available to
 * infer semantic equality, i.e. either the codes do not reference the same concept or the coding systems were unknown.
 */
public fun CodedValue.isSemanticallyEqual(rhs: CodedValue): Boolean {
    for (lhsCode in this.allCodes()) {
        for (rhsCode in rhs.allCodes()) {
            if (lhsCode.codingSystemUri() == NomenclatureConstants.IEEE_11073_10101_URN &&
                rhsCode.codingSystemUri() == NomenclatureConstants.IEEE_11073_10101_URN
            ) {
                if (lhsCode.codeAttr.string.toLong() == rhsCode.codeAttr.string.toLong() &&
                    lhsCode.codingSystemVersionAttr != null &&
                    rhsCode.codingSystemVersionAttr != null &&
                    lhsCode.codingSystemVersionAttr == rhsCode.codingSystemVersionAttr
                ) {
                    return true
                }
            }

            try {
                val sanitizedLhs = sanitizeCodedValue(lhsCode)
                val sanitizedRhs = sanitizeCodedValue(rhsCode)
                if (sanitizedLhs.codingSystem() == sanitizedRhs.codingSystem() &&
                    lhsCode.codeAttr.string.toLong() == rhsCode.codeAttr.string.toLong() &&
                    lhsCode.codingSystemVersionAttr == rhsCode.codingSystemVersionAttr
                ) {
                    return true
                }
            } catch (e: NumberFormatException) {
                // explicitly throw on conversion errors as the input is not trustworthy any longer
                throw e
            } catch (e: Exception) {
                Logger.logger.debug { "Could not compare coded values. Cause: ${e.message})" }
                Logger.logger.trace(e) { "Could not compare coded values. Cause: ${e.message})" }
            }
        }
    }

    return false
}
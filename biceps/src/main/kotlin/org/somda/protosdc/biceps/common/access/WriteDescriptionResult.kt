package org.somda.protosdc.biceps.common.access

import org.somda.protosdc.biceps.common.MdibVersion
import org.somda.protosdc.biceps.common.WrittenMdibDescriptionModifications

/**
 * Result set of a write description call.
 *
 * @param mdibVersion the written MDIB version.
 * @param entities all written entities.
 */
public data class WriteDescriptionResult(
    val mdibVersion: MdibVersion,
    val entities: WrittenMdibDescriptionModifications
)
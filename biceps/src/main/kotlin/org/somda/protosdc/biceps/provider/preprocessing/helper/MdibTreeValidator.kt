package org.somda.protosdc.biceps.provider.preprocessing.helper

import com.google.common.collect.HashMultimap
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf
import kotlin.reflect.KClass

/**
 * Utility class to verify cardinality and parent-child type correctness.
 */
internal class MdibTreeValidator {
    private val allowedParents: HashMultimap<KClass<out AbstractDescriptorOneOf>, KClass<out AbstractDescriptorOneOf>> = HashMultimap.create()
    private val oneChildEntities: MutableSet<KClass<out AbstractDescriptorOneOf>> = mutableSetOf()

    init {
        setupAllowedParents()
        setupChildCardinality()
    }

    /**
     * Checks if a descriptor is allowed to appear many times as a child.
     *
     * @param child the descriptor to check.
     * @return true if it is allowed to appear more than once, false otherwise.
     */
    fun isManyAllowed(child: AbstractDescriptorOneOf): Boolean {
        return !oneChildEntities.contains(child::class)
    }

    /**
     * Checks if the parent child descriptor relation is eligible.
     *
     * @param parent the parent descriptor to check against.
     * @param child  the child descriptor to check against.
     * @return true if parent child relationship is eligible in terms of the BICEPS hierarchy.
     */
    fun isValidParent(parent: AbstractDescriptorOneOf, child: AbstractDescriptorOneOf): Boolean {
        return allowedParents.containsEntry(child::class, parent::class)
    }

    /**
     * Resolves allowed parents descriptor type for a given child descriptor type.
     *
     * @param child the child where to retrieve parents for.
     * @return a set of parent descriptor classes.
     */
    fun allowedParents(child: AbstractDescriptorOneOf): Set<KClass<out AbstractDescriptorOneOf>> {
        return allowedParents[child::class]
    }

    private fun setupAllowedParents() {
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceVmdDescriptor::class,
            AbstractDescriptorOneOf.ChoiceMdsDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceChannelDescriptor::class,
            AbstractDescriptorOneOf.ChoiceVmdDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceNumericMetricDescriptor::class,
            AbstractDescriptorOneOf.ChoiceChannelDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceStringMetricDescriptor::class,
            AbstractDescriptorOneOf.ChoiceChannelDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceEnumStringMetricDescriptor::class,
            AbstractDescriptorOneOf.ChoiceChannelDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceRealTimeSampleArrayMetricDescriptor::class,
            AbstractDescriptorOneOf.ChoiceChannelDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceDistributionSampleArrayMetricDescriptor::class,
            AbstractDescriptorOneOf.ChoiceChannelDescriptor::class
        )
        allowedParents.replaceValues(
            AbstractDescriptorOneOf.ChoiceAlertSystemDescriptor::class,
            listOf(
                AbstractDescriptorOneOf.ChoiceMdsDescriptor::class,
                AbstractDescriptorOneOf.ChoiceVmdDescriptor::class
            )
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceAlertConditionDescriptor::class,
            AbstractDescriptorOneOf.ChoiceAlertSystemDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceLimitAlertConditionDescriptor::class,
            AbstractDescriptorOneOf.ChoiceAlertSystemDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceAlertSignalDescriptor::class,
            AbstractDescriptorOneOf.ChoiceAlertSystemDescriptor::class
        )
        allowedParents.replaceValues(
            AbstractDescriptorOneOf.ChoiceScoDescriptor::class,
            listOf(
                AbstractDescriptorOneOf.ChoiceMdsDescriptor::class,
                AbstractDescriptorOneOf.ChoiceVmdDescriptor::class
            )
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceActivateOperationDescriptor::class,
            AbstractDescriptorOneOf.ChoiceScoDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceSetStringOperationDescriptor::class,
            AbstractDescriptorOneOf.ChoiceScoDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceSetValueOperationDescriptor::class,
            AbstractDescriptorOneOf.ChoiceScoDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceSetComponentStateOperationDescriptor::class,
            AbstractDescriptorOneOf.ChoiceScoDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceSetMetricStateOperationDescriptor::class,
            AbstractDescriptorOneOf.ChoiceScoDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceSetContextStateOperationDescriptor::class,
            AbstractDescriptorOneOf.ChoiceScoDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceSetAlertStateOperationDescriptor::class,
            AbstractDescriptorOneOf.ChoiceScoDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceClockDescriptor::class,
            AbstractDescriptorOneOf.ChoiceMdsDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceBatteryDescriptor::class,
            AbstractDescriptorOneOf.ChoiceMdsDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceSystemContextDescriptor::class,
            AbstractDescriptorOneOf.ChoiceMdsDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoicePatientContextDescriptor::class,
            AbstractDescriptorOneOf.ChoiceSystemContextDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceLocationContextDescriptor::class,
            AbstractDescriptorOneOf.ChoiceSystemContextDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceEnsembleContextDescriptor::class,
            AbstractDescriptorOneOf.ChoiceSystemContextDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceWorkflowContextDescriptor::class,
            AbstractDescriptorOneOf.ChoiceSystemContextDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceMeansContextDescriptor::class,
            AbstractDescriptorOneOf.ChoiceSystemContextDescriptor::class
        )
        allowedParents.put(
            AbstractDescriptorOneOf.ChoiceOperatorContextDescriptor::class,
            AbstractDescriptorOneOf.ChoiceSystemContextDescriptor::class
        )
    }

    private fun setupChildCardinality() {
        oneChildEntities.add(AbstractDescriptorOneOf.ChoiceAlertSystemDescriptor::class)
        oneChildEntities.add(AbstractDescriptorOneOf.ChoiceSystemContextDescriptor::class)
        oneChildEntities.add(AbstractDescriptorOneOf.ChoiceScoDescriptor::class)
        oneChildEntities.add(AbstractDescriptorOneOf.ChoicePatientContextDescriptor::class)
        oneChildEntities.add(AbstractDescriptorOneOf.ChoiceLocationContextDescriptor::class)
    }
}
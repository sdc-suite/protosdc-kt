package org.somda.protosdc.biceps.common.preprocessing

import org.somda.protosdc.biceps.common.MdibDescriptionModifications
import org.somda.protosdc.biceps.common.storage.DescriptionPreprocessingSegment
import org.somda.protosdc.biceps.common.storage.MdibStorage
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf
import javax.inject.Inject

/**
 * Removes children from descriptors in order to avoid redundant information in the MDIB storage.
 */
internal class DescriptorChildRemover @Inject constructor() : DescriptionPreprocessingSegment {
    override fun process(
        modifications: MdibDescriptionModifications,
        storage: MdibStorage
    ) = MdibDescriptionModifications().addAll(modifications.asList().map { modification ->
        when (val descriptor = modification.descriptor) {
            is AbstractDescriptorOneOf.ChoiceAlertSystemDescriptor ->
                descriptor.copy(
                    value = descriptor.value.copy(
                        alertCondition = listOf(),
                        alertSignal = listOf()
                    )
                )
            is AbstractDescriptorOneOf.ChoiceChannelDescriptor ->
                descriptor.copy(
                    value = descriptor.value.copy(
                        metric = listOf()
                    )
                )
            is AbstractDescriptorOneOf.ChoiceScoDescriptor ->
                descriptor.copy(
                    value = descriptor.value.copy(
                        operation = listOf()
                    )
                )
            is AbstractDescriptorOneOf.ChoiceSystemContextDescriptor ->
                descriptor.copy(
                    value = descriptor.value.copy(
                        ensembleContext = listOf(),
                        locationContext = null,
                        meansContext = listOf(),
                        operatorContext = listOf(),
                        patientContext = null,
                        workflowContext = listOf()
                    )
                )
            is AbstractDescriptorOneOf.ChoiceVmdDescriptor ->
                descriptor.copy(
                    value = descriptor.value.copy(
                        channel = listOf(),
                        abstractComplexDeviceComponentDescriptor = descriptor.value.abstractComplexDeviceComponentDescriptor.copy(
                            alertSystem = null,
                            sco = null
                        )
                    )
                )
            is AbstractDescriptorOneOf.ChoiceMdsDescriptor ->
                descriptor.copy(
                    value = descriptor.value.copy(
                        vmd = listOf(),
                        battery = listOf(),
                        clock = null,
                        systemContext = null,
                        abstractComplexDeviceComponentDescriptor = descriptor.value.abstractComplexDeviceComponentDescriptor.copy(
                            alertSystem = null,
                            sco = null
                        )
                    )
                )
            else -> descriptor
        }.let { modification.copy(descriptor = it) }
    })
}

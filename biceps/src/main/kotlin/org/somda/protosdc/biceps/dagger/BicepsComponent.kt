package org.somda.protosdc.biceps.dagger

import dagger.BindsInstance
import dagger.Component
import dagger.Subcomponent
import org.somda.protosdc.biceps.BicepsConfig
import org.somda.protosdc.biceps.consumer.access.RemoteMdibAccess
import org.somda.protosdc.biceps.provider.access.LocalMdibAccess
import org.somda.protosdc.common.ComponentConfig
import org.somda.protosdc.common.InstanceId
import javax.annotation.Nullable
import javax.inject.Scope
import javax.inject.Singleton

@Scope
@Retention(value = AnnotationRetention.RUNTIME)
internal annotation class BicepsScope

@Singleton
@Component(modules = [BicepsModule::class])
internal interface BicepsComponent {
    @Component.Builder
    interface Builder {
        fun bind(@BindsInstance @ComponentConfig instanceId: InstanceId): Builder
        fun bind(@BindsInstance @ComponentConfig @Nullable bicepsConfig: BicepsConfig): Builder
        fun build(): BicepsComponent
    }

    // todo remove when migrating protoSdc project
    fun localMdibAccessFactory(): LocalMdibAccess.Factory
    fun remoteMdibAccessFactory(): RemoteMdibAccess.Factory
}
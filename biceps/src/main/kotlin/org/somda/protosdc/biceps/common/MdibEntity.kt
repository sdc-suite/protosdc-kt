package org.somda.protosdc.biceps.common

import org.somda.protosdc.model.biceps.AbstractAlertDescriptor
import org.somda.protosdc.model.biceps.AbstractAlertState
import org.somda.protosdc.model.biceps.AbstractComplexDeviceComponentDescriptor
import org.somda.protosdc.model.biceps.AbstractComplexDeviceComponentState
import org.somda.protosdc.model.biceps.AbstractContextDescriptor
import org.somda.protosdc.model.biceps.AbstractContextState
import org.somda.protosdc.model.biceps.AbstractContextStateOneOf
import org.somda.protosdc.model.biceps.AbstractDescriptor
import org.somda.protosdc.model.biceps.AbstractDescriptorOneOf
import org.somda.protosdc.model.biceps.AbstractDeviceComponentDescriptor
import org.somda.protosdc.model.biceps.AbstractDeviceComponentState
import org.somda.protosdc.model.biceps.AbstractMetricDescriptor
import org.somda.protosdc.model.biceps.AbstractMetricState
import org.somda.protosdc.model.biceps.AbstractMultiState
import org.somda.protosdc.model.biceps.AbstractOperationDescriptor
import org.somda.protosdc.model.biceps.AbstractOperationState
import org.somda.protosdc.model.biceps.AbstractSetStateOperationDescriptor
import org.somda.protosdc.model.biceps.AbstractState
import org.somda.protosdc.model.biceps.AbstractStateOneOf
import org.somda.protosdc.model.biceps.ActivateOperationDescriptor
import org.somda.protosdc.model.biceps.ActivateOperationState
import org.somda.protosdc.model.biceps.AlertConditionDescriptor
import org.somda.protosdc.model.biceps.AlertConditionState
import org.somda.protosdc.model.biceps.AlertSignalDescriptor
import org.somda.protosdc.model.biceps.AlertSignalState
import org.somda.protosdc.model.biceps.AlertSystemDescriptor
import org.somda.protosdc.model.biceps.AlertSystemState
import org.somda.protosdc.model.biceps.BatteryDescriptor
import org.somda.protosdc.model.biceps.BatteryState
import org.somda.protosdc.model.biceps.ChannelDescriptor
import org.somda.protosdc.model.biceps.ChannelState
import org.somda.protosdc.model.biceps.ClockDescriptor
import org.somda.protosdc.model.biceps.ClockState
import org.somda.protosdc.model.biceps.DistributionSampleArrayMetricDescriptor
import org.somda.protosdc.model.biceps.DistributionSampleArrayMetricState
import org.somda.protosdc.model.biceps.EnsembleContextDescriptor
import org.somda.protosdc.model.biceps.EnsembleContextState
import org.somda.protosdc.model.biceps.EnumStringMetricDescriptor
import org.somda.protosdc.model.biceps.EnumStringMetricState
import org.somda.protosdc.model.biceps.LimitAlertConditionDescriptor
import org.somda.protosdc.model.biceps.LimitAlertConditionState
import org.somda.protosdc.model.biceps.LocationContextDescriptor
import org.somda.protosdc.model.biceps.LocationContextState
import org.somda.protosdc.model.biceps.MdsDescriptor
import org.somda.protosdc.model.biceps.MdsState
import org.somda.protosdc.model.biceps.MeansContextDescriptor
import org.somda.protosdc.model.biceps.MeansContextState
import org.somda.protosdc.model.biceps.NumericMetricDescriptor
import org.somda.protosdc.model.biceps.NumericMetricState
import org.somda.protosdc.model.biceps.OperatorContextDescriptor
import org.somda.protosdc.model.biceps.OperatorContextState
import org.somda.protosdc.model.biceps.PatientContextDescriptor
import org.somda.protosdc.model.biceps.PatientContextState
import org.somda.protosdc.model.biceps.RealTimeSampleArrayMetricDescriptor
import org.somda.protosdc.model.biceps.RealTimeSampleArrayMetricState
import org.somda.protosdc.model.biceps.ScoDescriptor
import org.somda.protosdc.model.biceps.ScoState
import org.somda.protosdc.model.biceps.SetAlertStateOperationDescriptor
import org.somda.protosdc.model.biceps.SetAlertStateOperationState
import org.somda.protosdc.model.biceps.SetComponentStateOperationDescriptor
import org.somda.protosdc.model.biceps.SetComponentStateOperationState
import org.somda.protosdc.model.biceps.SetContextStateOperationDescriptor
import org.somda.protosdc.model.biceps.SetContextStateOperationState
import org.somda.protosdc.model.biceps.SetMetricStateOperationDescriptor
import org.somda.protosdc.model.biceps.SetMetricStateOperationState
import org.somda.protosdc.model.biceps.SetStringOperationDescriptor
import org.somda.protosdc.model.biceps.SetStringOperationState
import org.somda.protosdc.model.biceps.SetValueOperationDescriptor
import org.somda.protosdc.model.biceps.SetValueOperationState
import org.somda.protosdc.model.biceps.StringMetricDescriptor
import org.somda.protosdc.model.biceps.StringMetricState
import org.somda.protosdc.model.biceps.SystemContextDescriptor
import org.somda.protosdc.model.biceps.SystemContextState
import org.somda.protosdc.model.biceps.VmdDescriptor
import org.somda.protosdc.model.biceps.VmdState
import org.somda.protosdc.model.biceps.WorkflowContextDescriptor
import org.somda.protosdc.model.biceps.WorkflowContextState
import kotlin.reflect.KClass

/**
 * Representation of a containment tree entry including state, parent and child information.
 *
 * Any entity contains a list of states, a descriptor and an optional set of children.
 * [MdibEntity] is an immutable class with read access only.
 */
public sealed interface MdibEntity {
    public companion object {
        public fun typeFromDescriptorOneOf(oneOf: AbstractDescriptorOneOf): KClass<out MdibEntity> = when (oneOf) {
            is AbstractDescriptorOneOf.ChoiceAlertConditionDescriptor -> AlertCondition::class
            is AbstractDescriptorOneOf.ChoiceLimitAlertConditionDescriptor -> LimitAlertCondition::class
            is AbstractDescriptorOneOf.ChoiceAlertSignalDescriptor -> AlertSignal::class
            is AbstractDescriptorOneOf.ChoiceAlertSystemDescriptor -> AlertSystem::class
            is AbstractDescriptorOneOf.ChoiceClockDescriptor -> Clock::class
            is AbstractDescriptorOneOf.ChoiceBatteryDescriptor -> Battery::class
            is AbstractDescriptorOneOf.ChoiceChannelDescriptor -> Channel::class
            is AbstractDescriptorOneOf.ChoiceScoDescriptor -> Sco::class
            is AbstractDescriptorOneOf.ChoiceSystemContextDescriptor -> SystemContext::class
            is AbstractDescriptorOneOf.ChoiceVmdDescriptor -> Vmd::class
            is AbstractDescriptorOneOf.ChoiceMdsDescriptor -> Mds::class
            is AbstractDescriptorOneOf.ChoiceSetStringOperationDescriptor -> SetStringOperation::class
            is AbstractDescriptorOneOf.ChoiceSetComponentStateOperationDescriptor -> SetComponentStateOperation::class
            is AbstractDescriptorOneOf.ChoiceSetAlertStateOperationDescriptor -> SetAlertStateOperation::class
            is AbstractDescriptorOneOf.ChoiceSetMetricStateOperationDescriptor -> SetMetricStateOperation::class
            is AbstractDescriptorOneOf.ChoiceSetContextStateOperationDescriptor -> SetContextStateOperation::class
            is AbstractDescriptorOneOf.ChoiceActivateOperationDescriptor -> ActivateOperation::class
            is AbstractDescriptorOneOf.ChoiceSetValueOperationDescriptor -> SetValueOperation::class
            is AbstractDescriptorOneOf.ChoiceNumericMetricDescriptor -> NumericMetric::class
            is AbstractDescriptorOneOf.ChoiceDistributionSampleArrayMetricDescriptor -> DistributionSampleArrayMetric::class
            is AbstractDescriptorOneOf.ChoiceRealTimeSampleArrayMetricDescriptor -> RealTimeSampleArrayMetric::class
            is AbstractDescriptorOneOf.ChoiceStringMetricDescriptor -> StringMetric::class
            is AbstractDescriptorOneOf.ChoiceEnumStringMetricDescriptor -> EnumStringMetric::class
            is AbstractDescriptorOneOf.ChoiceMeansContextDescriptor -> MeansContext::class
            is AbstractDescriptorOneOf.ChoiceWorkflowContextDescriptor -> WorkflowContext::class
            is AbstractDescriptorOneOf.ChoicePatientContextDescriptor -> PatientContext::class
            is AbstractDescriptorOneOf.ChoiceOperatorContextDescriptor -> OperatorContext::class
            is AbstractDescriptorOneOf.ChoiceEnsembleContextDescriptor -> EnsembleContext::class
            is AbstractDescriptorOneOf.ChoiceLocationContextDescriptor -> LocationContext::class
            else -> throw InvalidWhenBranchException(oneOf)
        }

        public fun typeFromStateOneOf(oneOf: AbstractStateOneOf): KClass<out MdibEntity> = when (oneOf) {
            is AbstractStateOneOf.ChoiceAlertConditionState -> AlertCondition::class
            is AbstractStateOneOf.ChoiceLimitAlertConditionState -> LimitAlertCondition::class
            is AbstractStateOneOf.ChoiceAlertSignalState -> AlertSignal::class
            is AbstractStateOneOf.ChoiceAlertSystemState -> AlertSystem::class
            is AbstractStateOneOf.ChoiceClockState -> Clock::class
            is AbstractStateOneOf.ChoiceBatteryState -> Battery::class
            is AbstractStateOneOf.ChoiceChannelState -> Channel::class
            is AbstractStateOneOf.ChoiceScoState -> Sco::class
            is AbstractStateOneOf.ChoiceSystemContextState -> SystemContext::class
            is AbstractStateOneOf.ChoiceVmdState -> Vmd::class
            is AbstractStateOneOf.ChoiceMdsState -> Mds::class
            is AbstractStateOneOf.ChoiceSetStringOperationState -> SetStringOperation::class
            is AbstractStateOneOf.ChoiceSetComponentStateOperationState -> SetComponentStateOperation::class
            is AbstractStateOneOf.ChoiceSetAlertStateOperationState -> SetAlertStateOperation::class
            is AbstractStateOneOf.ChoiceSetMetricStateOperationState -> SetMetricStateOperation::class
            is AbstractStateOneOf.ChoiceSetContextStateOperationState -> SetContextStateOperation::class
            is AbstractStateOneOf.ChoiceActivateOperationState -> ActivateOperation::class
            is AbstractStateOneOf.ChoiceSetValueOperationState -> SetValueOperation::class
            is AbstractStateOneOf.ChoiceNumericMetricState -> NumericMetric::class
            is AbstractStateOneOf.ChoiceDistributionSampleArrayMetricState -> DistributionSampleArrayMetric::class
            is AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState -> RealTimeSampleArrayMetric::class
            is AbstractStateOneOf.ChoiceStringMetricState -> StringMetric::class
            is AbstractStateOneOf.ChoiceEnumStringMetricState -> EnumStringMetric::class
            is AbstractStateOneOf.ChoiceMeansContextState -> MeansContext::class
            is AbstractStateOneOf.ChoiceWorkflowContextState -> WorkflowContext::class
            is AbstractStateOneOf.ChoicePatientContextState -> PatientContext::class
            is AbstractStateOneOf.ChoiceOperatorContextState -> OperatorContext::class
            is AbstractStateOneOf.ChoiceEnsembleContextState -> EnsembleContext::class
            is AbstractStateOneOf.ChoiceLocationContextState -> LocationContext::class
            else -> throw InvalidWhenBranchException(oneOf)
        }
    }

    public val descriptorOneOf: AbstractDescriptorOneOf

    public val lastChanged: MdibVersion
    public val handle: String
        get() = descriptorOneOf.handleString()
    public val sourceMds: String

    public sealed interface AlertConditionBase : AbstractAlert {
        public val alertConditionDescriptor: AlertConditionDescriptor
        public val alertConditionState: AlertConditionState
    }

    public sealed interface StringMetricBase : AbstractMetric {
        public val stringMetricDescriptor: StringMetricDescriptor
        public val stringMetricState: StringMetricState
    }

    public sealed interface AbstractEntity : MdibEntity {
        public val abstractDescriptor: AbstractDescriptor
        public val abstractState: AbstractState
    }

    public sealed interface AbstractMultiStateEntity : MdibEntity {
        public val abstractDescriptor: AbstractDescriptor
        public val abstractStates: List<AbstractState>
        public val abstractMultiStates: List<AbstractMultiState>
    }

    public sealed interface AbstractMetric : AbstractEntity {
        public val abstractMetricDescriptor: AbstractMetricDescriptor
        public val abstractMetricState: AbstractMetricState

        override val abstractDescriptor: AbstractDescriptor
            get() = abstractMetricDescriptor.abstractDescriptor
        override val abstractState: AbstractState
            get() = abstractMetricState.abstractState
    }

    public sealed interface AbstractAlert : AbstractEntity {
        public val abstractAlertDescriptor: AbstractAlertDescriptor
        public val abstractAlertState: AbstractAlertState

        override val abstractDescriptor: AbstractDescriptor
            get() = abstractAlertDescriptor.abstractDescriptor
        override val abstractState: AbstractState
            get() = abstractAlertState.abstractState
    }

    public sealed interface AbstractComplexDeviceComponent : AbstractDeviceComponent {
        public val abstractComplexDeviceComponentDescriptor: AbstractComplexDeviceComponentDescriptor
        public val abstractComplexDeviceComponentState: AbstractComplexDeviceComponentState

        override val abstractDeviceComponentDescriptor: AbstractDeviceComponentDescriptor
            get() = abstractComplexDeviceComponentDescriptor.abstractDeviceComponentDescriptor
        override val abstractDeviceComponentState: AbstractDeviceComponentState
            get() = abstractComplexDeviceComponentState.abstractDeviceComponentState
    }

    public sealed interface AbstractDeviceComponent : AbstractEntity {
        public val abstractDeviceComponentDescriptor: AbstractDeviceComponentDescriptor
        public val abstractDeviceComponentState: AbstractDeviceComponentState

        override val abstractDescriptor: AbstractDescriptor
            get() = abstractDeviceComponentDescriptor.abstractDescriptor
        override val abstractState: AbstractState
            get() = abstractDeviceComponentState.abstractState
    }

    public sealed interface AbstractOperation : AbstractEntity {
        public val abstractOperationDescriptor: AbstractOperationDescriptor
        public val abstractOperationState: AbstractOperationState

        override val abstractDescriptor: AbstractDescriptor
            get() = abstractOperationDescriptor.abstractDescriptor
        override val abstractState: AbstractState
            get() = abstractOperationState.abstractState
    }

    public sealed interface AbstractSetStateOperation : AbstractOperation {
        public val abstractSetStateOperationDescriptor: AbstractSetStateOperationDescriptor

        override val abstractOperationDescriptor: AbstractOperationDescriptor
            get() = abstractSetStateOperationDescriptor.abstractOperationDescriptor
    }

    public sealed interface AbstractContext : AbstractMultiStateEntity {
        public val abstractContextDescriptor: AbstractContextDescriptor
        public val abstractContextStates: List<AbstractContextState>

        override val abstractDescriptor: AbstractDescriptor
            get() = abstractContextDescriptor.abstractDescriptor
        override val abstractMultiStates: List<AbstractMultiState>
            get() = abstractContextStates.map { it.abstractMultiState }
        override val abstractStates: List<AbstractState>
            get() = abstractContextStates.map { it.abstractMultiState.abstractState }
    }

    // Metrics

    public data class StringMetric(
        override val lastChanged: MdibVersion,
        val descriptor: StringMetricDescriptor,
        val state: StringMetricState,
        val parent: String,
        override val sourceMds: String
    ) : StringMetricBase {
        override val stringMetricDescriptor: StringMetricDescriptor
            get() = descriptor
        override val stringMetricState: StringMetricState
            get() = state
        override val abstractMetricDescriptor: AbstractMetricDescriptor
            get() = descriptor.abstractMetricDescriptor
        override val abstractMetricState: AbstractMetricState
            get() = state.abstractMetricState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceStringMetricDescriptor(descriptor)
    }

    public data class EnumStringMetric(
        override val lastChanged: MdibVersion,
        val descriptor: EnumStringMetricDescriptor,
        val state: EnumStringMetricState,
        val parent: String,
        override val sourceMds: String
    ) : StringMetricBase {
        override val stringMetricDescriptor: StringMetricDescriptor
            get() = descriptor.stringMetricDescriptor
        override val stringMetricState: StringMetricState
            get() = state.stringMetricState
        override val abstractMetricDescriptor: AbstractMetricDescriptor
            get() = descriptor.stringMetricDescriptor.abstractMetricDescriptor
        override val abstractMetricState: AbstractMetricState
            get() = state.stringMetricState.abstractMetricState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceEnumStringMetricDescriptor(descriptor)
    }

    public data class NumericMetric(
        override val lastChanged: MdibVersion,
        val descriptor: NumericMetricDescriptor,
        val state: NumericMetricState,
        val parent: String,
        override val sourceMds: String
    ) : AbstractMetric {
        override val abstractMetricDescriptor: AbstractMetricDescriptor
            get() = descriptor.abstractMetricDescriptor
        override val abstractMetricState: AbstractMetricState
            get() = state.abstractMetricState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceNumericMetricDescriptor(descriptor)
    }

    public data class RealTimeSampleArrayMetric(
        override val lastChanged: MdibVersion,
        val descriptor: RealTimeSampleArrayMetricDescriptor,
        val state: RealTimeSampleArrayMetricState,
        val parent: String,
        override val sourceMds: String
    ) : AbstractMetric {
        override val abstractMetricDescriptor: AbstractMetricDescriptor
            get() = descriptor.abstractMetricDescriptor
        override val abstractMetricState: AbstractMetricState
            get() = state.abstractMetricState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceRealTimeSampleArrayMetricDescriptor(descriptor)
    }

    public data class DistributionSampleArrayMetric(
        override val lastChanged: MdibVersion,
        val descriptor: DistributionSampleArrayMetricDescriptor,
        val state: DistributionSampleArrayMetricState,
        val parent: String,
        override val sourceMds: String
    ) : AbstractMetric {
        override val abstractMetricDescriptor: AbstractMetricDescriptor
            get() = descriptor.abstractMetricDescriptor
        override val abstractMetricState: AbstractMetricState
            get() = state.abstractMetricState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceDistributionSampleArrayMetricDescriptor(descriptor)
    }

    // Alerts

    public data class AlertSystem(
        override val lastChanged: MdibVersion,
        val descriptor: AlertSystemDescriptor,
        val state: AlertSystemState,
        val parent: String,
        override val sourceMds: String,
        val children: List<String>
    ) : AbstractAlert {
        override val abstractAlertDescriptor: AbstractAlertDescriptor
            get() = descriptor.abstractAlertDescriptor
        override val abstractAlertState: AbstractAlertState
            get() = state.abstractAlertState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceAlertSystemDescriptor(descriptor)
    }

    public data class AlertCondition(
        override val lastChanged: MdibVersion,
        val descriptor: AlertConditionDescriptor,
        val state: AlertConditionState,
        val parent: String,
        override val sourceMds: String
    ) : AlertConditionBase {
        override val alertConditionDescriptor: AlertConditionDescriptor
            get() = descriptor
        override val alertConditionState: AlertConditionState
            get() = state
        override val abstractAlertDescriptor: AbstractAlertDescriptor
            get() = descriptor.abstractAlertDescriptor
        override val abstractAlertState: AbstractAlertState
            get() = state.abstractAlertState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceAlertConditionDescriptor(descriptor)
    }

    public data class LimitAlertCondition(
        override val lastChanged: MdibVersion,
        val descriptor: LimitAlertConditionDescriptor,
        val state: LimitAlertConditionState,
        val parent: String,
        override val sourceMds: String
    ) : AlertConditionBase {
        override val alertConditionDescriptor: AlertConditionDescriptor
            get() = descriptor.alertConditionDescriptor
        override val alertConditionState: AlertConditionState
            get() = state.alertConditionState
        override val abstractAlertDescriptor: AbstractAlertDescriptor
            get() = descriptor.alertConditionDescriptor.abstractAlertDescriptor
        override val abstractAlertState: AbstractAlertState
            get() = state.alertConditionState.abstractAlertState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceLimitAlertConditionDescriptor(descriptor)
    }

    public data class AlertSignal(
        override val lastChanged: MdibVersion,
        val descriptor: AlertSignalDescriptor,
        val state: AlertSignalState,
        val parent: String,
        override val sourceMds: String
    ) : AbstractAlert {
        override val abstractAlertDescriptor: AbstractAlertDescriptor
            get() = descriptor.abstractAlertDescriptor
        override val abstractAlertState: AbstractAlertState
            get() = state.abstractAlertState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceAlertSignalDescriptor(descriptor)
    }

    // Components

    public data class Mds(
        override val lastChanged: MdibVersion,
        val descriptor: MdsDescriptor,
        val state: MdsState,
        val children: List<String>,
        override val sourceMds: String = descriptor.handleString()
    ) : AbstractComplexDeviceComponent {
        override val abstractComplexDeviceComponentDescriptor: AbstractComplexDeviceComponentDescriptor
            get() = descriptor.abstractComplexDeviceComponentDescriptor
        override val abstractComplexDeviceComponentState: AbstractComplexDeviceComponentState
            get() = state.abstractComplexDeviceComponentState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceMdsDescriptor(descriptor)
    }

    public data class Vmd(
        override val lastChanged: MdibVersion,
        val descriptor: VmdDescriptor,
        val state: VmdState,
        val parent: String,
        override val sourceMds: String,
        val children: List<String>
    ) : AbstractComplexDeviceComponent {
        override val abstractComplexDeviceComponentDescriptor: AbstractComplexDeviceComponentDescriptor
            get() = descriptor.abstractComplexDeviceComponentDescriptor
        override val abstractComplexDeviceComponentState: AbstractComplexDeviceComponentState
            get() = state.abstractComplexDeviceComponentState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceVmdDescriptor(descriptor)
    }

    public data class Channel(
        override val lastChanged: MdibVersion,
        val descriptor: ChannelDescriptor,
        val state: ChannelState,
        val parent: String,
        override val sourceMds: String,
        val children: List<String>
    ) : AbstractDeviceComponent {
        override val abstractDeviceComponentDescriptor: AbstractDeviceComponentDescriptor
            get() = descriptor.abstractDeviceComponentDescriptor
        override val abstractDeviceComponentState: AbstractDeviceComponentState
            get() = state.abstractDeviceComponentState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceChannelDescriptor(descriptor)
    }

    public data class Sco(
        override val lastChanged: MdibVersion,
        val descriptor: ScoDescriptor,
        val state: ScoState,
        val parent: String,
        override val sourceMds: String,
        val children: List<String>
    ) : AbstractDeviceComponent {
        override val abstractDeviceComponentDescriptor: AbstractDeviceComponentDescriptor
            get() = descriptor.abstractDeviceComponentDescriptor
        override val abstractDeviceComponentState: AbstractDeviceComponentState
            get() = state.abstractDeviceComponentState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceScoDescriptor(descriptor)
    }

    public data class Battery(
        override val lastChanged: MdibVersion,
        val descriptor: BatteryDescriptor,
        val state: BatteryState,
        val parent: String,
        override val sourceMds: String
    ) : AbstractDeviceComponent {
        override val abstractDeviceComponentDescriptor: AbstractDeviceComponentDescriptor
            get() = descriptor.abstractDeviceComponentDescriptor
        override val abstractDeviceComponentState: AbstractDeviceComponentState
            get() = state.abstractDeviceComponentState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceBatteryDescriptor(descriptor)
    }

    public data class Clock(
        override val lastChanged: MdibVersion,
        val descriptor: ClockDescriptor,
        val state: ClockState,
        val parent: String,
        override val sourceMds: String
    ) : AbstractDeviceComponent {
        override val abstractDeviceComponentDescriptor: AbstractDeviceComponentDescriptor
            get() = descriptor.abstractDeviceComponentDescriptor
        override val abstractDeviceComponentState: AbstractDeviceComponentState
            get() = state.abstractDeviceComponentState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceClockDescriptor(descriptor)
    }

    public data class SystemContext(
        override val lastChanged: MdibVersion,
        val descriptor: SystemContextDescriptor,
        val state: SystemContextState,
        val parent: String,
        override val sourceMds: String,
        val children: List<String>
    ) : AbstractDeviceComponent {
        override val abstractDeviceComponentDescriptor: AbstractDeviceComponentDescriptor
            get() = descriptor.abstractDeviceComponentDescriptor
        override val abstractDeviceComponentState: AbstractDeviceComponentState
            get() = state.abstractDeviceComponentState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceSystemContextDescriptor(descriptor)
    }

    // Operations

    public data class SetValueOperation(
        override val lastChanged: MdibVersion,
        val descriptor: SetValueOperationDescriptor,
        val state: SetValueOperationState,
        val parent: String,
        override val sourceMds: String
    ) : AbstractOperation {
        override val abstractOperationDescriptor: AbstractOperationDescriptor
            get() = descriptor.abstractOperationDescriptor
        override val abstractOperationState: AbstractOperationState
            get() = state.abstractOperationState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceSetValueOperationDescriptor(descriptor)
    }

    public data class SetStringOperation(
        override val lastChanged: MdibVersion,
        val descriptor: SetStringOperationDescriptor,
        val state: SetStringOperationState,
        val parent: String,
        override val sourceMds: String
    ) : AbstractOperation {
        override val abstractOperationDescriptor: AbstractOperationDescriptor
            get() = descriptor.abstractOperationDescriptor
        override val abstractOperationState: AbstractOperationState
            get() = state.abstractOperationState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceSetStringOperationDescriptor(descriptor)
    }

    public data class SetAlertStateOperation(
        override val lastChanged: MdibVersion,
        val descriptor: SetAlertStateOperationDescriptor,
        val state: SetAlertStateOperationState,
        val parent: String,
        override val sourceMds: String
    ) : AbstractSetStateOperation {
        override val abstractOperationState: AbstractOperationState
            get() = state.abstractOperationState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceSetAlertStateOperationDescriptor(descriptor)
        override val abstractSetStateOperationDescriptor: AbstractSetStateOperationDescriptor
            get() = descriptor.abstractSetStateOperationDescriptor
    }

    public data class SetMetricStateOperation(
        override val lastChanged: MdibVersion,
        val descriptor: SetMetricStateOperationDescriptor,
        val state: SetMetricStateOperationState,
        val parent: String,
        override val sourceMds: String
    ) : AbstractSetStateOperation {
        override val abstractOperationState: AbstractOperationState
            get() = state.abstractOperationState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceSetMetricStateOperationDescriptor(descriptor)
        override val abstractSetStateOperationDescriptor: AbstractSetStateOperationDescriptor
            get() = descriptor.abstractSetStateOperationDescriptor
    }

    public data class SetComponentStateOperation(
        override val lastChanged: MdibVersion,
        val descriptor: SetComponentStateOperationDescriptor,
        val state: SetComponentStateOperationState,
        val parent: String,
        override val sourceMds: String
    ) : AbstractSetStateOperation {
        override val abstractOperationState: AbstractOperationState
            get() = state.abstractOperationState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceSetComponentStateOperationDescriptor(descriptor)
        override val abstractSetStateOperationDescriptor: AbstractSetStateOperationDescriptor
            get() = descriptor.abstractSetStateOperationDescriptor
    }

    public data class SetContextStateOperation(
        override val lastChanged: MdibVersion,
        val descriptor: SetContextStateOperationDescriptor,
        val state: SetContextStateOperationState,
        val parent: String,
        override val sourceMds: String
    ) : AbstractSetStateOperation {
        override val abstractOperationState: AbstractOperationState
            get() = state.abstractOperationState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceSetContextStateOperationDescriptor(descriptor)
        override val abstractSetStateOperationDescriptor: AbstractSetStateOperationDescriptor
            get() = descriptor.abstractSetStateOperationDescriptor
    }

    public data class ActivateOperation(
        override val lastChanged: MdibVersion,
        val descriptor: ActivateOperationDescriptor,
        val state: ActivateOperationState,
        val parent: String,
        override val sourceMds: String
    ) : AbstractSetStateOperation {
        override val abstractOperationState: AbstractOperationState
            get() = state.abstractOperationState
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceActivateOperationDescriptor(descriptor)
        override val abstractSetStateOperationDescriptor: AbstractSetStateOperationDescriptor
            get() = descriptor.abstractSetStateOperationDescriptor
    }

    // Contexts

    public data class PatientContext(
        override val lastChanged: MdibVersion,
        val descriptor: PatientContextDescriptor,
        val states: List<PatientContextState>,
        val parent: String,
        override val sourceMds: String
    ) : AbstractContext {
        override val abstractContextDescriptor: AbstractContextDescriptor
            get() = descriptor.abstractContextDescriptor
        override val abstractContextStates: List<AbstractContextState>
            get() = states.map { it.abstractContextState }
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoicePatientContextDescriptor(descriptor)
    }

    public data class LocationContext(
        override val lastChanged: MdibVersion,
        val descriptor: LocationContextDescriptor,
        val states: List<LocationContextState>,
        val parent: String,
        override val sourceMds: String
    ) : AbstractContext {
        override val abstractContextDescriptor: AbstractContextDescriptor
            get() = descriptor.abstractContextDescriptor
        override val abstractContextStates: List<AbstractContextState>
            get() = states.map { it.abstractContextState }
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceLocationContextDescriptor(descriptor)
    }

    public data class EnsembleContext(
        override val lastChanged: MdibVersion,
        val descriptor: EnsembleContextDescriptor,
        val states: List<EnsembleContextState>,
        val parent: String,
        override val sourceMds: String
    ) : AbstractContext {
        override val abstractContextDescriptor: AbstractContextDescriptor
            get() = descriptor.abstractContextDescriptor
        override val abstractContextStates: List<AbstractContextState>
            get() = states.map { it.abstractContextState }
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceEnsembleContextDescriptor(descriptor)
    }

    public data class WorkflowContext(
        override val lastChanged: MdibVersion,
        val descriptor: WorkflowContextDescriptor,
        val states: List<WorkflowContextState>,
        val parent: String,
        override val sourceMds: String
    ) : AbstractContext {
        override val abstractContextDescriptor: AbstractContextDescriptor
            get() = descriptor.abstractContextDescriptor
        override val abstractContextStates: List<AbstractContextState>
            get() = states.map { it.abstractContextState }
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceWorkflowContextDescriptor(descriptor)
    }

    public data class MeansContext(
        override val lastChanged: MdibVersion,
        val descriptor: MeansContextDescriptor,
        val states: List<MeansContextState>,
        val parent: String,
        override val sourceMds: String
    ) : AbstractContext {
        override val abstractContextDescriptor: AbstractContextDescriptor
            get() = descriptor.abstractContextDescriptor
        override val abstractContextStates: List<AbstractContextState>
            get() = states.map { it.abstractContextState }
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceMeansContextDescriptor(descriptor)
    }

    public data class OperatorContext(
        override val lastChanged: MdibVersion,
        val descriptor: OperatorContextDescriptor,
        val states: List<OperatorContextState>,
        val parent: String,
        override val sourceMds: String
    ) : AbstractContext {
        override val abstractContextDescriptor: AbstractContextDescriptor
            get() = descriptor.abstractContextDescriptor
        override val abstractContextStates: List<AbstractContextState>
            get() = states.map { it.abstractContextState }
        override val descriptorOneOf: AbstractDescriptorOneOf
            get() = AbstractDescriptorOneOf.ChoiceOperatorContextDescriptor(descriptor)
    }

    /**
     * Gets parent handle of this entity.
     *
     * @return the parent handle or null if there is none (only true for [Mds]).
     */
    public fun resolveParent(): String? = when (this) {
        is ActivateOperation -> this.parent
        is AlertCondition -> this.parent
        is AlertSignal -> this.parent
        is AlertSystem -> this.parent
        is Battery -> this.parent
        is Channel -> this.parent
        is Clock -> this.parent
        is DistributionSampleArrayMetric -> this.parent
        is EnsembleContext -> this.parent
        is EnumStringMetric -> this.parent
        is LimitAlertCondition -> this.parent
        is LocationContext -> this.parent
        is Mds -> null
        is MeansContext -> this.parent
        is NumericMetric -> this.parent
        is OperatorContext -> this.parent
        is PatientContext -> this.parent
        is RealTimeSampleArrayMetric -> this.parent
        is Sco -> this.parent
        is SetAlertStateOperation -> this.parent
        is SetComponentStateOperation -> this.parent
        is SetContextStateOperation -> this.parent
        is SetMetricStateOperation -> this.parent
        is SetStringOperation -> this.parent
        is SetValueOperation -> this.parent
        is StringMetric -> this.parent
        is SystemContext -> this.parent
        is Vmd -> this.parent
        is WorkflowContext -> this.parent
    }

    /**
     * Gets all child handles of this entity.
     *
     * @return child handles. The list is empty if there is no child or if there is no children supported in general.
     */
    public fun resolveChildren(): List<String> = when (this) {
        is AlertSystem -> this.children
        is Mds -> this.children
        is Sco -> this.children
        is SystemContext -> this.children
        is Vmd -> this.children
        is Channel -> this.children
        else -> listOf()
    }

    /**
     * Gets the context states of this entity.
     *
     * @return the list of context states. The list is empty if there is no context state or if this object is not
     *         a context entity.
     */
    public fun resolveContextStates(): List<AbstractContextStateOneOf> = when (this) {
        is EnsembleContext -> this.states.map { AbstractContextStateOneOf.ChoiceEnsembleContextState(it) }
        is MeansContext -> this.states.map { AbstractContextStateOneOf.ChoiceMeansContextState(it) }
        is LocationContext -> this.states.map { AbstractContextStateOneOf.ChoiceLocationContextState(it) }
        is OperatorContext -> this.states.map { AbstractContextStateOneOf.ChoiceOperatorContextState(it) }
        is PatientContext -> this.states.map { AbstractContextStateOneOf.ChoicePatientContextState(it) }
        is WorkflowContext -> this.states.map { AbstractContextStateOneOf.ChoiceWorkflowContextState(it) }
        else -> listOf()
    }

    /**
     * Checks if this entity is a context entity.
     *
     * @return true if the entity is a context entity, i.e. contains an [EnsembleContext], [MeansContext],
     *         [LocationContext], [OperatorContext], [PatientContext] or [WorkflowContext], otherwise false.
     */
    public fun isContext(): Boolean = when (this) {
        is EnsembleContext,
        is MeansContext,
        is LocationContext,
        is OperatorContext,
        is PatientContext,
        is WorkflowContext -> true

        else -> false
    }

    /**
     * Gets the state of this entity.
     *
     * @return the state if and only if this is not a context entity, otherwise null.
     */
    public fun resolveSingleState(): AbstractStateOneOf? = when (this) {
        is ActivateOperation -> AbstractStateOneOf.ChoiceActivateOperationState(this.state)
        is AlertCondition -> AbstractStateOneOf.ChoiceAlertConditionState(this.state)
        is AlertSignal -> AbstractStateOneOf.ChoiceAlertSignalState(this.state)
        is AlertSystem -> AbstractStateOneOf.ChoiceAlertSystemState(this.state)
        is Battery -> AbstractStateOneOf.ChoiceBatteryState(this.state)
        is Channel -> AbstractStateOneOf.ChoiceChannelState(this.state)
        is Clock -> AbstractStateOneOf.ChoiceClockState(this.state)
        is DistributionSampleArrayMetric -> AbstractStateOneOf.ChoiceDistributionSampleArrayMetricState(this.state)
        is EnumStringMetric -> AbstractStateOneOf.ChoiceEnumStringMetricState(this.state)
        is LimitAlertCondition -> AbstractStateOneOf.ChoiceLimitAlertConditionState(this.state)
        is Mds -> AbstractStateOneOf.ChoiceMdsState(this.state)
        is NumericMetric -> AbstractStateOneOf.ChoiceNumericMetricState(this.state)
        is RealTimeSampleArrayMetric -> AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState(this.state)
        is Sco -> AbstractStateOneOf.ChoiceScoState(this.state)
        is SetAlertStateOperation -> AbstractStateOneOf.ChoiceSetAlertStateOperationState(this.state)
        is SetComponentStateOperation -> AbstractStateOneOf.ChoiceSetComponentStateOperationState(this.state)
        is SetContextStateOperation -> AbstractStateOneOf.ChoiceSetContextStateOperationState(this.state)
        is SetMetricStateOperation -> AbstractStateOneOf.ChoiceSetMetricStateOperationState(this.state)
        is SetStringOperation -> AbstractStateOneOf.ChoiceSetStringOperationState(this.state)
        is SetValueOperation -> AbstractStateOneOf.ChoiceSetValueOperationState(this.state)
        is StringMetric -> AbstractStateOneOf.ChoiceStringMetricState(this.state)
        is SystemContext -> AbstractStateOneOf.ChoiceSystemContextState(this.state)
        is Vmd -> AbstractStateOneOf.ChoiceVmdState(this.state)
        else -> null
    }

    /**
     * Gets the states of this entity.
     *
     * @return a list of states that reflect the context states if this entity is a context entity, or otherwise a list
     *         with exactly one entry.
     */
    public fun resolveStates(): List<AbstractStateOneOf> = when (this) {
        is ActivateOperation -> listOf(AbstractStateOneOf.ChoiceActivateOperationState(this.state))
        is AlertCondition -> listOf(AbstractStateOneOf.ChoiceAlertConditionState(this.state))
        is AlertSignal -> listOf(AbstractStateOneOf.ChoiceAlertSignalState(this.state))
        is AlertSystem -> listOf(AbstractStateOneOf.ChoiceAlertSystemState(this.state))
        is Battery -> listOf(AbstractStateOneOf.ChoiceBatteryState(this.state))
        is Channel -> listOf(AbstractStateOneOf.ChoiceChannelState(this.state))
        is Clock -> listOf(AbstractStateOneOf.ChoiceClockState(this.state))
        is DistributionSampleArrayMetric -> listOf(AbstractStateOneOf.ChoiceDistributionSampleArrayMetricState(this.state))
        is EnumStringMetric -> listOf(AbstractStateOneOf.ChoiceEnumStringMetricState(this.state))
        is LimitAlertCondition -> listOf(AbstractStateOneOf.ChoiceLimitAlertConditionState(this.state))
        is Mds -> listOf(AbstractStateOneOf.ChoiceMdsState(this.state))
        is NumericMetric -> listOf(AbstractStateOneOf.ChoiceNumericMetricState(this.state))
        is RealTimeSampleArrayMetric -> listOf(AbstractStateOneOf.ChoiceRealTimeSampleArrayMetricState(this.state))
        is Sco -> listOf(AbstractStateOneOf.ChoiceScoState(this.state))
        is SetAlertStateOperation -> listOf(AbstractStateOneOf.ChoiceSetAlertStateOperationState(this.state))
        is SetComponentStateOperation -> listOf(AbstractStateOneOf.ChoiceSetComponentStateOperationState(this.state))
        is SetContextStateOperation -> listOf(AbstractStateOneOf.ChoiceSetContextStateOperationState(this.state))
        is SetMetricStateOperation -> listOf(AbstractStateOneOf.ChoiceSetMetricStateOperationState(this.state))
        is SetStringOperation -> listOf(AbstractStateOneOf.ChoiceSetStringOperationState(this.state))
        is SetValueOperation -> listOf(AbstractStateOneOf.ChoiceSetValueOperationState(this.state))
        is StringMetric -> listOf(AbstractStateOneOf.ChoiceStringMetricState(this.state))
        is SystemContext -> listOf(AbstractStateOneOf.ChoiceSystemContextState(this.state))
        is Vmd -> listOf(AbstractStateOneOf.ChoiceVmdState(this.state))
        is EnsembleContext -> this.states.map { AbstractStateOneOf.ChoiceEnsembleContextState(it) }
        is MeansContext -> this.states.map { AbstractStateOneOf.ChoiceMeansContextState(it) }
        is LocationContext -> this.states.map { AbstractStateOneOf.ChoiceLocationContextState(it) }
        is OperatorContext -> this.states.map { AbstractStateOneOf.ChoiceOperatorContextState(it) }
        is PatientContext -> this.states.map { AbstractStateOneOf.ChoicePatientContextState(it) }
        is WorkflowContext -> this.states.map { AbstractStateOneOf.ChoiceWorkflowContextState(it) }
    }
}
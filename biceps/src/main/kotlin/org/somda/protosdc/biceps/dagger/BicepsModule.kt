package org.somda.protosdc.biceps.dagger

import dagger.Module
import dagger.Provides
import org.somda.protosdc.biceps.BicepsConfig
import org.somda.protosdc.biceps.common.MdibEntityFactory
import org.somda.protosdc.biceps.common.MdibVersion
import org.somda.protosdc.biceps.common.storage.DefaultInstantiation
import org.somda.protosdc.biceps.consumer.access.RemoteMdibAccess
import org.somda.protosdc.biceps.consumer.access.RemoteMdibAccessImpl
import org.somda.protosdc.biceps.provider.access.LocalMdibAccess
import org.somda.protosdc.biceps.provider.access.LocalMdibAccessImpl
import org.somda.protosdc.common.ComponentConfig
import org.somda.protosdc.common.InstanceId
import javax.annotation.Nullable
import javax.inject.Singleton

@Module
internal abstract class BicepsModule {
    companion object {

        @Singleton
        @Provides
        fun instanceId(@ComponentConfig instanceId: InstanceId) = instanceId
        
        @Singleton
        @Provides
        fun mdibStorageUtil() = DefaultInstantiation()

        @Singleton
        @Provides
        fun mdibEntityFactory() = MdibEntityFactory()

        @Singleton
        @Provides
        fun localMdibAccessFactory(implFactory: LocalMdibAccessImpl.Factory) = object : LocalMdibAccess.Factory {
            override fun create() = implFactory.create()
            override fun create(initialMdibVersion: MdibVersion) = implFactory.create(initialMdibVersion)
        }

        @Singleton
        @Provides
        fun remoteMdibAccessFactory(implFactory: RemoteMdibAccessImpl.Factory) = object : RemoteMdibAccess.Factory {
            override fun create(initialMdibVersion: MdibVersion) = implFactory.create(initialMdibVersion)
        }

        @Singleton
        @Provides
        fun bicepsConfig(@ComponentConfig @Nullable bicepsConfig: BicepsConfig?) = bicepsConfig ?: BicepsConfig()
    }
}
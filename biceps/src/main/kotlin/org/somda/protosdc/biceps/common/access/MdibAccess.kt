package org.somda.protosdc.biceps.common.access

import org.somda.protosdc.biceps.common.MdibEntity
import org.somda.protosdc.biceps.common.MdibVersion
import org.somda.protosdc.biceps.common.storage.NotFoundException
import org.somda.protosdc.model.biceps.AbstractContextStateOneOf
import org.somda.protosdc.model.biceps.Handle
import kotlin.reflect.KClass

/**
 * Read access to an MDIB storage.
 */
public interface MdibAccess {

    /**
     * The latest known MDIB version.
     *
     * @return the latest known MDIB version which in case of remote access may not necessarily reflect the MDIB version
     *         of the whole MDIB (e.g. if only a subset of all available reports is subscribed). In case of local access
     *         the returned version corresponds to the latest state.
     */
    public suspend fun mdibVersion(): MdibVersion

    /**
     * The latest known medical device description version.
     *
     * @return the latest known medical device description version which in case of remote access may be outdated. In
     *         case of local access the returned version corresponds to the latest description.
     */
    public suspend fun mdDescriptionVersion(): Long

    /**
     * The latest known medical device state version.
     *
     * @return the latest known medical device state version which in case of remote access may be outdated. In
     *         case of local access the returned version corresponds to the latest state.
     */
    public suspend fun mdStateVersion(): Long

    /**
     * Finds an entity for the given handle.
     *
     * @param handle the descriptor handle to seek.
     * @return a result representing the found [MdibEntity], [NotFoundException] if the entity was not found
     * or another exception expressing a failure otherwise.
     */
    public suspend fun anyEntity(handle: String): Result<MdibEntity>

    /**
     * Finds an entity for the given handle of the given type.
     *
     * @param handle the descriptor handle to seek.
     * @param type type of the entity to seek.
     * @return a result representing the found [MdibEntity], [NotFoundException] if the entity was not found
     * or another exception expressing a failure otherwise.
     */
    public suspend fun <T : MdibEntity> entity(handle: String, type: KClass<out T>): Result<T>

    /**
     * Finds an entity for the given handle.
     *
     * @param handle the descriptor handle to seek.
     * @return a result representing the found [MdibEntity], [NotFoundException] if the entity was not found
     * or another exception expressing a failure otherwise.
     */
    public suspend fun anyEntity(handle: Handle): Result<MdibEntity>

    /**
     * Finds an entity for the given handle of the given type.
     *
     * @param handle the descriptor handle to seek.
     * @param type type of the entity to seek.
     * @return a result representing the found [MdibEntity], [NotFoundException] if the entity was not found
     * or another exception expressing a failure otherwise.
     */
    public suspend fun <T : MdibEntity> entity(handle: Handle, type: KClass<out T>): Result<T>

    /**
     * Gets a copy of all root entities.
     *
     * @return a list of all available [MdibEntity.Mds] instances.
     */
    public suspend fun rootEntities(): List<MdibEntity.Mds>

    /**
     * Finds a context state for a given handle.
     *
     * @param handle the state handle to seek.
     * @return the corresponding context state, [NotFoundException] if the entity was not found
     * or another exception expressing a failure otherwise.
     */
    public suspend fun abstractContextState(handle: String): Result<AbstractContextStateOneOf>

    /**
     * Finds a context state for a given handle of a given type.
     *
     * @param handle the state handle to seek.
     * @param type type of the context state to seek
     * @return the corresponding context state, [NotFoundException] if the entity was not found
     * or another exception expressing a failure otherwise.
     */
    public suspend fun <T : AbstractContextStateOneOf> contextState(handle: String, type: KClass<out T>): Result<T>

    /**
     * Finds a context state for a given handle.
     *
     * @param handle the state handle to seek.
     * @return the corresponding context state as a result, [NotFoundException] if the entity was not found
     * or another exception expressing a failure otherwise.
     */
    public suspend fun abstractContextState(handle: Handle): Result<AbstractContextStateOneOf>

    /**
     * Finds a context state for a given handle of a given type.
     *
     * @param handle the state handle to seek.
     * @param type type of the context state to seek
     * @return the corresponding context state as a result, [NotFoundException] if the entity was not found
     * or another exception expressing a failure otherwise.
     */
    public suspend fun <T : AbstractContextStateOneOf> contextState(handle: Handle, type: KClass<out T>): Result<T>

    /**
     * Gets a copy of all context states.
     *
     * @return a collection of all available context states.
     */
    public suspend fun contextStates(): Collection<AbstractContextStateOneOf>

    /**
     * Filters entities by type.
     *
     * @param type the type to filter for.
     * @return a collection that contains all entities of the given *type*.
     */
    public suspend fun <T: MdibEntity> entitiesByType(type: KClass<out T>): Collection<T>

    /**
     * Filters child entities by type.
     *
     * @param handle the parent handle for which children are filtered.
     * @param type the type to filter for.
     * @return a list that contains all children for *handle* of the given *type*.
     */
    public suspend fun <T: MdibEntity> childrenByType(handle: String, type: KClass<out T>): List<T>

    /**
     * Filters child entities by type.
     *
     * @param handle the parent handle for which children are filtered.
     * @param type the type to filter for.
     * @return a list that contains all children for *handle* of the given *type*.
     */
    public suspend fun <T: MdibEntity> childrenByType(handle: Handle, type: KClass<out T>): List<T>
}

public suspend inline fun <reified T: MdibEntity> MdibAccess.childrenByType(handle: String): List<T> {
    return this.childrenByType(handle, T::class)
}

public suspend inline fun <reified T: MdibEntity> MdibAccess.childrenByType(handle: Handle): List<T> {
    return this.childrenByType(handle, T::class)
}

public suspend inline fun <reified T: MdibEntity> MdibAccess.entitiesByType(): Collection<T> {
    return this.entitiesByType(T::class)
}

public suspend inline fun <reified T : AbstractContextStateOneOf> MdibAccess.contextState(handle: String): Result<T> {
    return this.contextState(handle, T::class)
}

public suspend inline fun <reified T : AbstractContextStateOneOf> MdibAccess.contextState(handle: Handle): Result<T> {
    return this.contextState(handle, T::class)
}

public suspend inline fun <reified T : MdibEntity> MdibAccess.entity(handle: String): Result<T> {
    return this.entity(handle, T::class)
}

public suspend inline fun <reified T : MdibEntity> MdibAccess.entity(handle: Handle): Result<T> {
    return this.entity(handle, T::class)
}
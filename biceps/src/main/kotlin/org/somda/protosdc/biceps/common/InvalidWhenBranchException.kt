package org.somda.protosdc.biceps.common

/**
 * Thrown when abstract descriptors or states are instantiated and end up in a when-branch.
 *
 * @param obj the object for which instantiation should not ensue.
 */
public class InvalidWhenBranchException(obj: Any) :
    Exception("Accessed class in when-branch that is not supposed to be instantiated individually: $obj")
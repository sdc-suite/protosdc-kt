package org.somda.protosdc.biceps.common

import org.somda.protosdc.model.biceps.AbstractAlertDescriptor
import org.somda.protosdc.model.biceps.AbstractAlertState
import org.somda.protosdc.model.biceps.AbstractComplexDeviceComponentDescriptor
import org.somda.protosdc.model.biceps.AbstractComplexDeviceComponentState
import org.somda.protosdc.model.biceps.AbstractContextDescriptor
import org.somda.protosdc.model.biceps.AbstractContextState
import org.somda.protosdc.model.biceps.AbstractDescriptor
import org.somda.protosdc.model.biceps.AbstractDeviceComponentDescriptor
import org.somda.protosdc.model.biceps.AbstractDeviceComponentState
import org.somda.protosdc.model.biceps.AbstractMetricDescriptor
import org.somda.protosdc.model.biceps.AbstractMetricState
import org.somda.protosdc.model.biceps.AbstractMetricValue
import org.somda.protosdc.model.biceps.AbstractMultiState
import org.somda.protosdc.model.biceps.AbstractOperationDescriptor
import org.somda.protosdc.model.biceps.AbstractOperationState
import org.somda.protosdc.model.biceps.AbstractSetStateOperationDescriptor
import org.somda.protosdc.model.biceps.AbstractState
import org.somda.protosdc.model.biceps.ActivateOperationDescriptor
import org.somda.protosdc.model.biceps.ActivateOperationState
import org.somda.protosdc.model.biceps.AlertActivation
import org.somda.protosdc.model.biceps.AlertConditionDescriptor
import org.somda.protosdc.model.biceps.AlertConditionKind
import org.somda.protosdc.model.biceps.AlertConditionMonitoredLimits
import org.somda.protosdc.model.biceps.AlertConditionPriority
import org.somda.protosdc.model.biceps.AlertConditionReference
import org.somda.protosdc.model.biceps.AlertConditionState
import org.somda.protosdc.model.biceps.AlertSignalDescriptor
import org.somda.protosdc.model.biceps.AlertSignalManifestation
import org.somda.protosdc.model.biceps.AlertSignalPresence
import org.somda.protosdc.model.biceps.AlertSignalPrimaryLocation
import org.somda.protosdc.model.biceps.AlertSignalState
import org.somda.protosdc.model.biceps.AlertSystemDescriptor
import org.somda.protosdc.model.biceps.AlertSystemState
import org.somda.protosdc.model.biceps.ApprovedJurisdictions
import org.somda.protosdc.model.biceps.BaseDemographicsOneOf
import org.somda.protosdc.model.biceps.BatteryDescriptor
import org.somda.protosdc.model.biceps.BatteryState
import org.somda.protosdc.model.biceps.CalibrationInfo
import org.somda.protosdc.model.biceps.CauseInfo
import org.somda.protosdc.model.biceps.ChannelDescriptor
import org.somda.protosdc.model.biceps.ChannelState
import org.somda.protosdc.model.biceps.ClockDescriptor
import org.somda.protosdc.model.biceps.ClockState
import org.somda.protosdc.model.biceps.CodeIdentifier
import org.somda.protosdc.model.biceps.CodedValue
import org.somda.protosdc.model.biceps.ComponentActivation
import org.somda.protosdc.model.biceps.ContextAssociation
import org.somda.protosdc.model.biceps.DerivationMethod
import org.somda.protosdc.model.biceps.DistributionSampleArrayMetricDescriptor
import org.somda.protosdc.model.biceps.DistributionSampleArrayMetricState
import org.somda.protosdc.model.biceps.EnsembleContextDescriptor
import org.somda.protosdc.model.biceps.EnsembleContextState
import org.somda.protosdc.model.biceps.EnumStringMetricDescriptor
import org.somda.protosdc.model.biceps.EnumStringMetricState
import org.somda.protosdc.model.biceps.Extension
import org.somda.protosdc.model.biceps.GenerationMode
import org.somda.protosdc.model.biceps.Handle
import org.somda.protosdc.model.biceps.HandleRef
import org.somda.protosdc.model.biceps.InstanceIdentifier
import org.somda.protosdc.model.biceps.InstanceIdentifierOneOf
import org.somda.protosdc.model.biceps.LimitAlertConditionDescriptor
import org.somda.protosdc.model.biceps.LimitAlertConditionState
import org.somda.protosdc.model.biceps.LocationContextDescriptor
import org.somda.protosdc.model.biceps.LocationContextState
import org.somda.protosdc.model.biceps.LocationDetail
import org.somda.protosdc.model.biceps.MdsDescriptor
import org.somda.protosdc.model.biceps.MdsOperatingMode
import org.somda.protosdc.model.biceps.MdsState
import org.somda.protosdc.model.biceps.MeansContextDescriptor
import org.somda.protosdc.model.biceps.MeansContextState
import org.somda.protosdc.model.biceps.Measurement
import org.somda.protosdc.model.biceps.MeasurementValidity
import org.somda.protosdc.model.biceps.MetricAvailability
import org.somda.protosdc.model.biceps.MetricCategory
import org.somda.protosdc.model.biceps.NumericMetricDescriptor
import org.somda.protosdc.model.biceps.NumericMetricState
import org.somda.protosdc.model.biceps.NumericMetricValue
import org.somda.protosdc.model.biceps.OperatingJurisdiction
import org.somda.protosdc.model.biceps.OperatingMode
import org.somda.protosdc.model.biceps.OperationRef
import org.somda.protosdc.model.biceps.OperatorContextDescriptor
import org.somda.protosdc.model.biceps.OperatorContextState
import org.somda.protosdc.model.biceps.PatientContextDescriptor
import org.somda.protosdc.model.biceps.PatientContextState
import org.somda.protosdc.model.biceps.PatientDemographicsCoreDataOneOf
import org.somda.protosdc.model.biceps.PhysicalConnectorInfo
import org.somda.protosdc.model.biceps.QualityIndicator
import org.somda.protosdc.model.biceps.Range
import org.somda.protosdc.model.biceps.RealTimeSampleArrayMetricDescriptor
import org.somda.protosdc.model.biceps.RealTimeSampleArrayMetricState
import org.somda.protosdc.model.biceps.RealTimeValueType
import org.somda.protosdc.model.biceps.ReferencedVersion
import org.somda.protosdc.model.biceps.SafetyClassification
import org.somda.protosdc.model.biceps.SampleArrayValue
import org.somda.protosdc.model.biceps.ScoDescriptor
import org.somda.protosdc.model.biceps.ScoState
import org.somda.protosdc.model.biceps.SetAlertStateOperationDescriptor
import org.somda.protosdc.model.biceps.SetAlertStateOperationState
import org.somda.protosdc.model.biceps.SetComponentStateOperationDescriptor
import org.somda.protosdc.model.biceps.SetComponentStateOperationState
import org.somda.protosdc.model.biceps.SetContextStateOperationDescriptor
import org.somda.protosdc.model.biceps.SetContextStateOperationState
import org.somda.protosdc.model.biceps.SetMetricStateOperationDescriptor
import org.somda.protosdc.model.biceps.SetMetricStateOperationState
import org.somda.protosdc.model.biceps.SetStringOperationDescriptor
import org.somda.protosdc.model.biceps.SetStringOperationState
import org.somda.protosdc.model.biceps.SetValueOperationDescriptor
import org.somda.protosdc.model.biceps.SetValueOperationState
import org.somda.protosdc.model.biceps.StringMetricDescriptor
import org.somda.protosdc.model.biceps.StringMetricState
import org.somda.protosdc.model.biceps.StringMetricValue
import org.somda.protosdc.model.biceps.SystemContextDescriptor
import org.somda.protosdc.model.biceps.SystemContextState
import org.somda.protosdc.model.biceps.SystemSignalActivation
import org.somda.protosdc.model.biceps.TimeZone
import org.somda.protosdc.model.biceps.Timestamp
import org.somda.protosdc.model.biceps.VmdDescriptor
import org.somda.protosdc.model.biceps.VmdState
import org.somda.protosdc.model.biceps.WorkflowContextDescriptor
import org.somda.protosdc.model.biceps.WorkflowContextState
import java.math.BigDecimal
import java.time.Instant
import kotlin.time.Duration

public object MdibFactory {
    public fun timestamp(): Timestamp = Timestamp(Instant.now().toEpochMilli())

    public fun codedValue(code: String, codingSystem: String? = null, codingSystemVersion: String? = null): CodedValue =
        CodedValue(
            codingSystemAttr = codingSystem,
            codeAttr = CodeIdentifier(code),
            codingSystemVersionAttr = codingSystemVersion
        )

    public fun instanceIdentifier(
        root: String? = null,
        extension: String? = null,
        type: CodedValue? = null
    ): InstanceIdentifier =
        InstanceIdentifier(
            rootAttr = root?.let { InstanceIdentifier.RootAttr(it) },
            extensionAttr = extension?.let { InstanceIdentifier.ExtensionAttr(it) },
            type = type
        )

    public fun instanceIdentifierOneOf(
        root: String? = null,
        extension: String? = null,
        type: CodedValue? = null
    ): InstanceIdentifierOneOf.ChoiceInstanceIdentifier =
        InstanceIdentifierOneOf.ChoiceInstanceIdentifier(
            instanceIdentifier(
                root,
                extension,
                type
            )
        )

    public fun numericMetricValue(
        extensionElement: Extension? = null,
        metricQuality: AbstractMetricValue.MetricQuality,
        annotation: List<AbstractMetricValue.Annotation> = listOf(),
        startTimeAttr: Timestamp? = null,
        stopTimeAttr: Timestamp? = null,
        determinationTimeAttr: Timestamp? = null,
        valueAttr: BigDecimal? = null
    ): NumericMetricValue = NumericMetricValue(
        abstractMetricValue = abstractMetricValue(
            extensionElement = extensionElement,
            metricQuality = metricQuality,
            annotation = annotation,
            startTimeAttr = startTimeAttr,
            stopTimeAttr = stopTimeAttr,
            determinationTimeAttr = determinationTimeAttr
        ),
        valueAttr = valueAttr
    )

    public fun stringMetricValue(
        extensionElement: Extension? = null,
        metricQuality: AbstractMetricValue.MetricQuality,
        annotation: List<AbstractMetricValue.Annotation> = listOf(),
        startTimeAttr: Timestamp? = null,
        stopTimeAttr: Timestamp? = null,
        determinationTimeAttr: Timestamp? = null,
        valueAttr: String? = null
    ): StringMetricValue = StringMetricValue(
        abstractMetricValue = abstractMetricValue(
            extensionElement = extensionElement,
            metricQuality = metricQuality,
            annotation = annotation,
            startTimeAttr = startTimeAttr,
            stopTimeAttr = stopTimeAttr,
            determinationTimeAttr = determinationTimeAttr
        ),
        valueAttr = valueAttr
    )

    public fun sampleArrayMetricValue(
        extensionElement: Extension? = null,
        metricQuality: AbstractMetricValue.MetricQuality,
        annotation: List<AbstractMetricValue.Annotation> = listOf(),
        startTimeAttr: Timestamp? = null,
        stopTimeAttr: Timestamp? = null,
        determinationTimeAttr: Timestamp? = null,
        samplesAttr: RealTimeValueType? = null
    ): SampleArrayValue = SampleArrayValue(
        abstractMetricValue = abstractMetricValue(
            extensionElement = extensionElement,
            metricQuality = metricQuality,
            annotation = annotation,
            startTimeAttr = startTimeAttr,
            stopTimeAttr = stopTimeAttr,
            determinationTimeAttr = determinationTimeAttr
        ),
        samplesAttr = samplesAttr
    )

    public fun range(lower: Double? = null, upper: Double? = null, stepWidth: Double? = null): Range = Range(
        lowerAttr = lower?.let { BigDecimal.valueOf(it) },
        upperAttr = upper?.let { BigDecimal.valueOf(it) },
        stepWidthAttr = stepWidth?.let { BigDecimal.valueOf(it) }
    )

    public fun metricQuality(
        extensionElement: Extension? = null,
        validityAttr: MeasurementValidity,
        modeAttr: GenerationMode? = null,
        qiAttr: QualityIndicator? = null,
    ): AbstractMetricValue.MetricQuality = AbstractMetricValue.MetricQuality(
        extensionElement = extensionElement,
        validityAttr = validityAttr,
        modeAttr = modeAttr,
        qiAttr = qiAttr
    )

    public fun mdsDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        productionSpecification: List<AbstractDeviceComponentDescriptor.ProductionSpecification> = listOf(),
        metaData: MdsDescriptor.MetaData? = null,
        approvedJurisdictions: ApprovedJurisdictions? = null
    ): MdsDescriptor = MdsDescriptor(
        abstractComplexDeviceComponentDescriptor = abstractComplexDeviceComponentDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            productionSpecification = productionSpecification
        ),
        metaData = metaData,
        approvedJurisdictions = approvedJurisdictions
    )

    public fun mdsState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        calibrationInfo: CalibrationInfo? = null,
        nextCalibration: CalibrationInfo? = null,
        physicalConnector: PhysicalConnectorInfo? = null,
        activationStateAttr: ComponentActivation? = null,
        operatingHoursAttr: Int? = null,
        operatingCyclesAttr: Int? = null,
        operatingJurisdiction: OperatingJurisdiction? = null,
        langAttr: String? = null,
        operatingModeAttr: MdsOperatingMode? = null
    ): MdsState = MdsState(
        abstractComplexDeviceComponentState = abstractComplexDeviceComponentState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            calibrationInfo = calibrationInfo,
            nextCalibration = nextCalibration,
            physicalConnector = physicalConnector,
            activationStateAttr = activationStateAttr,
            operatingHoursAttr = operatingHoursAttr,
            operatingCyclesAttr = operatingCyclesAttr
        ),
        operatingJurisdiction = operatingJurisdiction,
        langAttr = langAttr,
        operatingModeAttr = operatingModeAttr
    )

    public fun vmdDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        productionSpecification: List<AbstractDeviceComponentDescriptor.ProductionSpecification> = listOf(),
        approvedJurisdictions: ApprovedJurisdictions? = null
    ): VmdDescriptor = VmdDescriptor(
        abstractComplexDeviceComponentDescriptor = abstractComplexDeviceComponentDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            productionSpecification = productionSpecification
        ),
        approvedJurisdictions = approvedJurisdictions
    )

    public fun vmdState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        calibrationInfo: CalibrationInfo? = null,
        nextCalibration: CalibrationInfo? = null,
        physicalConnector: PhysicalConnectorInfo? = null,
        activationStateAttr: ComponentActivation? = null,
        operatingHoursAttr: Int? = null,
        operatingCyclesAttr: Int? = null,
        operatingJurisdiction: OperatingJurisdiction? = null
    ): VmdState = VmdState(
        abstractComplexDeviceComponentState = abstractComplexDeviceComponentState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            calibrationInfo = calibrationInfo,
            nextCalibration = nextCalibration,
            physicalConnector = physicalConnector,
            activationStateAttr = activationStateAttr,
            operatingHoursAttr = operatingHoursAttr,
            operatingCyclesAttr = operatingCyclesAttr
        ),
        operatingJurisdiction = operatingJurisdiction,
    )

    public fun channelDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        productionSpecification: List<AbstractDeviceComponentDescriptor.ProductionSpecification> = listOf()
    ): ChannelDescriptor = ChannelDescriptor(
        abstractDeviceComponentDescriptor = abstractDeviceComponentDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            productionSpecification = productionSpecification
        )
    )

    public fun channelState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        calibrationInfo: CalibrationInfo? = null,
        nextCalibration: CalibrationInfo? = null,
        physicalConnector: PhysicalConnectorInfo? = null,
        activationStateAttr: ComponentActivation? = null,
        operatingHoursAttr: Int? = null,
        operatingCyclesAttr: Int? = null
    ): ChannelState = ChannelState(
        abstractDeviceComponentState = abstractDeviceComponentState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            calibrationInfo = calibrationInfo,
            nextCalibration = nextCalibration,
            physicalConnector = physicalConnector,
            activationStateAttr = activationStateAttr,
            operatingHoursAttr = operatingHoursAttr,
            operatingCyclesAttr = operatingCyclesAttr
        )
    )

    public fun batteryDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        productionSpecification: List<AbstractDeviceComponentDescriptor.ProductionSpecification> = listOf(),
        capacityFullCharge: Measurement? = null,
        capacitySpecified: Measurement? = null,
        voltageSpecified: Measurement? = null
    ): BatteryDescriptor = BatteryDescriptor(
        abstractDeviceComponentDescriptor = abstractDeviceComponentDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            productionSpecification = productionSpecification
        ),
        capacityFullCharge = capacityFullCharge,
        capacitySpecified = capacitySpecified,
        voltageSpecified = voltageSpecified
    )

    public fun batteryState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        calibrationInfo: CalibrationInfo? = null,
        nextCalibration: CalibrationInfo? = null,
        physicalConnector: PhysicalConnectorInfo? = null,
        activationStateAttr: ComponentActivation? = null,
        operatingHoursAttr: Int? = null,
        operatingCyclesAttr: Int? = null,
        capacityRemaining: Measurement? = null,
        voltage: Measurement? = null,
        current: Measurement? = null,
        temperature: Measurement? = null,
        remainingBatteryTime: Measurement? = null,
        chargeStatusAttr: BatteryState.ChargeStatusAttr? = null,
        chargeCyclesAttr: Int? = null,
    ): BatteryState = BatteryState(
        abstractDeviceComponentState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            calibrationInfo = calibrationInfo,
            nextCalibration = nextCalibration,
            physicalConnector = physicalConnector,
            activationStateAttr = activationStateAttr,
            operatingHoursAttr = operatingHoursAttr,
            operatingCyclesAttr = operatingCyclesAttr
        ),
        capacityRemaining = capacityRemaining,
        voltage = voltage,
        current = current,
        temperature = temperature,
        remainingBatteryTime = remainingBatteryTime,
        chargeStatusAttr = chargeStatusAttr,
        chargeCyclesAttr = chargeCyclesAttr
    )

    public fun clockDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        productionSpecification: List<AbstractDeviceComponentDescriptor.ProductionSpecification> = listOf(),
        timeProtocol: List<CodedValue> = listOf(),
        resolutionAttr: Duration? = null
    ): ClockDescriptor = ClockDescriptor(
        abstractDeviceComponentDescriptor = abstractDeviceComponentDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            productionSpecification = productionSpecification
        ),
        timeProtocol = timeProtocol,
        resolutionAttr = resolutionAttr
    )

    public fun clockState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        calibrationInfo: CalibrationInfo? = null,
        nextCalibration: CalibrationInfo? = null,
        physicalConnector: PhysicalConnectorInfo? = null,
        activationStateAttr: ComponentActivation? = null,
        operatingHoursAttr: Int? = null,
        operatingCyclesAttr: Int? = null,
        activeSyncProtocol: CodedValue? = null,
        referenceSource: List<String> = listOf(),
        dateAndTimeAttr: Timestamp? = null,
        remoteSyncAttr: Boolean,
        accuracyAttr: BigDecimal? = null,
        lastSetAttr: Timestamp? = null,
        timeZoneAttr: TimeZone? = null,
        criticalUseAttr: Boolean? = null
    ): ClockState = ClockState(
        abstractDeviceComponentState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            calibrationInfo = calibrationInfo,
            nextCalibration = nextCalibration,
            physicalConnector = physicalConnector,
            activationStateAttr = activationStateAttr,
            operatingHoursAttr = operatingHoursAttr,
            operatingCyclesAttr = operatingCyclesAttr
        ),
        activeSyncProtocol = activeSyncProtocol,
        referenceSource = referenceSource,
        dateAndTimeAttr = dateAndTimeAttr,
        remoteSyncAttr = remoteSyncAttr,
        accuracyAttr = accuracyAttr,
        lastSetAttr = lastSetAttr,
        timeZoneAttr = timeZoneAttr,
        criticalUseAttr = criticalUseAttr
    )

    public fun systemContextDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        productionSpecification: List<AbstractDeviceComponentDescriptor.ProductionSpecification> = listOf()
    ): SystemContextDescriptor = SystemContextDescriptor(
        abstractDeviceComponentDescriptor = abstractDeviceComponentDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            productionSpecification = productionSpecification
        )
    )

    public fun systemContextState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        calibrationInfo: CalibrationInfo? = null,
        nextCalibration: CalibrationInfo? = null,
        physicalConnector: PhysicalConnectorInfo? = null,
        activationStateAttr: ComponentActivation? = null,
        operatingHoursAttr: Int? = null,
        operatingCyclesAttr: Int? = null
    ): SystemContextState = SystemContextState(
        abstractDeviceComponentState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            calibrationInfo = calibrationInfo,
            nextCalibration = nextCalibration,
            physicalConnector = physicalConnector,
            activationStateAttr = activationStateAttr,
            operatingHoursAttr = operatingHoursAttr,
            operatingCyclesAttr = operatingCyclesAttr
        )
    )

    public fun scoDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        productionSpecification: List<AbstractDeviceComponentDescriptor.ProductionSpecification> = listOf()
    ): ScoDescriptor = ScoDescriptor(
        abstractDeviceComponentDescriptor = abstractDeviceComponentDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            productionSpecification = productionSpecification
        )
    )

    public fun scoState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        calibrationInfo: CalibrationInfo? = null,
        nextCalibration: CalibrationInfo? = null,
        physicalConnector: PhysicalConnectorInfo? = null,
        activationStateAttr: ComponentActivation? = null,
        operatingHoursAttr: Int? = null,
        operatingCyclesAttr: Int? = null,
        operationGroup: List<ScoState.OperationGroup> = listOf(),
        invocationRequestedAttr: OperationRef? = null,
        invocationRequiredAttr: OperationRef? = null
    ): ScoState = ScoState(
        abstractDeviceComponentState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            calibrationInfo = calibrationInfo,
            nextCalibration = nextCalibration,
            physicalConnector = physicalConnector,
            activationStateAttr = activationStateAttr,
            operatingHoursAttr = operatingHoursAttr,
            operatingCyclesAttr = operatingCyclesAttr
        ),
        operationGroup = operationGroup,
        invocationRequestedAttr = invocationRequestedAttr,
        invocationRequiredAttr = invocationRequiredAttr
    )

    public fun alertSystemDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        maxPhysiologicalParallelAlarmsAttr: Int? = null,
        maxTechnicalParallelAlarmsAttr: Int? = null,
        selfCheckPeriodAttr: Duration?
    ): AlertSystemDescriptor = AlertSystemDescriptor(
        abstractAlertDescriptor = abstractAlertDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr
        ),
        maxPhysiologicalParallelAlarmsAttr = maxPhysiologicalParallelAlarmsAttr,
        maxTechnicalParallelAlarmsAttr = maxTechnicalParallelAlarmsAttr,
        selfCheckPeriodAttr = selfCheckPeriodAttr
    )

    public fun patientContextDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null
    ): PatientContextDescriptor = PatientContextDescriptor(
        abstractContextDescriptor = abstractContextDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr
        )
    )

    public fun patientContextState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        category: CodedValue? = null,
        handleAttr: Handle,
        validator: List<InstanceIdentifierOneOf> = listOf(),
        identification: List<InstanceIdentifierOneOf> = listOf(),
        contextAssociationAttr: ContextAssociation? = null,
        bindingMdibVersionAttr: ReferencedVersion? = null,
        unbindingMdibVersionAttr: ReferencedVersion? = null,
        bindingStartTimeAttr: Timestamp? = null,
        bindingEndTimeAttr: Timestamp? = null,
        coreData: PatientDemographicsCoreDataOneOf? = null
    ): PatientContextState = PatientContextState(
        abstractContextState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            category = category,
            handleAttr = handleAttr,
            validator = validator,
            identification = identification,
            contextAssociationAttr = contextAssociationAttr,
            bindingMdibVersionAttr = bindingMdibVersionAttr,
            unbindingMdibVersionAttr = unbindingMdibVersionAttr,
            bindingStartTimeAttr = bindingStartTimeAttr,
            bindingEndTimeAttr = bindingEndTimeAttr
        ),
        coreData = coreData
    )

    public fun locationContextDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null
    ): LocationContextDescriptor = LocationContextDescriptor(
        abstractContextDescriptor = abstractContextDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr
        )
    )

    public fun locationContextState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        category: CodedValue? = null,
        handleAttr: Handle,
        validator: List<InstanceIdentifierOneOf> = listOf(),
        identification: List<InstanceIdentifierOneOf> = listOf(),
        contextAssociationAttr: ContextAssociation? = null,
        bindingMdibVersionAttr: ReferencedVersion? = null,
        unbindingMdibVersionAttr: ReferencedVersion? = null,
        bindingStartTimeAttr: Timestamp? = null,
        bindingEndTimeAttr: Timestamp? = null,
        locationDetail: LocationDetail? = null
    ): LocationContextState = LocationContextState(
        abstractContextState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            category = category,
            handleAttr = handleAttr,
            validator = validator,
            identification = identification,
            contextAssociationAttr = contextAssociationAttr,
            bindingMdibVersionAttr = bindingMdibVersionAttr,
            unbindingMdibVersionAttr = unbindingMdibVersionAttr,
            bindingStartTimeAttr = bindingStartTimeAttr,
            bindingEndTimeAttr = bindingEndTimeAttr
        ),
        locationDetail = locationDetail
    )

    public fun ensembleContextDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null
    ): EnsembleContextDescriptor = EnsembleContextDescriptor(
        abstractContextDescriptor = abstractContextDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr
        )
    )

    public fun ensembleContextState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        category: CodedValue? = null,
        handleAttr: Handle,
        validator: List<InstanceIdentifierOneOf> = listOf(),
        identification: List<InstanceIdentifierOneOf> = listOf(),
        contextAssociationAttr: ContextAssociation? = null,
        bindingMdibVersionAttr: ReferencedVersion? = null,
        unbindingMdibVersionAttr: ReferencedVersion? = null,
        bindingStartTimeAttr: Timestamp? = null,
        bindingEndTimeAttr: Timestamp? = null
    ): EnsembleContextState = EnsembleContextState(
        abstractContextState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            category = category,
            handleAttr = handleAttr,
            validator = validator,
            identification = identification,
            contextAssociationAttr = contextAssociationAttr,
            bindingMdibVersionAttr = bindingMdibVersionAttr,
            unbindingMdibVersionAttr = unbindingMdibVersionAttr,
            bindingStartTimeAttr = bindingStartTimeAttr,
            bindingEndTimeAttr = bindingEndTimeAttr
        )
    )

    public fun operatorContextDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null
    ): OperatorContextDescriptor = OperatorContextDescriptor(
        abstractContextDescriptor = abstractContextDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr
        )
    )

    public fun operatorContextState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        category: CodedValue? = null,
        handleAttr: Handle,
        validator: List<InstanceIdentifierOneOf> = listOf(),
        identification: List<InstanceIdentifierOneOf> = listOf(),
        contextAssociationAttr: ContextAssociation? = null,
        bindingMdibVersionAttr: ReferencedVersion? = null,
        unbindingMdibVersionAttr: ReferencedVersion? = null,
        bindingStartTimeAttr: Timestamp? = null,
        bindingEndTimeAttr: Timestamp? = null,
        operatorDetails: BaseDemographicsOneOf? = null
    ): OperatorContextState = OperatorContextState(
        abstractContextState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            category = category,
            handleAttr = handleAttr,
            validator = validator,
            identification = identification,
            contextAssociationAttr = contextAssociationAttr,
            bindingMdibVersionAttr = bindingMdibVersionAttr,
            unbindingMdibVersionAttr = unbindingMdibVersionAttr,
            bindingStartTimeAttr = bindingStartTimeAttr,
            bindingEndTimeAttr = bindingEndTimeAttr
        ),
        operatorDetails = operatorDetails
    )

    public fun meansContextDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null
    ): MeansContextDescriptor = MeansContextDescriptor(
        abstractContextDescriptor = abstractContextDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr
        )
    )

    public fun meansContextState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        category: CodedValue? = null,
        handleAttr: Handle,
        validator: List<InstanceIdentifierOneOf> = listOf(),
        identification: List<InstanceIdentifierOneOf> = listOf(),
        contextAssociationAttr: ContextAssociation? = null,
        bindingMdibVersionAttr: ReferencedVersion? = null,
        unbindingMdibVersionAttr: ReferencedVersion? = null,
        bindingStartTimeAttr: Timestamp? = null,
        bindingEndTimeAttr: Timestamp? = null
    ): MeansContextState = MeansContextState(
        abstractContextState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            category = category,
            handleAttr = handleAttr,
            validator = validator,
            identification = identification,
            contextAssociationAttr = contextAssociationAttr,
            bindingMdibVersionAttr = bindingMdibVersionAttr,
            unbindingMdibVersionAttr = unbindingMdibVersionAttr,
            bindingStartTimeAttr = bindingStartTimeAttr,
            bindingEndTimeAttr = bindingEndTimeAttr
        )
    )

    public fun workflowContextDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null
    ): WorkflowContextDescriptor = WorkflowContextDescriptor(
        abstractContextDescriptor = abstractContextDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr
        )
    )

    public fun stringMetricDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        unit: CodedValue,
        bodySite: List<CodedValue> = listOf(),
        relation: List<AbstractMetricDescriptor.Relation> = listOf(),
        metricCategoryAttr: MetricCategory,
        derivationMethodAttr: DerivationMethod? = null,
        metricAvailabilityAttr: MetricAvailability,
        maxMeasurementTimeAttr: Duration? = null,
        maxDelayTimeAttr: Duration? = null,
        determinationPeriodAttr: Duration? = null,
        lifeTimePeriodAttr: Duration? = null,
        activationDurationAttr: Duration? = null
    ): StringMetricDescriptor = StringMetricDescriptor(
        abstractMetricDescriptor = abstractMetricDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            unit = unit,
            bodySite = bodySite,
            relation = relation,
            metricCategoryAttr = metricCategoryAttr,
            derivationMethodAttr = derivationMethodAttr,
            metricAvailabilityAttr = metricAvailabilityAttr,
            maxMeasurementTimeAttr = maxMeasurementTimeAttr,
            maxDelayTimeAttr = maxDelayTimeAttr,
            determinationPeriodAttr = determinationPeriodAttr,
            lifeTimePeriodAttr = lifeTimePeriodAttr,
            activationDurationAttr = activationDurationAttr
        )
    )

    public fun stringMetricState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        bodySite: List<CodedValue> = listOf(),
        physicalConnector: PhysicalConnectorInfo? = null,
        activationStateAttr: ComponentActivation? = null,
        activeDeterminationPeriodAttr: Duration? = null,
        lifeTimePeriodAttr: Duration? = null,
        metricValue: StringMetricValue? = null
    ): StringMetricState = StringMetricState(
        abstractMetricState = abstractMetricState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            bodySite = bodySite,
            physicalConnector = physicalConnector,
            activationStateAttr = activationStateAttr,
            activeDeterminationPeriodAttr = activeDeterminationPeriodAttr,
            lifeTimePeriodAttr = lifeTimePeriodAttr
        ),
        metricValue = metricValue
    )

    public fun enumStringMetricDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        unit: CodedValue,
        bodySite: List<CodedValue> = listOf(),
        relation: List<AbstractMetricDescriptor.Relation> = listOf(),
        metricCategoryAttr: MetricCategory,
        derivationMethodAttr: DerivationMethod? = null,
        metricAvailabilityAttr: MetricAvailability,
        maxMeasurementTimeAttr: Duration? = null,
        maxDelayTimeAttr: Duration? = null,
        determinationPeriodAttr: Duration? = null,
        lifeTimePeriodAttr: Duration? = null,
        activationDurationAttr: Duration? = null,
        allowedValue: List<EnumStringMetricDescriptor.AllowedValue> = listOf()
    ): EnumStringMetricDescriptor = EnumStringMetricDescriptor(
        stringMetricDescriptor = stringMetricDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            unit = unit,
            bodySite = bodySite,
            relation = relation,
            metricCategoryAttr = metricCategoryAttr,
            derivationMethodAttr = derivationMethodAttr,
            metricAvailabilityAttr = metricAvailabilityAttr,
            maxMeasurementTimeAttr = maxMeasurementTimeAttr,
            maxDelayTimeAttr = maxDelayTimeAttr,
            determinationPeriodAttr = determinationPeriodAttr,
            lifeTimePeriodAttr = lifeTimePeriodAttr,
            activationDurationAttr = activationDurationAttr
        ),
        allowedValue = allowedValue
    )

    public fun enumStringMetricState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        bodySite: List<CodedValue> = listOf(),
        physicalConnector: PhysicalConnectorInfo? = null,
        activationStateAttr: ComponentActivation? = null,
        activeDeterminationPeriodAttr: Duration? = null,
        lifeTimePeriodAttr: Duration? = null,
        metricValue: StringMetricValue? = null
    ): EnumStringMetricState = EnumStringMetricState(
        stringMetricState = stringMetricState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            bodySite = bodySite,
            physicalConnector = physicalConnector,
            activationStateAttr = activationStateAttr,
            activeDeterminationPeriodAttr = activeDeterminationPeriodAttr,
            lifeTimePeriodAttr = lifeTimePeriodAttr,
            metricValue = metricValue
        )
    )

    public fun numericMetricDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        unit: CodedValue,
        bodySite: List<CodedValue> = listOf(),
        relation: List<AbstractMetricDescriptor.Relation> = listOf(),
        metricCategoryAttr: MetricCategory,
        derivationMethodAttr: DerivationMethod? = null,
        metricAvailabilityAttr: MetricAvailability,
        maxMeasurementTimeAttr: Duration? = null,
        maxDelayTimeAttr: Duration? = null,
        determinationPeriodAttr: Duration? = null,
        lifeTimePeriodAttr: Duration? = null,
        activationDurationAttr: Duration? = null,
        technicalRange: List<Range> = listOf(),
        resolutionAttr: BigDecimal,
        averagingPeriodAttr: Duration? = null
    ): NumericMetricDescriptor = NumericMetricDescriptor(
        abstractMetricDescriptor = abstractMetricDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            unit = unit,
            bodySite = bodySite,
            relation = relation,
            metricCategoryAttr = metricCategoryAttr,
            derivationMethodAttr = derivationMethodAttr,
            metricAvailabilityAttr = metricAvailabilityAttr,
            maxMeasurementTimeAttr = maxMeasurementTimeAttr,
            maxDelayTimeAttr = maxDelayTimeAttr,
            determinationPeriodAttr = determinationPeriodAttr,
            lifeTimePeriodAttr = lifeTimePeriodAttr,
            activationDurationAttr = activationDurationAttr
        ),
        technicalRange = technicalRange,
        resolutionAttr = resolutionAttr,
        averagingPeriodAttr = averagingPeriodAttr
    )

    public fun numericMetricState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        bodySite: List<CodedValue> = listOf(),
        physicalConnector: PhysicalConnectorInfo? = null,
        activationStateAttr: ComponentActivation? = null,
        activeDeterminationPeriodAttr: Duration? = null,
        lifeTimePeriodAttr: Duration? = null,
        metricValue: NumericMetricValue? = null,
        physiologicalRange: List<Range> = listOf(),
        activeAveragingPeriodAttr: Duration? = null,
    ): NumericMetricState = NumericMetricState(
        abstractMetricState = abstractMetricState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            bodySite = bodySite,
            physicalConnector = physicalConnector,
            activationStateAttr = activationStateAttr,
            activeDeterminationPeriodAttr = activeDeterminationPeriodAttr,
            lifeTimePeriodAttr = lifeTimePeriodAttr
        ),
        metricValue = metricValue,
        physiologicalRange = physiologicalRange,
        activeAveragingPeriodAttr = activeAveragingPeriodAttr
    )

    public fun realTimeSampleArrayMetricDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        unit: CodedValue,
        bodySite: List<CodedValue> = listOf(),
        relation: List<AbstractMetricDescriptor.Relation> = listOf(),
        metricCategoryAttr: MetricCategory,
        derivationMethodAttr: DerivationMethod? = null,
        metricAvailabilityAttr: MetricAvailability,
        maxMeasurementTimeAttr: Duration? = null,
        maxDelayTimeAttr: Duration? = null,
        determinationPeriodAttr: Duration? = null,
        lifeTimePeriodAttr: Duration? = null,
        activationDurationAttr: Duration? = null,
        technicalRange: List<Range> = listOf(),
        resolutionAttr: BigDecimal,
        samplePeriodAttr: Duration,
    ): RealTimeSampleArrayMetricDescriptor = RealTimeSampleArrayMetricDescriptor(
        abstractMetricDescriptor = abstractMetricDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            unit = unit,
            bodySite = bodySite,
            relation = relation,
            metricCategoryAttr = metricCategoryAttr,
            derivationMethodAttr = derivationMethodAttr,
            metricAvailabilityAttr = metricAvailabilityAttr,
            maxMeasurementTimeAttr = maxMeasurementTimeAttr,
            maxDelayTimeAttr = maxDelayTimeAttr,
            determinationPeriodAttr = determinationPeriodAttr,
            lifeTimePeriodAttr = lifeTimePeriodAttr,
            activationDurationAttr = activationDurationAttr
        ),
        technicalRange = technicalRange,
        resolutionAttr = resolutionAttr,
        samplePeriodAttr = samplePeriodAttr
    )

    public fun realTimeSampleArrayMetricState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        bodySite: List<CodedValue> = listOf(),
        physicalConnector: PhysicalConnectorInfo? = null,
        activationStateAttr: ComponentActivation? = null,
        activeDeterminationPeriodAttr: Duration? = null,
        lifeTimePeriodAttr: Duration? = null,
        metricValue: SampleArrayValue? = null,
        physiologicalRange: List<Range> = listOf()
    ): RealTimeSampleArrayMetricState = RealTimeSampleArrayMetricState(
        abstractMetricState = abstractMetricState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            bodySite = bodySite,
            physicalConnector = physicalConnector,
            activationStateAttr = activationStateAttr,
            activeDeterminationPeriodAttr = activeDeterminationPeriodAttr,
            lifeTimePeriodAttr = lifeTimePeriodAttr
        ),
        metricValue = metricValue,
        physiologicalRange = physiologicalRange
    )

    public fun distributionSampleArrayMetricDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        unit: CodedValue,
        bodySite: List<CodedValue> = listOf(),
        relation: List<AbstractMetricDescriptor.Relation> = listOf(),
        metricCategoryAttr: MetricCategory,
        derivationMethodAttr: DerivationMethod? = null,
        metricAvailabilityAttr: MetricAvailability,
        maxMeasurementTimeAttr: Duration? = null,
        maxDelayTimeAttr: Duration? = null,
        determinationPeriodAttr: Duration? = null,
        lifeTimePeriodAttr: Duration? = null,
        activationDurationAttr: Duration? = null,
        technicalRange: List<Range> = listOf(),
        domainUnit: CodedValue,
        distributionRange: Range,
        resolutionAttr: BigDecimal
    ): DistributionSampleArrayMetricDescriptor = DistributionSampleArrayMetricDescriptor(
        abstractMetricDescriptor = abstractMetricDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            unit = unit,
            bodySite = bodySite,
            relation = relation,
            metricCategoryAttr = metricCategoryAttr,
            derivationMethodAttr = derivationMethodAttr,
            metricAvailabilityAttr = metricAvailabilityAttr,
            maxMeasurementTimeAttr = maxMeasurementTimeAttr,
            maxDelayTimeAttr = maxDelayTimeAttr,
            determinationPeriodAttr = determinationPeriodAttr,
            lifeTimePeriodAttr = lifeTimePeriodAttr,
            activationDurationAttr = activationDurationAttr
        ),
        technicalRange = technicalRange,
        domainUnit = domainUnit,
        distributionRange = distributionRange,
        resolutionAttr = resolutionAttr
    )

    public fun distributionSampleArrayMetricState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        bodySite: List<CodedValue> = listOf(),
        physicalConnector: PhysicalConnectorInfo? = null,
        activationStateAttr: ComponentActivation? = null,
        activeDeterminationPeriodAttr: Duration? = null,
        lifeTimePeriodAttr: Duration? = null,
        metricValue: SampleArrayValue? = null,
        physiologicalRange: List<Range> = listOf()
    ): DistributionSampleArrayMetricState = DistributionSampleArrayMetricState(
        abstractMetricState = abstractMetricState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            bodySite = bodySite,
            physicalConnector = physicalConnector,
            activationStateAttr = activationStateAttr,
            activeDeterminationPeriodAttr = activeDeterminationPeriodAttr,
            lifeTimePeriodAttr = lifeTimePeriodAttr
        ),
        metricValue = metricValue,
        physiologicalRange = physiologicalRange
    )

    public fun setStringOperationDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        operationTargetAttr: HandleRef,
        maxTimeToFinishAttr: Duration? = null,
        invocationEffectiveTimeoutAttr: Duration? = null,
        retriggerableAttr: Boolean? = null,
        accessLevelAttr: AbstractOperationDescriptor.AccessLevelAttr? = null,
        maxLengthAttr: Long? = null
    ): SetStringOperationDescriptor = SetStringOperationDescriptor(
        abstractOperationDescriptor = abstractOperationDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            operationTargetAttr = operationTargetAttr,
            maxTimeToFinishAttr = maxTimeToFinishAttr,
            invocationEffectiveTimeoutAttr = invocationEffectiveTimeoutAttr,
            retriggerableAttr = retriggerableAttr,
            accessLevelAttr = accessLevelAttr
        ),
        maxLengthAttr = maxLengthAttr
    )

    public fun setStringOperationState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        operatingModeAttr: OperatingMode,
        allowedValues: SetStringOperationState.AllowedValues? = null
    ): SetStringOperationState = SetStringOperationState(
        abstractOperationState = abstractOperationState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            operatingModeAttr = operatingModeAttr
        ),
        allowedValues = allowedValues
    )

    public fun setValueOperationDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        operationTargetAttr: HandleRef,
        maxTimeToFinishAttr: Duration? = null,
        invocationEffectiveTimeoutAttr: Duration? = null,
        retriggerableAttr: Boolean? = null,
        accessLevelAttr: AbstractOperationDescriptor.AccessLevelAttr? = null
    ): SetValueOperationDescriptor = SetValueOperationDescriptor(
        abstractOperationDescriptor = abstractOperationDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            operationTargetAttr = operationTargetAttr,
            maxTimeToFinishAttr = maxTimeToFinishAttr,
            invocationEffectiveTimeoutAttr = invocationEffectiveTimeoutAttr,
            retriggerableAttr = retriggerableAttr,
            accessLevelAttr = accessLevelAttr
        )
    )

    public fun setValueOperationState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        operatingModeAttr: OperatingMode,
        allowedRange: List<Range> = listOf()
    ): SetValueOperationState = SetValueOperationState(
        abstractOperationState = abstractOperationState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            operatingModeAttr = operatingModeAttr
        ),
        allowedRange = allowedRange
    )

    public fun activateOperationDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        operationTargetAttr: HandleRef,
        maxTimeToFinishAttr: Duration? = null,
        invocationEffectiveTimeoutAttr: Duration? = null,
        retriggerableAttr: Boolean? = null,
        accessLevelAttr: AbstractOperationDescriptor.AccessLevelAttr? = null,
        modifiableData: List<String> = listOf(),
        argument: List<ActivateOperationDescriptor.Argument> = listOf()
    ): ActivateOperationDescriptor = ActivateOperationDescriptor(
        abstractSetStateOperationDescriptor = abstractSetStateOperationDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            operationTargetAttr = operationTargetAttr,
            maxTimeToFinishAttr = maxTimeToFinishAttr,
            invocationEffectiveTimeoutAttr = invocationEffectiveTimeoutAttr,
            retriggerableAttr = retriggerableAttr,
            accessLevelAttr = accessLevelAttr,
            modifiableData = modifiableData
        ),
        argument = argument
    )

    public fun activateOperationState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        operatingModeAttr: OperatingMode
    ): ActivateOperationState = ActivateOperationState(
        abstractOperationState = abstractOperationState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            operatingModeAttr = operatingModeAttr
        )
    )

    public fun setMetricStateOperationDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        operationTargetAttr: HandleRef,
        maxTimeToFinishAttr: Duration? = null,
        invocationEffectiveTimeoutAttr: Duration? = null,
        retriggerableAttr: Boolean? = null,
        accessLevelAttr: AbstractOperationDescriptor.AccessLevelAttr? = null,
        modifiableData: List<String> = listOf()
    ): SetMetricStateOperationDescriptor = SetMetricStateOperationDescriptor(
        abstractSetStateOperationDescriptor = abstractSetStateOperationDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            operationTargetAttr = operationTargetAttr,
            maxTimeToFinishAttr = maxTimeToFinishAttr,
            invocationEffectiveTimeoutAttr = invocationEffectiveTimeoutAttr,
            retriggerableAttr = retriggerableAttr,
            accessLevelAttr = accessLevelAttr,
            modifiableData = modifiableData
        )
    )

    public fun setMetricStateOperationState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        operatingModeAttr: OperatingMode
    ): SetMetricStateOperationState = SetMetricStateOperationState(
        abstractOperationState = abstractOperationState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            operatingModeAttr = operatingModeAttr
        )
    )

    public fun setComponentStateOperationDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        operationTargetAttr: HandleRef,
        maxTimeToFinishAttr: Duration? = null,
        invocationEffectiveTimeoutAttr: Duration? = null,
        retriggerableAttr: Boolean? = null,
        accessLevelAttr: AbstractOperationDescriptor.AccessLevelAttr? = null,
        modifiableData: List<String> = listOf()
    ): SetComponentStateOperationDescriptor = SetComponentStateOperationDescriptor(
        abstractSetStateOperationDescriptor = abstractSetStateOperationDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            operationTargetAttr = operationTargetAttr,
            maxTimeToFinishAttr = maxTimeToFinishAttr,
            invocationEffectiveTimeoutAttr = invocationEffectiveTimeoutAttr,
            retriggerableAttr = retriggerableAttr,
            accessLevelAttr = accessLevelAttr,
            modifiableData = modifiableData
        )
    )

    public fun setComponentStateOperationState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        operatingModeAttr: OperatingMode
    ): SetComponentStateOperationState = SetComponentStateOperationState(
        abstractOperationState = abstractOperationState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            operatingModeAttr = operatingModeAttr
        )
    )

    public fun setAlertStateOperationDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        operationTargetAttr: HandleRef,
        maxTimeToFinishAttr: Duration? = null,
        invocationEffectiveTimeoutAttr: Duration? = null,
        retriggerableAttr: Boolean? = null,
        accessLevelAttr: AbstractOperationDescriptor.AccessLevelAttr? = null,
        modifiableData: List<String> = listOf()
    ): SetAlertStateOperationDescriptor = SetAlertStateOperationDescriptor(
        abstractSetStateOperationDescriptor = abstractSetStateOperationDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            operationTargetAttr = operationTargetAttr,
            maxTimeToFinishAttr = maxTimeToFinishAttr,
            invocationEffectiveTimeoutAttr = invocationEffectiveTimeoutAttr,
            retriggerableAttr = retriggerableAttr,
            accessLevelAttr = accessLevelAttr,
            modifiableData = modifiableData
        )
    )

    public fun setAlertStateOperationState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        operatingModeAttr: OperatingMode
    ): SetAlertStateOperationState = SetAlertStateOperationState(
        abstractOperationState = abstractOperationState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            operatingModeAttr = operatingModeAttr
        )
    )

    public fun setContextStateOperationDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        operationTargetAttr: HandleRef,
        maxTimeToFinishAttr: Duration? = null,
        invocationEffectiveTimeoutAttr: Duration? = null,
        retriggerableAttr: Boolean? = null,
        accessLevelAttr: AbstractOperationDescriptor.AccessLevelAttr? = null,
        modifiableData: List<String> = listOf()
    ): SetContextStateOperationDescriptor = SetContextStateOperationDescriptor(
        abstractSetStateOperationDescriptor = abstractSetStateOperationDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            operationTargetAttr = operationTargetAttr,
            maxTimeToFinishAttr = maxTimeToFinishAttr,
            invocationEffectiveTimeoutAttr = invocationEffectiveTimeoutAttr,
            retriggerableAttr = retriggerableAttr,
            accessLevelAttr = accessLevelAttr,
            modifiableData = modifiableData
        )
    )

    public fun setContextStateOperationState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        operatingModeAttr: OperatingMode
    ): SetContextStateOperationState = SetContextStateOperationState(
        abstractOperationState = abstractOperationState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            operatingModeAttr = operatingModeAttr
        )
    )

    public fun workflowContextState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        category: CodedValue? = null,
        handleAttr: Handle,
        validator: List<InstanceIdentifierOneOf> = listOf(),
        identification: List<InstanceIdentifierOneOf> = listOf(),
        contextAssociationAttr: ContextAssociation? = null,
        bindingMdibVersionAttr: ReferencedVersion? = null,
        unbindingMdibVersionAttr: ReferencedVersion? = null,
        bindingStartTimeAttr: Timestamp? = null,
        bindingEndTimeAttr: Timestamp? = null,
        workflowDetail: WorkflowContextState.WorkflowDetail? = null,
    ): WorkflowContextState = WorkflowContextState(
        abstractContextState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            category = category,
            handleAttr = handleAttr,
            validator = validator,
            identification = identification,
            contextAssociationAttr = contextAssociationAttr,
            bindingMdibVersionAttr = bindingMdibVersionAttr,
            unbindingMdibVersionAttr = unbindingMdibVersionAttr,
            bindingStartTimeAttr = bindingStartTimeAttr,
            bindingEndTimeAttr = bindingEndTimeAttr
        ),
        workflowDetail = workflowDetail
    )

    public fun alertSystemState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        activationStateAttr: AlertActivation,
        systemSignalActivation: List<SystemSignalActivation> = listOf(),
        lastSelfCheckAttr: Timestamp? = null,
        selfCheckCountAttr: Long? = null,
        presentPhysiologicalAlarmConditionsAttr: AlertConditionReference? = null,
        presentTechnicalAlarmConditionsAttr: AlertConditionReference? = null
    ): AlertSystemState = AlertSystemState(
        abstractAlertState = abstractAlertState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            activationStateAttr = activationStateAttr
        ),
        systemSignalActivation = systemSignalActivation,
        lastSelfCheckAttr = lastSelfCheckAttr,
        selfCheckCountAttr = selfCheckCountAttr,
        presentPhysiologicalAlarmConditionsAttr = presentPhysiologicalAlarmConditionsAttr,
        presentTechnicalAlarmConditionsAttr = presentTechnicalAlarmConditionsAttr
    )

    public fun alertConditionDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        source: List<HandleRef> = listOf(),
        causeInfo: List<CauseInfo> = listOf(),
        kindAttr: AlertConditionKind,
        priorityAttr: AlertConditionPriority,
        defaultConditionGenerationDelayAttr: Duration? = null,
        canEscalateAttr: AlertConditionDescriptor.CanEscalateAttr? = null,
        canDeescalateAttr: AlertConditionDescriptor.CanDeescalateAttr? = null,
    ): AlertConditionDescriptor = AlertConditionDescriptor(
        abstractAlertDescriptor = abstractAlertDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr
        ),
        source = source,
        causeInfo = causeInfo,
        kindAttr = kindAttr,
        priorityAttr = priorityAttr,
        defaultConditionGenerationDelayAttr = defaultConditionGenerationDelayAttr,
        canEscalateAttr = canEscalateAttr,
        canDeescalateAttr = canDeescalateAttr
    )

    public fun alertConditionState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        activationStateAttr: AlertActivation,
        actualPriorityAttr: AlertConditionPriority? = null,
        rankAttr: Int? = null,
        presenceAttr: Boolean? = null,
        determinationTimeAttr: Timestamp? = null
    ): AlertConditionState = AlertConditionState(
        abstractAlertState = abstractAlertState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            activationStateAttr = activationStateAttr
        ),
        actualPriorityAttr = actualPriorityAttr,
        rankAttr = rankAttr,
        presenceAttr = presenceAttr,
        determinationTimeAttr = determinationTimeAttr
    )

    public fun limitAlertConditionDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        source: List<HandleRef> = listOf(),
        causeInfo: List<CauseInfo> = listOf(),
        kindAttr: AlertConditionKind,
        priorityAttr: AlertConditionPriority,
        defaultConditionGenerationDelayAttr: Duration? = null,
        canEscalateAttr: AlertConditionDescriptor.CanEscalateAttr? = null,
        canDeescalateAttr: AlertConditionDescriptor.CanDeescalateAttr? = null,
        maxLimits: Range,
        autoLimitSupportedAttr: Boolean?
    ): LimitAlertConditionDescriptor = LimitAlertConditionDescriptor(
        alertConditionDescriptor = alertConditionDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            source = source,
            causeInfo = causeInfo,
            kindAttr = kindAttr,
            priorityAttr = priorityAttr,
            defaultConditionGenerationDelayAttr = defaultConditionGenerationDelayAttr,
            canEscalateAttr = canEscalateAttr,
            canDeescalateAttr = canDeescalateAttr
        ),
        maxLimits = maxLimits,
        autoLimitSupportedAttr = autoLimitSupportedAttr
    )

    public fun limitAlertConditionState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        activationStateAttr: AlertActivation,
        actualPriorityAttr: AlertConditionPriority? = null,
        rankAttr: Int? = null,
        presenceAttr: Boolean? = null,
        determinationTimeAttr: Timestamp? = null,
        limits: Range,
        monitoredAlertLimitsAttr: AlertConditionMonitoredLimits,
        autoLimitActivationStateAttr: AlertActivation? = null
    ): LimitAlertConditionState = LimitAlertConditionState(
        alertConditionState = alertConditionState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            activationStateAttr = activationStateAttr,
            actualPriorityAttr = actualPriorityAttr,
            rankAttr = rankAttr,
            presenceAttr = presenceAttr,
            determinationTimeAttr = determinationTimeAttr
        ),
        limits = limits,
        monitoredAlertLimitsAttr = monitoredAlertLimitsAttr,
        autoLimitActivationStateAttr = autoLimitActivationStateAttr
    )

    public fun alertSignalDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        conditionSignaledAttr: HandleRef? = null,
        manifestationAttr: AlertSignalManifestation,
        latchingAttr: Boolean,
        defaultSignalGenerationDelayAttr: Duration? = null,
        minSignalGenerationDelayAttr: Duration? = null,
        maxSignalGenerationDelayAttr: Duration? = null,
        signalDelegationSupportedAttr: Boolean? = null,
        acknowledgementSupportedAttr: Boolean? = null,
        acknowledgeTimeoutAttr: Duration? = null
    ): AlertSignalDescriptor = AlertSignalDescriptor(
        abstractAlertDescriptor = abstractAlertDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr
        ),
        conditionSignaledAttr = conditionSignaledAttr,
        manifestationAttr = manifestationAttr,
        latchingAttr = latchingAttr,
        defaultSignalGenerationDelayAttr = defaultSignalGenerationDelayAttr,
        minSignalGenerationDelayAttr = minSignalGenerationDelayAttr,
        maxSignalGenerationDelayAttr = maxSignalGenerationDelayAttr,
        signalDelegationSupportedAttr = signalDelegationSupportedAttr,
        acknowledgementSupportedAttr = acknowledgementSupportedAttr,
        acknowledgeTimeoutAttr = acknowledgeTimeoutAttr
    )

    public fun alertSignalState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        activationStateAttr: AlertActivation,
        actualSignalGenerationDelayAttr: Duration? = null,
        presenceAttr: AlertSignalPresence? = null,
        locationAttr: AlertSignalPrimaryLocation? = null,
        slotAttr: Int? = null,
    ): AlertSignalState = AlertSignalState(
        abstractAlertState = abstractAlertState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            activationStateAttr = activationStateAttr
        ),
        actualSignalGenerationDelayAttr = actualSignalGenerationDelayAttr,
        presenceAttr = presenceAttr,
        locationAttr = locationAttr,
        slotAttr = slotAttr
    )

    private fun abstractMetricValue(
        extensionElement: Extension? = null,
        metricQuality: AbstractMetricValue.MetricQuality,
        annotation: List<AbstractMetricValue.Annotation> = listOf(),
        startTimeAttr: Timestamp? = null,
        stopTimeAttr: Timestamp? = null,
        determinationTimeAttr: Timestamp? = null
    ) = AbstractMetricValue(
        extensionElement = extensionElement,
        metricQuality = metricQuality,
        annotation = annotation,
        startTimeAttr = startTimeAttr,
        stopTimeAttr = stopTimeAttr,
        determinationTimeAttr = determinationTimeAttr
    )

    private fun abstractSetStateOperationDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        operationTargetAttr: HandleRef,
        maxTimeToFinishAttr: Duration? = null,
        invocationEffectiveTimeoutAttr: Duration? = null,
        retriggerableAttr: Boolean? = null,
        accessLevelAttr: AbstractOperationDescriptor.AccessLevelAttr? = null,
        modifiableData: List<String> = listOf()
    ) = AbstractSetStateOperationDescriptor(
        abstractOperationDescriptor = abstractOperationDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            operationTargetAttr = operationTargetAttr,
            maxTimeToFinishAttr = maxTimeToFinishAttr,
            invocationEffectiveTimeoutAttr = invocationEffectiveTimeoutAttr,
            retriggerableAttr = retriggerableAttr,
            accessLevelAttr = accessLevelAttr
        ),
        modifiableData = modifiableData
    )

    private fun abstractOperationDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        operationTargetAttr: HandleRef,
        maxTimeToFinishAttr: Duration? = null,
        invocationEffectiveTimeoutAttr: Duration? = null,
        retriggerableAttr: Boolean? = null,
        accessLevelAttr: AbstractOperationDescriptor.AccessLevelAttr? = null
    ) = AbstractOperationDescriptor(
        abstractDescriptor = abstractDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr
        ),
        operationTargetAttr = operationTargetAttr,
        maxTimeToFinishAttr = maxTimeToFinishAttr,
        invocationEffectiveTimeoutAttr = invocationEffectiveTimeoutAttr,
        retriggerableAttr = retriggerableAttr,
        accessLevelAttr = accessLevelAttr
    )

    private fun abstractOperationState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        operatingModeAttr: OperatingMode
    ) = AbstractOperationState(
        abstractState = abstractState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr
        ),
        operatingModeAttr = operatingModeAttr
    )

    private fun abstractMetricDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        unit: CodedValue,
        bodySite: List<CodedValue> = listOf(),
        relation: List<AbstractMetricDescriptor.Relation> = listOf(),
        metricCategoryAttr: MetricCategory,
        derivationMethodAttr: DerivationMethod? = null,
        metricAvailabilityAttr: MetricAvailability,
        maxMeasurementTimeAttr: Duration? = null,
        maxDelayTimeAttr: Duration? = null,
        determinationPeriodAttr: Duration? = null,
        lifeTimePeriodAttr: Duration? = null,
        activationDurationAttr: Duration? = null
    ) = AbstractMetricDescriptor(
        abstractDescriptor = abstractDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr
        ),
        unit = unit,
        bodySite = bodySite,
        relation = relation,
        metricCategoryAttr = metricCategoryAttr,
        derivationMethodAttr = derivationMethodAttr,
        metricAvailabilityAttr = metricAvailabilityAttr,
        maxMeasurementTimeAttr = maxMeasurementTimeAttr,
        maxDelayTimeAttr = maxDelayTimeAttr,
        determinationPeriodAttr = determinationPeriodAttr,
        lifeTimePeriodAttr = lifeTimePeriodAttr,
        activationDurationAttr = activationDurationAttr
    )

    private fun abstractMetricState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        bodySite: List<CodedValue> = listOf(),
        physicalConnector: PhysicalConnectorInfo? = null,
        activationStateAttr: ComponentActivation? = null,
        activeDeterminationPeriodAttr: Duration? = null,
        lifeTimePeriodAttr: Duration? = null,
    ) = AbstractMetricState(
        abstractState = abstractState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr
        ),
        bodySite = bodySite,
        physicalConnector = physicalConnector,
        activationStateAttr = activationStateAttr,
        activeDeterminationPeriodAttr = activeDeterminationPeriodAttr,
        lifeTimePeriodAttr = lifeTimePeriodAttr
    )

    private fun abstractContextDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null
    ) = AbstractContextDescriptor(
        abstractDescriptor = abstractDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr
        )
    )

    private fun abstractContextState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        category: CodedValue? = null,
        handleAttr: Handle,
        validator: List<InstanceIdentifierOneOf> = listOf(),
        identification: List<InstanceIdentifierOneOf> = listOf(),
        contextAssociationAttr: ContextAssociation? = null,
        bindingMdibVersionAttr: ReferencedVersion? = null,
        unbindingMdibVersionAttr: ReferencedVersion? = null,
        bindingStartTimeAttr: Timestamp? = null,
        bindingEndTimeAttr: Timestamp? = null
    ) = AbstractContextState(
        abstractMultiState = abstractMultiState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            category = category,
            handleAttr = handleAttr
        ),
        validator = validator,
        identification = identification,
        contextAssociationAttr = contextAssociationAttr,
        bindingMdibVersionAttr = bindingMdibVersionAttr,
        unbindingMdibVersionAttr = unbindingMdibVersionAttr,
        bindingStartTimeAttr = bindingStartTimeAttr,
        bindingEndTimeAttr = bindingEndTimeAttr
    )

    private fun abstractMultiState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        category: CodedValue? = null,
        handleAttr: Handle,
    ) = AbstractMultiState(
        abstractState = abstractState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr
        ),
        category = category,
        handleAttr = handleAttr
    )

    private fun abstractAlertDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null
    ) = AbstractAlertDescriptor(
        abstractDescriptor = abstractDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr
        )
    )

    private fun abstractAlertState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        activationStateAttr: AlertActivation
    ) = AbstractAlertState(
        abstractState = abstractState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr
        ),
        activationStateAttr = activationStateAttr
    )

    private fun abstractComplexDeviceComponentState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        calibrationInfo: CalibrationInfo? = null,
        nextCalibration: CalibrationInfo? = null,
        physicalConnector: PhysicalConnectorInfo? = null,
        activationStateAttr: ComponentActivation? = null,
        operatingHoursAttr: Int? = null,
        operatingCyclesAttr: Int? = null
    ) = AbstractComplexDeviceComponentState(
        abstractDeviceComponentState(
            extensionElement = extensionElement,
            descriptorHandleAttr = descriptorHandleAttr,
            calibrationInfo = calibrationInfo,
            nextCalibration = nextCalibration,
            physicalConnector = physicalConnector,
            activationStateAttr = activationStateAttr,
            operatingHoursAttr = operatingHoursAttr,
            operatingCyclesAttr = operatingCyclesAttr
        )
    )

    private fun abstractDeviceComponentState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef,
        calibrationInfo: CalibrationInfo? = null,
        nextCalibration: CalibrationInfo? = null,
        physicalConnector: PhysicalConnectorInfo? = null,
        activationStateAttr: ComponentActivation? = null,
        operatingHoursAttr: Int? = null,
        operatingCyclesAttr: Int? = null
    ) = AbstractDeviceComponentState(
        abstractState = abstractState(extensionElement, descriptorHandleAttr),
        calibrationInfo = calibrationInfo,
        nextCalibration = nextCalibration,
        physicalConnector = physicalConnector,
        activationStateAttr = activationStateAttr,
        operatingHoursAttr = operatingHoursAttr,
        operatingCyclesAttr = operatingCyclesAttr
    )

    private fun abstractState(
        extensionElement: Extension? = null,
        descriptorHandleAttr: HandleRef
    ) = AbstractState(
        extensionElement = extensionElement,
        descriptorHandleAttr = descriptorHandleAttr
    )

    private fun abstractDeviceComponentDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        productionSpecification: List<AbstractDeviceComponentDescriptor.ProductionSpecification> = listOf()
    ) = AbstractDeviceComponentDescriptor(
        abstractDescriptor = abstractDescriptor(extensionElement, type, handleAttr, safetyClassificationAttr),
        productionSpecification = productionSpecification
    )

    private fun abstractComplexDeviceComponentDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null,
        productionSpecification: List<AbstractDeviceComponentDescriptor.ProductionSpecification> = listOf()
    ) = AbstractComplexDeviceComponentDescriptor(
        abstractDeviceComponentDescriptor = abstractDeviceComponentDescriptor(
            extensionElement = extensionElement,
            type = type,
            handleAttr = handleAttr,
            safetyClassificationAttr = safetyClassificationAttr,
            productionSpecification = productionSpecification,
        )
    )

    private fun abstractDescriptor(
        extensionElement: Extension? = null,
        type: CodedValue? = null,
        handleAttr: Handle,
        safetyClassificationAttr: SafetyClassification? = null
    ) = AbstractDescriptor(
        extensionElement = extensionElement,
        type = type,
        handleAttr = handleAttr,
        safetyClassificationAttr = safetyClassificationAttr
    )
}
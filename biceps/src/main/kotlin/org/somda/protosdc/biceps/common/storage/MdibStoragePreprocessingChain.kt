package org.somda.protosdc.biceps.common.storage

import org.somda.protosdc.biceps.common.MdibDescriptionModifications
import org.somda.protosdc.biceps.common.MdibStateModifications

public interface MdibStoragePreprocessingChain {
    /**
     * Accepts a set of description modifications and applies them on every available description chain segment.
     *
     * @param modifications the modification to pass to the chain segments.
     * @return the modifications after preprocessing.
     * @throws PreprocessingException in case a chain segment fails.
     */
    @Throws(PreprocessingException::class)
    public fun processDescriptionModifications(modifications: MdibDescriptionModifications): MdibDescriptionModifications

    /**
     * Accepts a set of state modifications and applies them on every available state chain segment.
     *
     * @param modifications the modification to pass to the chain segments.
     * @return the modifications after preprocessing.
     * @throws PreprocessingException in case a chain segment fails.
     */
    @Throws(PreprocessingException::class)
    public fun processStateModifications(modifications: MdibStateModifications): MdibStateModifications
}
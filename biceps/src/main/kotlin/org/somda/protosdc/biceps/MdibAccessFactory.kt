package org.somda.protosdc.biceps

import org.somda.protosdc.biceps.common.MdibVersion
import org.somda.protosdc.biceps.consumer.access.RemoteMdibAccess
import org.somda.protosdc.biceps.dagger.DaggerBicepsComponent
import org.somda.protosdc.biceps.provider.access.LocalMdibAccess
import org.somda.protosdc.common.InstanceId
import java.util.*

@DslMarker
public annotation class BicepsInitDsl

public interface MdibAccessFactory {

    public fun createLocalMdibAccess(initialMdibVersion: MdibVersion? = null): LocalMdibAccess
    public fun createRemoteMdibAccess(initialMdibVersion: MdibVersion): RemoteMdibAccess

    @BicepsInitDsl
    public interface Init {
        public val instanceId: InstanceId
        public val config: BicepsConfig

        public fun instanceId(instanceId: InstanceId)
        public fun config(config: BicepsConfig)
    }
}

public class MdibAccessFactoryImpl(init: MdibAccessFactoryInitImpl) : MdibAccessFactory {
    private val component = DaggerBicepsComponent.builder()
        .bind(init.instanceId)
        .build()

    override fun createLocalMdibAccess(initialMdibVersion: MdibVersion?): LocalMdibAccess {
        return initialMdibVersion?.let { component.localMdibAccessFactory().create(it) }
            ?: component.localMdibAccessFactory().create()
    }

    override fun createRemoteMdibAccess(initialMdibVersion: MdibVersion): RemoteMdibAccess {
        return component.remoteMdibAccessFactory().create(initialMdibVersion)
    }
}

@BicepsInitDsl
public class MdibAccessFactoryInitImpl : MdibAccessFactory.Init {
    private var _instanceId: InstanceId = InstanceId(UUID.randomUUID().toString())
    private var _config: BicepsConfig = BicepsConfig()

    override val instanceId: InstanceId
        get() = _instanceId

    override val config: BicepsConfig
        get() = _config

    override fun instanceId(instanceId: InstanceId) {
        this._instanceId = instanceId
    }

    override fun config(config: BicepsConfig) {
        this._config = config
    }
}

public fun mdibAccessFactory(init: MdibAccessFactory.Init.() -> Unit = {}): MdibAccessFactory {
    return MdibAccessFactoryImpl(MdibAccessFactoryInitImpl().apply(init))
}


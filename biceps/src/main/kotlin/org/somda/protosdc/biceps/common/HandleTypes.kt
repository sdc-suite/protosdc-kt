package org.somda.protosdc.biceps.common

import org.somda.protosdc.model.biceps.Handle
import org.somda.protosdc.model.biceps.HandleRef

/**
 * Utility interface to generate handles or handle references from enum class values.
 *
 * Example implementation
 *
 * ```kotlin
 * enum class MyHandles(private val str: String) : HandleTypes {
 *     MDS("mds"),
 *     VMD("vmd");
 *
 *     override fun str() = str
 * }
 * ```
 *
 * Use extension functions to access handle and handle reference wrapping:
 *
 * ```kotlin
 * println(MyHandles.VMD.str())
 * println(MyHandles.VMD.handle())
 * println(MyHandles.VMD.handle().ref())
 * ```
 */
public interface HandleTypes {
    public fun str(): String
}

/**
 * Takes a handle string and wraps it into a [Handle].
 *
 * @param localNameReceiver Selector to retrieve the handle string to be wrapped.
 */
public fun HandleTypes.handle(): Handle {
    return Handle(str())
}
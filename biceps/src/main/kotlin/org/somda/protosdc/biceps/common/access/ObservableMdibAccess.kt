package org.somda.protosdc.biceps.common.access

import kotlinx.coroutines.channels.ReceiveChannel

public interface ObservableMdibAccess : MdibAccess {

    public suspend fun subscribe(): ReceiveChannel<MdibAccessEvent>

    public suspend fun unsubscribeAll()
}
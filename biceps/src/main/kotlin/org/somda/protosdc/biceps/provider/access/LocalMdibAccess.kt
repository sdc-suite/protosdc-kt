package org.somda.protosdc.biceps.provider.access

import org.somda.protosdc.biceps.common.MdibDescriptionModifications
import org.somda.protosdc.biceps.common.MdibStateModifications
import org.somda.protosdc.biceps.common.MdibVersion
import org.somda.protosdc.biceps.common.access.ObservableMdibAccess
import org.somda.protosdc.biceps.common.access.ReadTransaction
import org.somda.protosdc.biceps.common.access.WriteDescriptionResult
import org.somda.protosdc.biceps.common.access.WriteStateResult
import org.somda.protosdc.biceps.common.storage.PreprocessingException

/**
 * Thread-safe MDIB read and write access for the BICEPS provider side.
 */
public interface LocalMdibAccess : ObservableMdibAccess, ReadTransaction {
    public interface Factory {
        public fun create(): LocalMdibAccess
        public fun create(initialMdibVersion: MdibVersion): LocalMdibAccess
    }

    /**
     * Processes the description modifications object, stores the data internally and trigger an event.
     *
     * Data is passed through if the elements of the modification change set do not violate the MDIB storage.
     *
     * *Attention: description modifications are expensive. Even if this operation allows to change states, it
     * should only be used for changes that affect descriptors.*
     *
     * @param descriptionModifications a set of insertions, updates and deletes.
     * @return a write result including inserted, updates and deleted entities.
     * @throws PreprocessingException if something goes wrong during preprocessing, i.e., the consistency of the MDIB
     * would be violated.
     */
    @Throws(PreprocessingException::class)
    public suspend fun writeDescription(descriptionModifications: MdibDescriptionModifications): WriteDescriptionResult

    /**
     * Processes the state modifications object, stores the data internally and triggers an event.
     *
     * Data is passed through if the elements of the modification change set do not violate the MDIB storage.
     *
     * *Hint: this function cheap in terms of runtime and should be used for state changes.*
     *
     * @param stateModifications a set of state updates.
     * @return a write result including the updated entities.
     * @throws PreprocessingException if something goes wrong during preprocessing, i.e., the consistency of the MDIB
     * would be violated.
     */
    @Throws(PreprocessingException::class)
    public suspend fun writeStates(stateModifications: MdibStateModifications): WriteStateResult
}
package org.somda.protosdc.biceps.common.storage

import kotlin.Exception

/**
 * An exception that is thrown if a preprocessing error occurs.
 */
public class PreprocessingException(message: String, cause: Throwable) :
    Exception(message, cause)
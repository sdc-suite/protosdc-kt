package org.somda.protosdc.biceps.common

import org.somda.protosdc.common.Oid
import org.somda.protosdc.common.toOid
import java.net.URI

/**
 * Available IEEE 11073-10101 standards as of 2022-02-06.
 */
public enum class IeeeVersion(public val number: Int) {
    /**
     * IEEE 1073.1.1.1-2004, replaced by ISO/IEEE 11073-10101-2004
     */
    VERSION_1(1),

    /**
     * IEEE 11073-10101a-2015 (amendment)
     */
    VERSION_2(2),

    /**
     * IEEE 11073-10101-2019 or ISO/IEEE 11073-10101-2020, respectively.
     */
    VERSION_3(3)
}

/**
 * Returns an IEEE 11073-10101 nomenclature OID.
 *
 * @param version the sub-arc number to add a specific revision or null to return the unversioned OID.
 * @return the IEEE 11073-10101 nomenclature OID with optional version sub-arc.
 */
public fun ieeeNomenclatureOid(version: IeeeVersion? = null): Oid = version?.let {
    NomenclatureConstants.IEEE_11073_10101_OID.appendArc(version.number)
} ?: NomenclatureConstants.IEEE_11073_10101_OID

/**
 * Nomenclature constants used by BICEPS.
 */
public object NomenclatureConstants {
    /**
     * IEEE 11073-10101 as identified by a URN.
     *
     * - No version information included.
     * - This representation is commonly used by HL7 FHIR, see http://build.fhir.org/mdc.html
     */
    public val IEEE_11073_10101_URN: URI = URI("urn:iso:std:iso:11073:10101")

    /**
     * IEEE 11073-10101 as identified by an ISO ASN.1 OID assignment that is recommended for future uses.
     *
     * Representation by secondary identifiers:
     *
     * iso(1) identified-organization(3) ieee(111) standards-association-numbered-series-standards(2) ieee11073(11073) part10101(10101)
     */
    public val IEEE_11073_10101_OID: Oid = "1.3.111.2.11073.10101".toOid()

    /**
     * IEEE 11073-10101 as identified by an ISO ASN.1 OID that was assigned in IEEE Std 11073-10101-2004.
     *
     * - This notation remains valid for use in existing uses.
     * - No version information included.
     *
     * Representation by secondary identifiers:
     *
     * iso(1) member-body(2) US(840) ieee11073(10004) mddl(1) version1(1) common(1) standardSpecificExtensions(0) modules(0)
     *
     */
    public val IEEE_11073_10101_LEGACY_OID: Oid = "1.2.840.10004.1.1.1.0.0".toOid()

    /**
     * IEEE 11073-10101 as identified by an HL7 ISO ASN.1 OID.
     *
     * No version information included.
     *
     * Representation by secondary identifiers:
     *
     * joint-iso-itu-t(2) country(16) us(840) organization(1) hl7(113883) externalCodeSystems(6) ieee11073(24) mddl(1) version1(1) common(1) standardSpecificExtensions(0) modules(0)
     */
    public val IEEE_11073_10101_HL7_OID: Oid = "2.16.840.1.113883.6.24.1.1.1.0.0".toOid()
}
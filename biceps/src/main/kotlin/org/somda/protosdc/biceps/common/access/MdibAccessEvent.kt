package org.somda.protosdc.biceps.common.access

import org.somda.protosdc.biceps.common.WrittenMdibDescriptionModifications
import org.somda.protosdc.biceps.common.WrittenStateModification

/**
 *
 */
public sealed class MdibAccessEvent(public val modifiedStates: List<WrittenStateModification>) {
    public abstract val mdibAccess: MdibAccess

    public data class AlertStateModification(
        override val mdibAccess: MdibAccess,
        val modifiedAlertStates: List<WrittenStateModification.Alert>
    ) : MdibAccessEvent(modifiedAlertStates) {
        public fun groupedBySource(): Map<String, List<WrittenStateModification.Alert>> =
            groupedBySource(this.modifiedAlertStates)
    }

    public data class ComponentStateModification(
        override val mdibAccess: MdibAccess,
        val modifiedComponentStates: List<WrittenStateModification.Component>
    ) : MdibAccessEvent(modifiedComponentStates) {
        public fun groupedBySource(): Map<String, List<WrittenStateModification.Component>> =
            groupedBySource(this.modifiedComponentStates)
    }

    public data class ContextStateModification(
        override val mdibAccess: MdibAccess,
        val modifiedContextStates: List<WrittenStateModification.Context>
    ) : MdibAccessEvent(modifiedContextStates) {
        public fun groupedBySource(): Map<String, List<WrittenStateModification.Context>> =
            groupedBySource(this.modifiedContextStates)
    }

    public data class MetricStateModification(
        override val mdibAccess: MdibAccess,
        val modifiedMetricStates: List<WrittenStateModification.Metric>
    ) : MdibAccessEvent(modifiedMetricStates) {
        public fun groupedBySource(): Map<String, List<WrittenStateModification.Metric>> =
            groupedBySource(this.modifiedMetricStates)
    }

    public data class OperationStateModification(
        override val mdibAccess: MdibAccess,
        val modifiedOperationStates: List<WrittenStateModification.Operation>
    ) : MdibAccessEvent(modifiedOperationStates) {
        public fun groupedBySource(): Map<String, List<WrittenStateModification.Operation>> =
            groupedBySource(this.modifiedOperationStates)
    }

    public data class WaveformModification(
        override val mdibAccess: MdibAccess,
        val modifiedWaveformStates: List<WrittenStateModification.Waveform>
    ) : MdibAccessEvent(modifiedWaveformStates) {
        public fun groupedBySource(): Map<String, List<WrittenStateModification.Waveform>> =
            groupedBySource(this.modifiedWaveformStates)
    }

    public data class DescriptionModification(
        override val mdibAccess: MdibAccess,
        val entities: WrittenMdibDescriptionModifications
    ) : MdibAccessEvent(listOf())
}

private fun <T : WrittenStateModification> groupedBySource(modifications: List<T>): Map<String, List<T>> {
    return mutableMapOf<String, MutableList<T>>().apply {
        modifications.forEach { modification ->
            val modList = this[modification.sourceMds] ?: mutableListOf<T>().also { this[modification.sourceMds] = it }
            modList.add(modification)
        }
    }
}
plugins {
    id("org.somda.protosdc.sdc.shared")
    alias(libs.plugins.org.somda.gitlab.maven.publishing)
    alias(libs.plugins.org.somda.append.snapshot.id)
}

kotlin {
    explicitApi()
}
package org.somda.protosdc.network.ip

import java.net.Inet4Address
import java.net.Inet6Address
import java.net.InetAddress

/**
 * Retrieves the IP version of this address.
 *
 * @return the IP version of this address.
 * @throws IllegalStateException if there is an unknown IP version.
 */
@Throws(IllegalStateException::class)
public fun InetAddress.ipVersion(): IpVersion = when (this) {
    is Inet4Address -> IpVersion.IP_V4
    is Inet6Address -> IpVersion.IP_V6
    else -> throw IllegalStateException("InetAddress has invalid IP protocol version")
}
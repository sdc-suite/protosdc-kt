package org.somda.protosdc.common

/**
 * Suspendable version of the [java.lang.AutoCloseable] interface.
 *
 * An [AsyncCloseable] is an object that may hold resources (such as file or socket handles) until it is closed.
 */
public interface AsyncCloseable {
    /**
     * Releases the held resources.
     */
    public suspend fun close()
}

/**
 * Executes the given [block] function on this resource and then closes it down correctly whether an exception
 * is thrown or not.
 *
 * * This function is mostly copied from the existing use-function specified for non-suspendable close()*
 *
 * @param block a function to process this [AsyncCloseable] resource.
 * @return the result of [block] function invoked on this resource.
 */
public suspend inline fun <T : AsyncCloseable?, R> T.use(block: (T) -> R): R {
    var exception: Throwable? = null
    try {
        return block(this)
    } catch (e: Throwable) {
        exception = e
        throw e
    } finally {
        when {
            this == null -> {
            }
            exception == null -> close()
            else ->
                try {
                    close()
                } catch (closeException: Throwable) {
                    // cause.addSuppressed(closeException) // ignored here
                }
        }
    }
}
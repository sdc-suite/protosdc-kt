package org.somda.protosdc.common

/**
 * Interface for APIs that support subscribable data via [QueuedPublisher].
 */
public interface QueuedObservable<T> {
    /**
     * Subscribes to this publisher.
     *
     * Only use this subscribe method if there is no intention of the subscriber to quit the subscription on its own.
     *
     * @return a sequence of data items. The sequence throws an [IllegalStateException] if the data is not fetched
     *         fast enough such that the underlying subscription queue overflows.
     */
    public fun subscribe(): Sequence<T>

    /**
     * Subscribes to this publisher.
     *
     * Use this function if there is intention of the subscriber to quit the subscription on its own.
     * While requesting updates, use [QueuedPublisher.unsubscribe] to stop the queue such that no more items will be yielded
     * by the sequence.
     *
     * @return a pair consisting of a subscription id (first) and a sequence of data items (second). The sequence
     *         throws an [IllegalStateException] if the data is not fetched fast enough such that the underlying
     *         subscription queue overflows.
     */
    public fun subscribeWithId(): Pair<QueuedPublisher.SubscriptionId, Sequence<T>>

    /**
     * Immediately finishes a running subscriptions, i.e. ends the sequence.
     *
     * @param subscriptionId the subscription identifier returned by [QueuedPublisher.subscribeWithId]
     */
    public fun unsubscribe(subscriptionId: QueuedPublisher.SubscriptionId)
}
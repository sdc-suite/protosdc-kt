package org.somda.protosdc.common

public interface Option

public abstract class Options<OptionType: Option>(option: Set<OptionType?>) {
    internal constructor(vararg option: OptionType?) : this(option.toSet())

    private val _options: MutableSet<OptionType?> = option.toMutableSet()
    public val options: Set<OptionType?> = _options

    public inline fun <reified T : OptionType?> get(): T? {
        return options.filterIsInstance<T>().firstOrNull()
    }

    public fun addOption(option: OptionType?) {
        _options.add(option)
    }
}
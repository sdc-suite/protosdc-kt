package org.somda.protosdc.common

import java.net.URI

public fun String.toOid(): Oid = Oid.fromString(this)
public fun URI.toOid(): Oid = Oid.fromUri(this)

public class Oid(components: List<Int>) {
    public val components: List<Int> = components.also {
        require(it.isNotEmpty() && it.first() >= 0 && it.first() <= 2) {
            "OID components $it are invalid. Size shall be greater than 0, first arc needs to be in [0, 2]"
        }
    }

    public val rootArc: Arc = components
        .map { Arc(this, it) }
        .reduceRight { acc, component -> Arc(this, acc.number, component) }

    public companion object {
        private val oidRegex = "^([0-2])((\\.0)|(\\.[1-9][0-9]*))*\$".toRegex()
        private const val URN_SCHEME = "urn"
        private const val URI_OID_PREFIX = "oid:"

        public fun fromString(oid: String): Oid = Oid(oidRegex.findAll(oid).iterator().let { matches ->
            require(matches.hasNext()) { "$oid is not a valid OID" }
            oid.split(".").toMutableList().map { it.toInt() }
        })

        public fun fromUri(uri: URI): Oid = uri.let {
            require(
                uri.scheme.startsWith(URN_SCHEME, true)
            ) {
                "URI scheme does not match URN: ${uri.scheme}"
            }
            require(
                uri.rawSchemeSpecificPart.startsWith(
                    URI_OID_PREFIX,
                    true
                )
            ) { "URN does not match OID: ${uri.rawSchemeSpecificPart}" }
            uri.rawSchemeSpecificPart.substring(URI_OID_PREFIX.length).toOid()
        }
    }

    public fun appendArc(number: Int): Oid = Oid(components
        .foldRight(mutableListOf(number)) { num, acc -> acc.apply { add(num) } }
        .asReversed()
    )

    public fun toUri(): URI = URI("urn:oid:$this")

    public fun isPrefixOf(oid: Oid): Boolean = this.components.size < oid.components.size && oid.toString().startsWith(this.toString())

    @Override
    override fun toString(): String = components.joinToString(".") { it.toString() }

    @Override
    override fun equals(other: Any?): Boolean {
        if (other == null || other !is Oid) {
            return false
        }
        return this.toString() == other.toString()
    }

    public class Arc(public val owner: Oid, public val number: Int, public val subArc: Arc? = null) {
        public fun toOid(): Oid {
            val components = mutableListOf(owner.rootArc.number)
            var arc = owner.rootArc
            while (arc != this && arc.subArc != null) {
                components.add(arc.subArc!!.number)
                arc = arc.subArc!!
            }
            return Oid(components)
        }
    }
}
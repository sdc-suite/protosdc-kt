package org.somda.protosdc.common

import java.lang.reflect.Field
import java.util.*

/**
 * Determines a field to be stringified if used with [stringify].
 */
@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
public annotation class Stringified

/**
 * Stringifies annotated fields of a given object.
 *
 * This function considers every field that is annotated with [Stringified].
 *
 * @return the stringified object in conformance with the protoSDC-kt coding conventions.
 */
public fun Any.stringify(): String =
    this.stringifyWithFilter(sortedMapOf()) { field: Field ->
        field.isAnnotationPresent(
            Stringified::class.java
        )
    }

/**
 * Stringifies annotated fields of a given object and appends defined map.
 *
 * @param keyValues key-values to be additionally appended.
 * @return the stringified object in conformance with the protoSDC-kt coding conventions.
 */
public fun Any.stringify(keyValues: SortedMap<String, Any?>): String =
    this.stringifyWithFilter(keyValues) { field: Field ->
        field.isAnnotationPresent(
            Stringified::class.java
        )
    }


/**
 * Stringifies some fields of a given object.
 *
 * @param fieldNames the fields of the class that are requested to be stringified.
 * @return the stringified object in conformance with the protoSDC-kt coding conventions.
 */
public fun Any.stringify(fieldNames: Array<String>): String =
    this.stringifyWithFilter(sortedMapOf()) { field: Field ->
        for (fieldName in fieldNames) {
            if (field.name == fieldName) {
                return@stringifyWithFilter true
            }
        }
        false
    }

/**
 * Stringifies some fields of a given object and appends defined map.
 *
 * @param fieldNames the fields of the class that are requested to be stringified.
 * @param keyValues  key-values to be additionally appended.
 * @return the stringified object in conformance with the protoSDC-kt coding conventions.
 */
public fun Any.stringify(fieldNames: Array<String>, keyValues: SortedMap<String, Any?>): String =
    this.stringifyWithFilter(keyValues) { field: Field ->
        for (fieldName in fieldNames) {
            if (field.name == fieldName) {
                return@stringifyWithFilter true
            }
        }
        false
    }

/**
 * Stringifies key-value pairs from a map.
 *
 * @param keyValues key-value pairs to put as properties of the output-string
 * @return the stringified object in conformance with the protoSDC-kt coding conventions.
 */
public fun Any.stringifyMap(keyValues: SortedMap<String, Any?>): String {
    val stringBuffer = start(this)
    appendMapValues(stringBuffer, keyValues)
    return finish(stringBuffer)
}

/**
 * Stringifies all fields of a given object.
 *
 * @return the stringified object in conformance with the protoSDC-kt coding conventions.
 */
public fun Any.stringifyAll(): String {
    return this.stringifyWithFilter(sortedMapOf()) { field: Field -> !field.name.startsWith("this$") }
}

private fun appendMapValues(stringBuffer: StringBuffer, keyValues: SortedMap<String, Any?>) {
    val iterator = keyValues.entries.iterator()
    var firstRun = true
    while (iterator.hasNext()) {
        if (!firstRun) {
            stringBuffer.append(", ")
        }
        firstRun = false
        val next = iterator.next()
        appendKeyValue(stringBuffer, next.key, next.value)
    }
}

private fun Any.stringifyWithFilter(
    keyValues: SortedMap<String, Any?>,
    filter: (field: Field) -> Boolean
): String {
    val stringBuffer = start(this)
    try {
        appendToStringProperties(stringBuffer, this, filter)
    } catch (e: IllegalAccessException) {
        throw RuntimeException(e)
    }
    if (keyValues.isNotEmpty()) {
        stringBuffer.append(", ")
        appendMapValues(stringBuffer, keyValues)
    }
    return finish(stringBuffer)
}

private fun start(obj: Any) = StringBuffer().append(obj::class.java.simpleName).append("(")

private fun finish(stringBuffer: StringBuffer) = stringBuffer.append(")").toString()

@Throws(IllegalAccessException::class)
private fun appendToStringProperties(stringBuffer: StringBuffer, obj: Any, filter: (field: Field) -> Boolean) {
    val fields = obj.javaClass.declaredFields
    var insertedFields = 0
    for (field in fields) {
        field.isAccessible = true
        if (!filter(field)) {
            continue
        }
        if (insertedFields++ > 0) {
            stringBuffer.append(", ")
        }
        appendKeyValue(stringBuffer, field.name, field[obj])
    }
}

private fun appendKeyValue(stringBuffer: StringBuffer, key: String, value: Any?) {
    stringBuffer.append(key)
    stringBuffer.append('=')
    stringBuffer.append(value) // null value is ok; will be converted to string "null"
}
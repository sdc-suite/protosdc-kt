package org.somda.protosdc.common

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext

public suspend inline fun <T> awaitWithIoDispatcher(crossinline block: suspend () -> T): Result<T> = coroutineScope {
    withContext(Dispatchers.IO) {
        runCatching { block() }
    }
}
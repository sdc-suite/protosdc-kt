package org.somda.protosdc.common

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.net.URI

class OidTest {

    @Test
    fun `test invalid oids`() {
        val invalidOids = listOf(
            "5.1.23",
            "1.2.-3",
            "1,2.3",
            "1.2 .3",
            "",
            "1.2.3."
        )

        invalidOids.forEach {
            assertThrows<IllegalArgumentException> {
                Oid.fromString(it)
            }
        }
    }

    @Test
    fun `test round trip`() {
        val oids = listOf(
            "1.2.3",
            "1.2.3.4.5.6",
        )

        assertEquals(oids, oids.map { Oid.fromString(it) }.map { it.toString() })
        assertEquals(oids, oids.map { it.toOid() }.map { it.toString() })
    }

    @Test
    fun `test components`() {
        assertEquals(listOf(1),"1".toOid().components)
        assertEquals(listOf(1, 2, 3, 4, 5),"1.2.3.4.5".toOid().components)
    }

    @Test
    fun `test arcs`() {
        val oid = "1.2.3".toOid()

        assertEquals(1, oid.rootArc.number)
        assertEquals("1", oid.rootArc.toOid().toString())
        assertEquals(2, oid.rootArc.subArc!!.number)
        assertEquals("1.2", oid.rootArc.subArc!!.toOid().toString())
        assertEquals(3, oid.rootArc.subArc!!.subArc!!.number)
        assertEquals("1.2.3", oid.rootArc.subArc!!.subArc!!.toOid().toString())
        assertEquals(null, oid.rootArc.subArc!!.subArc!!.subArc)

        assertEquals("1.2.3.10", oid.appendArc(10).toString())
    }

    @Test
    fun `test equals`() {
        assertEquals("1".toOid(), "1".toOid())
        assertEquals("1.2.3".toOid(), "1.2.3".toOid())
        assertNotEquals("1.2".toOid(), "1.2.3".toOid())
    }

    @Test
    fun `test uri`() {
        assertEquals(URI("urn:oid:1"), "1".toOid().toUri())
        assertEquals(URI("urn:oid:1.2.3"), "1.2.3".toOid().toUri())
    }

    @Test
    fun `test prefix`() {
        assertTrue("1.2.3".toOid().isPrefixOf("1.2.3.4".toOid()))
        assertFalse("1.2.3.4".toOid().isPrefixOf("1.2.3".toOid()))
        assertFalse("1.2.3.4".toOid().isPrefixOf("1.2.3.4".toOid()))
    }
}
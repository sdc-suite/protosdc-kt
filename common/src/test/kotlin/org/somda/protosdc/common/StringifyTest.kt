package org.somda.protosdc.common

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class StringifyTest {
    private val expectedStringBase = "foo"
    private val expectedIntBase = 50
    private val expectedStringDerived = "bar"
    private val expectedIntDerived = 100
    private var testObject = TestClassDerived(
        expectedStringDerived,
        expectedIntDerived,
        expectedStringBase,
        expectedIntBase
    )

    @BeforeEach
    fun beforeEach() {
        testObject = TestClassDerived(
            expectedStringDerived,
            expectedIntDerived,
            expectedStringBase,
            expectedIntBase
        )
    }

    @Test
    fun `test stringify() with annotations`() {
        run {
            val expectedString = String.format(
                "%s(anotherString=%s)",
                TestClassDerived::class.java.simpleName,
                testObject.anotherString
            )
            assertEquals(expectedString, testObject.stringify())
        }
        run {
            testObject = TestClassDerived(
                null,
                testObject.anotherInt,
                testObject.getAString(),
                testObject.anInt
            )
            val expectedString = String.format(
                "%s(anotherString=null)",
                TestClassDerived::class.java.simpleName
            )
            assertEquals(expectedString, testObject.stringify())
        }
    }

    @Test
    fun `test stringify() with annotations and map`() {
        val expectedString = String.format(
            "%s(anotherString=%s, foo1=bar, foo2=80)",
            TestClassDerived::class.java.simpleName,
            testObject.anotherString
        )
        val map = sortedMapOf<String, Any?>(
            compareBy { it },
            "foo1" to "bar",
            "foo2" to 80
        )
        assertEquals(expectedString, testObject.stringify(map))
    }

    @Test
    fun `test stringify() with field names`() {
        run {
            val expectedString = String.format(
                "%s(anotherInt=%s)",
                TestClassDerived::class.java.simpleName,
                testObject.anotherInt
            )
            assertEquals(expectedString, testObject.stringify(arrayOf("anotherInt")))
        }
        run {
            testObject = TestClassDerived(
                null,
                testObject.anotherInt,
                testObject.getAString(),
                testObject.anInt
            )
            val expectedString = String.format(
                "%s(anotherString=null, anotherInt=%s)",
                TestClassDerived::class.java.simpleName,
                testObject.anotherInt
            )
            assertEquals(expectedString, testObject.stringify(arrayOf("anotherString", "anotherInt")))
        }
    }

    @Test
    fun `test stringifyAll()`() {
        val expectedString = String.format(
            "%s(anotherString=%s, anotherInt=%s)",
            TestClassDerived::class.java.simpleName,
            testObject.anotherString,
            testObject.anotherInt
        )
        assertEquals(expectedString, testObject.stringifyAll())
    }

    @Test
    fun `test stringifyMap()`() {
        val map = sortedMapOf<String, Any?>(
            compareBy { it },
            "foo1" to "Bar",
            "foo2" to null,
            "foo3" to 100
        )
        val expectedString = String.format("%s(foo1=Bar, foo2=null, foo3=100)", TestClassDerived::class.java.simpleName)
        assertEquals(expectedString, testObject.stringifyMap(map))
    }

    internal open inner class TestClassBase(@Stringified private val aString: String, var anInt: Int) {
        fun getAString(): String {
            return aString
        }
    }

    internal inner class TestClassDerived(
        @Stringified val anotherString: String?,
        val anotherInt: Int,
        aString: String,
        anInt: Int
    ) : TestClassBase(aString, anInt)
}
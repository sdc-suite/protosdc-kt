package org.somda.protosdc.common

import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.concurrent.atomic.AtomicBoolean

class AsyncCloseableTest {
    @Test
    fun `test usage of use`(): Unit = runBlocking {
        val closed = AtomicBoolean(false)
        AsyncClose(closed).use {
            println("Processing AsyncClose")
        }

        assertTrue(closed.get())
    }

    @Test
    fun `test usage of use with exception`(): Unit = runBlocking {
        val closed = AtomicBoolean(false)
        assertThrows<Exception> {
            AsyncClose(closed).use {
                throw Exception()
            }
        }

        assertTrue(closed.get())
    }

    class AsyncClose(var closed: AtomicBoolean) : AsyncCloseable {
        override suspend fun close() {
            closed.set(true)
        }
    }
}
plugins {
    `kotlin-dsl`
}

repositories {
    gradlePluginPortal()
    mavenCentral()
    maven("https://gitlab.com/api/v4/projects/60445622/packages/maven")
}

dependencies {
    implementation(files(libs.javaClass.superclass.protectionDomain.codeSource.location))
    implementation(libs.gradleplugins.somda.append.snapshot.id)
    implementation(libs.gradleplugins.somda.maven.publishing)
    implementation(libs.gradleplugins.somda.repository.collection)
    implementation(libs.gradleplugins.somda.with.dokka.jar)
    implementation(libs.gradleplugins.dokka)
    implementation(libs.gradleplugins.kotlin.jvm)
    implementation(libs.gradleplugins.kotlin.kapt)
    implementation(libs.gradleplugins.kotlin.serialization)
}
